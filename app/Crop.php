<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Crop extends Model
{
    protected $fillable = ['farmer_id','season_id', 'crop_name', 'area', 'total_production',
    'average_yield','direct','through_intermediaries','total'];
    
    public function farmer()
    {
        return $this->belongsTo(farmer::class);
    }
    public function Season()
    {
        return $this->belongsTo(Season::class);
    }
}
