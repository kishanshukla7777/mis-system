<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\Model;
class Customer extends Model
{
    use Notifiable;
    use HasRoles;

    protected $fillable = ['user_id', 'company_id', 'first_name',
     'last_name','address','mobile_no'];
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function company()
    {
        return $this->belongsTo(company::class);
    }
}
