<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fig extends Model
{

    protected $fillable = ['fpo_id', 'name', 'created_by'];

    public function fpo()
    {
        return $this->belongsTo(fpo::class);
    }

    public function fig_farmer(){
        return $this->hasMany(fig_farmer::class);
    }
    public function farmer(){
        return $this->hasOne(farmer::class);
    }
}
