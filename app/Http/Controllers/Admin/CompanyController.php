<?php

namespace App\Http\Controllers\Admin;
use App\company;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreCompanyRequest;
use App\Http\Requests\Admin\UpdateCompanyRequest;
use App\User;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }

        if (Gate::allows('users_manage')) {
            $companies = company::with('user')->get();

        }else{
            $companies = company::with('user')
            ->where('user_id',Auth::id())
            ->get();
        }

       // dd( $companies);
        return view('admin.company.index', compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }
        $roles = Role::where('name','company')->get()->pluck('name', 'name');
        return view('admin.company.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCompanyRequest $request)
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }

        $user = User::create(
            [
                'name' => $request['name'],
                'email' => $request['email'],
                'password' => Hash::make($request['password']),
            ]
        );
      
        $companies = company::create(  [
            'user_id' => $user->id,
            'company_name' => $request['company_name'],
            'contact_person' => 'NULL',
            'address' => $request['address'],
            'contact_no' => $request['contact_no'],
        ]);
        $roles = $request->input('roles') ? $request->input('roles') : [];
        $user->assignRole($roles);
        
        toastr()->success('Company has been saved successfully!');

        return redirect()->route('admin.company.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function show(company $company)
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }

        $company->load('roles');
     
        return view('admin.company.show', compact('company'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(company $company)
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }
        
        $company = company::with('user')->where('id',$company->id)->first();
    
        $roles = Role::get()->pluck('name', 'name');

        return view('admin.company.edit', compact('company', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCompanyRequest $request)
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }
       
        $user['name'] = $request['name'];
        $user['email'] = $request['email'];
        if ($request['password']) {
            $user['password'] = Hash::make($request['password']);
        }
     
        User::where('id',$request['user_id'])->update($user);
       
        $company['user_id'] = $request['user_id'];
        $company['company_name'] = $request['company_name'];
        $company['contact_person'] = 'NULL';
        $company['address'] = $request['address'];
        $company['contact_no'] = $request['contact_no'];
        company::where('id',$request['id'])->update($company);
        
        // $roles = $request->input('roles') ? $request->input('roles') : [];
        // $user->syncRoles($roles);
        toastr()->success('Company has been updated successfully!');
        return redirect()->route('admin.company.index');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {  
       
         try {
            if(!empty($id)){
              
                $company = company::find($id);
                $user = User::find($company->user_id);
                $user->delete();
                $company->delete();
              
                toastr()->success('Company has been deleted successfully!');
            }else{
                toastr()->error('Something went wrong please try again');
            }

          }

          //catch exception
          catch(Exception $e) {
            toastr()->error('Something went wrong please try again');
            echo 'Message: ' .$e->getMessage();
          }
          return redirect()->route('admin.company.index');
    }

    public function massDestroy(Request $request)
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }
        company::whereIn('id', request('ids'))->delete();
        toastr()->success('Data has been deleted successfully!');
        return response()->noContent();
    }
}
