<?php

namespace App\Http\Controllers\Admin;

use App\Crop;
use App\farmer;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\StoreCropRequest;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Season;
use Illuminate\Support\Facades\Auth;

class CropController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('employee_manage')) {
            return abort(401);
        }

        $crops = Crop::with('farmer')->get();
        dd( $crops); exit;
        return view('admin.crop.index', compact('crops'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCropRequest $request)
    {
     
        if (! Gate::any(['employee_manage','company_manage','users_manage'])) {
            return abort(401);
        }
        $user = Crop::create($request->all());
        $farmer = farmer::where('id',$request->farmer_id)->first();
        $farmers = farmer::with('fpo')->get();
        //$crops = Crop::with('Season','farmer')->where('farmer_id',$farmer->id)->get();
      
        toastr()->success('Crop has been saved successfully!');
        return redirect()->route('admin.farmer.edit',compact('farmers','farmer'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::any(['employee_manage','company_manage','users_manage'])) {
            return abort(401);
        }
        $cropData = Season::all();
        return view('admin.crop.create',compact('id','cropData'));
    }
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Crop $crop)
    {   
      
        if (! Gate::any(['employee_manage','company_manage','users_manage'])) {
            return abort(401);
        }

        $crop->delete();
        if (Gate::allows('users_manage')) {
            $farmers = farmer::with('fpo','employee')
            ->get();

        }else{
            $farmers = farmer::with('fpo','employee')
            ->where('employee_id',Auth::id())
            ->get();
        }
    
        toastr()->success('crop has been deleted successfully!');
        return view('admin.farmer.index', compact('farmers'));
    }
}
