<?php

namespace App\Http\Controllers\Admin;

use App\company;
use App\Customer;
use App\Http\Requests\Admin\StoreCustomerRequest;
use App\Http\Requests\Admin\UpdateCustomerRequest;
use App\Http\Controllers\Controller;
use App\User;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::any(['users_manage','company_manage'])) {
            return abort(401);
        }

        if (Gate::any(['users_manage'])) {
            $customers = Customer::with('user','company')->get();
      
            return view('admin.customer.index', compact('customers'));
        }
        if (Gate::any(['company_manage'])) {
            $company = company::where('user_id',Auth::id())->first();
            $customers = Customer::with('user','company')
            ->where('company_id',$company->id)->get();
            //dd($customers);
        return view('admin.customer.index', compact('customers'));
        }

        $customers = Customer::with('user','company')->get();
      
        return view('admin.customer.index', compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::any(['users_manage','company_manage'])) {
            return abort(401);
        }

        if (Gate::any(['users_manage'])) {
            $companys = company::get();
 
           $roles = Role::where('name','employee')->get()->pluck('name', 'name');
            return view('admin.customer.create', compact('companys','roles'));
         }

        $roles = Role::where('name','employee')->get()->pluck('name', 'name');
        return view('admin.customer.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCustomerRequest $request)
    {   
  
        if (! Gate::any(['users_manage','company_manage'])) {
            return abort(401);
        }

        if (Gate::any(['users_manage'])) {
            $company = $request['company_id'];
        }else{
            $companyData = company::where('user_id',Auth::id())->first();

            $company = $companyData->id;
        }
        $user = User::create(
            [
                'name' => "NULL",
                'email' => $request['email'],
                'password' => Hash::make($request['password']),
            ]
        );
      
        $companies = Customer::create(  [
            'user_id' => $user->id,
            'company_id' => $company,
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'address' => $request['address'],
            'mobile_no' => $request['mobile_no'],
        ]);
        $roles = $request->input('roles') ? $request->input('roles') : [];
        $user->assignRole($roles);
        
        toastr()->success('Customer has been saved successfully!');

        return redirect()->route('admin.customer.index');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        if (! Gate::any(['users_manage','company_manage'])) {
            return abort(401);
        }

        if (Gate::any(['users_manage'])) {
            $companys = company::get(); 
            $customer = Customer::with('user','company')->where('id',$customer->id)->first();
            $roles = Role::where('name','employee')->get()->pluck('name', 'name');
          
            return view('admin.customer.edit', compact('customer', 'roles','companys'));
        
         }

        
        $customer = Customer::with('user','company')->where('id',$customer->id)->first();
         $roles = Role::where('name','employee')->get()->pluck('name', 'name');
        return view('admin.customer.edit', compact('customer', 'roles'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCustomerRequest $request, Customer $Customer)
    {
        if (! Gate::any(['users_manage','company_manage'])) {
            return abort(401);
        }
     
        $user['name'] = "NULL";
        $user['email'] = $request['email'];

        if ($request['password']) {
            $user['password'] = Hash::make($request['password']);
        }
        User::where('id',$request['user_id'])->update($user);
        if (Gate::any(['users_manage'])) {
            $customer['company_id'] = $request['company_id'];
        }else{
            $customer['company_id'] = $Customer->company_id;
        }
       
        $customer['user_id'] = $request['user_id'];
        $customer['first_name'] = $request['first_name'];
        $customer['last_name'] = $request['last_name'];
        $customer['address'] = $request['address'];
        $customer['mobile_no'] = $request['mobile_no'];
       
        Customer::where('id',$request['id'])->update($customer);
      
        toastr()->success('Customer has been updated successfully!');
        return redirect()->route('admin.customer.index');
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy(customer $customer)
    {
      
        if (! Gate::any(['users_manage','company_manage'])) {
            return abort(401);
        }
       
        User::where('id',$customer->user_id)->delete();
        $customer->delete();
        toastr()->success('Customer has been deleted successfully!');
        return redirect()->route('admin.customer.index');
    }


    public function massDestroy(Request $request)
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }
        company::whereIn('id', request('ids'))->delete();
        toastr()->success('Data has been deleted successfully!');
        return response()->noContent();
    }
}
