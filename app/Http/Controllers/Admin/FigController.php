<?php

namespace App\Http\Controllers\Admin;

use App\company;
use App\farmer;
use Illuminate\Support\Facades\Gate;
use App\Fig;
use App\Customer;
use App\fig_farmer;
use App\fpo;
use App\Http\Requests\Admin\StoreFigRequest;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Validation\Validator as ValidationValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator as FacadesValidator;
use Illuminate\Validation\Validator;

class FigController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Gate::any(['employee_manage', 'company_manage', 'users_manage'])) {
            return abort(401);
        }

        if (Gate::any(['company_manage'])) {

            $company = company::where('user_id', Auth::id())->first();

            $fpos = fpo::where('company_id', $company->id)
                ->get()->pluck('id')->implode(',');
            $fposId = explode(',', $fpos);
            $figs = Fig::with('fpo', 'fig_farmer', 'farmer')
                ->whereIn('fpo_id', $fposId)
                ->get();



            return view('admin.fig.index', compact('figs'));
        }
        if (Gate::any(['employee_manage'])) {

            $customer = customer::where('user_id', Auth::id())->first();

            $fpos = fpo::where('company_id', $customer->company_id)
                ->where('assign_emp', $customer->id)->get()->pluck('id')->implode(',');
            $fposId = explode(',', $fpos);

            $figs = Fig::with('fpo', 'fig_farmer', 'farmer')
                ->whereIn('fpo_id', $fposId)
                ->get();


            return view('admin.fig.index', compact('figs'));
        }

        $figs = Fig::with('fpo', 'fig_farmer', 'farmer')->get();
        //  dd( $figs );
        return view('admin.fig.index', compact('figs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Gate::any(['employee_manage', 'company_manage', 'users_manage'])) {
            return abort(401);
        }
        if (Gate::allows('users_manage')) {
            $fpos = fpo::get();
            return view('admin.fig.create', compact('fpos'));
        }

        if (Gate::allows('company_manage')) {

            $company = company::where('user_id', Auth::id())->first();

            $fpos = fpo::where('company_id', $company->id)->get();
            return view('admin.fig.create', compact('fpos'));
        }

        if (Gate::allows('employee_manage')) {


            $customer = Customer::where('user_id', Auth::id())->first();

            $fpos = fpo::where('assign_emp', $customer->id)->get();
            return view('admin.fig.create', compact('fpos'));
        }

        $fpos = fpo::all();
        return view('admin.fig.create', compact('fpos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreFigRequest $request)
    {
        if (!Gate::any(['employee_manage', 'company_manage', 'users_manage'])) {
            return abort(401);
        }
        $user = Fig::create($request->all());

        toastr()->success('Fig has been saved successfully!');
        return redirect()->route('admin.fig.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(fig $fig)
    {
        if (!Gate::any(['employee_manage', 'company_manage', 'users_manage'])) {
            return abort(401);
        }
        if (Gate::allows('users_manage')) {
            $fpos = fpo::get();
            return view('admin.fig.edit', compact('fpos', 'fig'));
        }

        if (Gate::allows('company_manage')) {

            $company = company::where('user_id', Auth::id())->first();

            $fpos = fpo::where('company_id', $company->id)->get();
            return view('admin.fig.edit', compact('fpos', 'fig'));
        }

        if (Gate::allows('employee_manage')) {


            $customer = Customer::where('user_id', Auth::id())->first();

            $fpos = fpo::where('company_id', $customer->company_id)->get();
            return view('admin.fig.edit', compact('fpos', 'fig'));
        }

        $fpos = fpo::all();
        return view('admin.fig.edit', compact('fpos', 'fig'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreFigRequest $request, fig $fig)
    {
        if (!Gate::any(['employee_manage', 'company_manage', 'users_manage'])) {
            return abort(401);
        }

        $fig->update($request->all());

        toastr()->success('Fig has been updated successfully!');
        return redirect()->route('admin.fig.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(fig $fig)
    {
        if (!Gate::any(['employee_manage', 'company_manage', 'users_manage'])) {
            return abort(401);
        }

        $getFig = fig_farmer::where('fig_id', $fig->id)->get();

        foreach ($getFig as $deleteFarmer) {
            $userUpdate['is_assign'] = 0;
            $userUpdate['fig_id'] = 0;
            farmer::where('id', $deleteFarmer->farmer_id)->update($userUpdate);
        }


        foreach ($getFig as $delete) {
            $delete->delete();
        }
        $fig->delete();
        toastr()->success('Fig has been deleted successfully!');
        return redirect()->route('admin.fig.index');
    }

    public function addfarmer($id)
    {

        $fig = fig::with('fpo')
            ->where('id', $id)->first();
        $figFarmer = fig_farmer::get()->pluck('farmer_id');

        if (Gate::any(['employee_manage'])) {
            $customer = customer::where('user_id', Auth::id())->first();
            $fpos = fpo::where('id', $fig->fpo_id)->get()->pluck('id')->implode(',');
            $fposId = explode(',', $fpos);

            $farmers = farmer::with('fig_farmer')
                ->whereIn('fpo_id', $fposId)
                ->Where('fig_id', 0)
                ->orWhere('fig_id', $id)
                ->get();
        }
        if (Gate::any(['users_manage'])) {

            $farmers = farmer::with('fig_farmer')
                ->where('fpo_id', $fig->fpo_id)
                ->Where('fig_id', 0)
                ->orWhere('fig_id', $id)
                ->get();
        }

        if (Gate::any(['company_manage'])) {

            $farmers = farmer::with('fig_farmer')
                ->where('fpo_id', $fig->fpo_id)
                ->Where('fig_id', 0)
                ->orWhere('fig_id', $id)
                ->get();
        }

        return view('admin.fig.addfarmer', compact('fig', 'farmers'));
    }
    public function savefarmer(Request $request)
    {

        $this->validate($request, [
            'duallistbox_demo1' => 'required'
        ]);

        if ($request['fig_id']) {
            $figFarmer = fig_farmer::where('fig_id', $request['fig_id'])
                ->where('customer_id', Auth::id())
                ->get();

            foreach ($figFarmer as $delete) {
                $userUpdate['is_assign'] = 0;
                $userUpdate['fig_id'] = 0;
                farmer::where('id', $delete->farmer_id)->update($userUpdate);
                fig_farmer::find($delete->id)->delete();
            }
        }

        $figs = Fig::with('fpo')->get();
        foreach ($request->duallistbox_demo1 as $val) {

            $userUpdate['is_assign'] = 1;
            $userUpdate['fig_id'] = $request['fig_id'];

            farmer::where('id', $val)->update($userUpdate);

            $farmer = fig_farmer::create(
                [
                    'fpo_id' => $request['fpo_id'],
                    'fig_id' => $request['fig_id'],
                    'farmer_id' => $val,
                    'customer_id' => Auth::id(),
                ]
            );
        }

        toastr()->success('Farmers has been deleted successfully!');
        return redirect('/admin/fig');
        //return view('admin.fig.index',compact('figs'));


    }


    public function addlrp($id)
    {


        $figFarmers = fig_farmer::with('farmer')
            ->where('fig_id', $id)->get();

        return view('admin.fig.showlrp', compact('figFarmers'));
    }
    public function savelrp(Request $request)
    {

        $this->validate($request, [
            'selectFarmer' => 'required'
        ]);
        $a = fig_farmer::where('id', $request['selectFarmer'])->first();

        $b = fig_farmer::where('fig_id', $a->fig_id)->get();
        foreach ($b as $c) {
            $fig_farmer = fig_farmer::find($c->id);
            $fig_farmer->lrp = 0;
            $fig_farmer->save();
        }

        $fig_farmer = fig_farmer::find($request->selectFarmer);
        $fig_farmer->lrp = 1;
        $fig_farmer->save();
        $figs = Fig::with('fpo')->get();
        toastr()->success('Farmers has been selected successfully!');
        return redirect('/admin/fig');
        //return view('admin.fig.index',compact('figs'));


    }
}
