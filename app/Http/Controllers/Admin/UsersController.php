<?php

namespace App\Http\Controllers\Admin;

use App\company;
use App\Customer;
use App\User;
use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreUsersRequest;
use App\Http\Requests\Admin\UpdateUsersRequest;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{
    /**
     * Display a listing of User.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }

        $users = User::all();
        // dd($users);
        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating new User.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }
        $roles = Role::get()->pluck('name', 'name');

        return view('admin.users.create', compact('roles'));
    }

    /**
     * Store a newly created User in storage.
     *
     * @param  \App\Http\Requests\StoreUsersRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUsersRequest $request)
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }
        $user = User::create($request->all());
        $roles = $request->input('roles') ? $request->input('roles') : [];
        $user->assignRole($roles);
        toastr()->success('User has been saved successfully!');
        return redirect()->route('admin.users.index');
    }


    /**
     * Show the form for editing User.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }
        $roles = Role::get()->pluck('name', 'name');

        return view('admin.users.edit', compact('user', 'roles'));
    }

    /**
     * Update User in storage.
     *
     * @param  \App\Http\Requests\UpdateUsersRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUsersRequest $request, User $user)
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }

        $user->update($request->all());
        $roles = $request->input('roles') ? $request->input('roles') : [];
        $user->syncRoles($roles);
        toastr()->success('User has been updated successfully!');
        return redirect()->route('admin.users.index');
    }

    public function show(User $user)
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }

        $user->load('roles');

        return view('admin.users.show', compact('user'));
    }

    /**
     * Remove User from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }

        $user->delete();
        toastr()->success('User has been deleted successfully!');
        return redirect()->route('admin.users.index');
    }

    /**
     * Delete all selected User at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }
        User::whereIn('id', request('ids'))->delete();
        toastr()->success('User has been deleted successfully!');
        return response()->noContent();
    }


    public function userProfile($id)
    {

        if ($id == Auth::id()) {
            $user = User::where('id', $id)->first();
            $user1 =  $user->roles['0'];

            if (Gate::any(['company_manage'])) {
                if ($user1->name == 'company') {
                    $company = company::where('user_id', $user->id)->first();
                }
                return view('admin.users.profile', compact('user', 'company'));
            }
            if (Gate::any(['employee_manage'])) {
                if ($user1->name == 'employee') {
                    $company = Customer::where('user_id', $user->id)->first();
                }
                return view('admin.users.profile', compact('user', 'company'));
            }

            if (Gate::any(['users_manage'])) {
                return view('admin.users.profile', compact('user'));
            }
        } else {
            return abort(401);
        }
    }

    public function saveProfile(Request $request, user $user)
    {

        $user = User::where('id', $request->user_id)->first();
        $user1 =  $user->roles['0'];

        if (Gate::any(['company_manage'])) {
            if ($user1->name == 'company') {
                $companyRecode = company::where('user_id', $user->id)->first();
            }
            $userUpdate['name'] = $request->name;
            $userUpdate['email'] = $request->email;

            $user = User::where('id', $request->user_id)->update($userUpdate);

            $company['company_name'] = $request->company_name;
            $company['address'] = $request->address;
            $company['contact_no'] = $request->contact_no;

            company::where('id', $companyRecode->id)->update($company);
            toastr()->success('User profile updated successfully!');
            return redirect()->route('admin.home');
        }

        if (Gate::any(['employee_manage'])) {
            if ($user1->name == 'employee') {
                $customerRecode = customer::where('user_id', $user->id)->first();
            }
            $userUpdate['email'] = $request->email;

            $user = User::where('id', $request->user_id)->update($userUpdate);

            $customer['first_name'] = $request->first_name;
            $customer['last_name'] = $request->last_name;
            $customer['address'] = $request->address;
            $customer['mobile_no'] = $request->mobile_no;

            Customer::where('id', $customerRecode->id)->update($customer);
            toastr()->success('User profile updated successfully!');
            return redirect()->route('admin.home');
        }

        if (Gate::any(['users_manage'])) {

            $userUpdate['name'] = $request->name;
            $userUpdate['email'] = $request->email;

            $user = User::where('id', $request->user_id)->update($userUpdate);


            toastr()->success('User profile updated successfully!');
            return redirect()->route('admin.home');
        }

        return redirect()->route('admin.home');
    }
}
