<?php

namespace App\Http\Controllers\Admin;

use App\company;
use App\Crop;
use App\Customer;
use App\farmer;
use App\fig_farmer;
use App\fpo;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class farmerController extends Controller
{
    public function index()
    {
        if (!Gate::any(['employee_manage', 'company_manage', 'users_manage'])) {
            return abort(401);
        }

        if (Gate::allows('users_manage')) {
            $farmers = farmer::with('fpo', 'employee')
                ->get();
        } else if (Gate::allows('company_manage')) {

            $company = company::where('user_id', Auth::id())->first();
            $fpos = fpo::where('company_id', $company->id)->get()->pluck('id')->implode(',');
            $fposId = explode(',', $fpos);

            $farmers = farmer::with('fpo', 'employee')
                ->whereIn('fpo_id', $fposId)
                ->get();
        } else {

            $customer = customer::where('user_id', Auth::id())->first();

            //where('company_id',$customer->id)->or
            $fpos = fpo::Where('assign_emp', $customer->id)
                ->get()->pluck('id')->implode(',');
            $fposId = explode(',', $fpos);
            $farmers = farmer::with('fpo', 'employee')
                ->whereIn('fpo_id', $fposId)
                ->get();
        }

        return view('admin.farmer.index', compact('farmers'));
    }
    public function create()
    {

        if (!Gate::any(['employee_manage', 'company_manage', 'users_manage'])) {
            return abort(401);
        }

        if (Gate::allows('users_manage')) {
            $fpos = fpo::get();
            return view('admin.farmer.create', compact('fpos'));
        }

        if (Gate::allows('company_manage')) {

            $company = company::where('user_id', Auth::id())->first();

            $fpos = fpo::where('company_id', $company->id)->get();
            return view('admin.farmer.create', compact('fpos'));
        }
        if (Gate::allows('employee_manage')) {
            $customer = Customer::where('user_id', Auth::id())->first();
            $fpos = fpo::where('assign_emp', $customer->id)
                /// ->orWhere('company_id',$customer->company_id)
                ->get();
            return view('admin.farmer.create', compact('fpos'));
        }
        return view('admin.farmer.create', compact('fpos'));
    }

    public function store(Request $request)
    {
        if (!Gate::any(['employee_manage', 'company_manage', 'users_manage'])) {
            return abort(401);
        }

        if ($request['fig_id']) {
            $figId = $request['fig_id'];
        } else {
            $figId = 0;
        }

        $farmer = farmer::create(
            [
                'fpo_id' => $request['fpo_id'],
                'fig_id' => $figId,
                'employee_id' => Auth::id(),
                'farmer_name' => $request['farmer_name'],
                'father_name' => $request['father_name'],
                'dob' => $request['dob'],
                'email' => $request['email'],
                'address' => $request['address'],
                'contact_no' => $request['contact_no'],
                'block' => $request['block'],
                'village' => $request['village'],
                'district' => $request['district'],
                'state' => $request['state'],
                'gender' => $request['gender'],
                'marital_status' => $request['marital_status'],
                'caste' => $request['caste'],
                'aadhar_number' => $request['aadhar_number'],
                'total_family_no' => $request['total_family_no'],
                'survey_no' => $request['survey_no'],
                'kharsa_no' => $request['kharsa_no'],
                'total_area' => $request['total_area'],
                'irrigated_area' => $request['irrigated_area'],
                'unirrigated_area' => $request['unirrigated_area'],
                'water_source' => $request['water_source'],
                'storage' => $request['storage'],
                'fertilizer_type' => $request['fertilizer_type'],
                'fertilizer_obtain_from' => $request['fertilizer_obtain_from'],
                'land_organic_farming' => $request['land_organic_farming'],
                'organic_farming' =>  $request['organic_farming'],
                'transport' => $request['transport'],

                'bank_account' => $request['bank_account'],
                'farmer_type' => $request['farmer_type'],
                'trainee_required' => $request['trainee_required'],
                'kisan_credit_card' => $request['kisan_credit_card'],
                'loan' => $request['loan'],
                'shg' => $request['shg'],
                'literate' => $request['literate'],


                'seed_buy_from' => $request['seed_buy_from'],
                'manure' => $request['manure'],
                'pesticides_used' => $request['pesticides_used'],
                'cost_of_production' => $request['cost_of_production'],
                'share_price' => $request['share_price'],

                'source_of_income' => $request['source_of_income'],
                'farmer_tools' => $request['farmer_tools'],
                'organic_farming' =>  $request['organic_farming'],
                'isbod' =>  $request['isbod']
            ]
        );

        if ($request['fig_id']) {
            $farmer1 = fig_farmer::create(
                [
                    'fpo_id' => $request['fpo_id'],
                    'fig_id' => $request['fig_id'],
                    'farmer_id' => $farmer->id,
                    'customer_id' => Auth::id(),
                ]
            );
        }

        toastr()->success('Farmer has been saved successfully!');
        return redirect()->route('admin.farmer.index');
    }

    public function edit(farmer $farmer)
    {
        if (!Gate::any(['employee_manage', 'company_manage', 'users_manage'])) {
            return abort(401);
        }

        if (Gate::allows('users_manage')) {
            $fpos = fpo::get();
            // $company = company::where('user_id',Auth::id())->first();

            // $fpos = fpo::where('company_id',$company->id)->get();
            $crops = Crop::with('Season', 'farmer')->where('farmer_id', $farmer->id)->get();

            $farmers = farmer::with('fpo')->get();

            return view('admin.farmer.edit', compact('farmer', 'farmers', 'crops', 'fpos'));
        }

        if (Gate::allows('company_manage')) {

            $company = company::where('user_id', Auth::id())->first();

            $fpos = fpo::where('company_id', $company->id)->get();
            $crops = Crop::with('Season', 'farmer')->where('farmer_id', $farmer->id)->get();

            $farmers = farmer::with('fpo')->get();

            return view('admin.farmer.edit', compact('farmer', 'farmers', 'crops', 'fpos'));
        }
        if (Gate::allows('employee_manage')) {

            $customer = customer::where('user_id', Auth::id())->first();

            $fpos = fpo::where('company_id', $customer->company_id)->get();
            $crops = Crop::with('Season', 'farmer')->where('farmer_id', $farmer->id)->get();

            $farmers = farmer::with('fpo')->get();

            return view('admin.farmer.edit', compact('farmer', 'farmers', 'crops', 'fpos'));
        }
    }

    public function update(Request $request)
    {
        if (!Gate::any(['employee_manage', 'company_manage', 'users_manage'])) {
            return abort(401);
        }
        if ($request['fig_id']) {
            $figId = $request['fig_id'];
        } else {
            $figId = 0;
        }

        $farmer['fpo_id'] = $request['fpo_id'];
        $farmer['fig_id'] = $figId;
        $farmer['employee_id'] = Auth::id();
        $farmer['farmer_name'] = $request['farmer_name'];
        $farmer['dob'] = $request['dob'];
        $farmer['email'] = $request['email'];
        $farmer['address'] = $request['address'];
        $farmer['contact_no'] = $request['contact_no'];
        $farmer['block'] = $request['block'];
        $farmer['village'] = $request['village'];
        $farmer['total_area'] = $request['total_area'];
        $farmer['district'] = $request['district'];
        $farmer['state'] = $request['state'];
        $farmer['irrigated_area'] = $request['irrigated_area'];
        $farmer['unirrigated_area'] = $request['unirrigated_area'];
        $farmer['storage'] = $request['storage'];
        $farmer['kharsa_no'] = $request['kharsa_no'];
        // $farmer['irrigation'] = $request['irrigation'];
        $farmer['water_source'] = $request['water_source'];
        $farmer['transport'] = $request['transport'];
        // $farmer['seed_type'] = $request['seed_type'];
        $farmer['seed_buy_from'] = $request['seed_buy_from'];
        $farmer['total_family_no'] = $request['total_family_no'];
        $farmer['aadhar_number'] = $request['aadhar_number'];
        // $farmer['vegetables_grown'] = $request['vegetables_grown'];
        $farmer['land_organic_farming'] = $request['land_organic_farming'];
        $farmer['manure'] = $request['manure'];
        $farmer['fertilizer_type'] = $request['fertilizer_type'];
        $farmer['fertilizer_obtain_from'] = $request['fertilizer_obtain_from'];
        $farmer['pesticides_used'] = $request['pesticides_used'];
        $farmer['cost_of_production'] = $request['cost_of_production'];
        $farmer['father_name'] = $request['father_name'];
        // $farmer['period'] = $request['period'];
        $farmer['share_price'] = $request['share_price'];
        $farmer['caste'] = $request['caste'];
        $farmer['farmer_type'] = $request['farmer_type'];
        $farmer['gender'] = $request['gender'];
        $farmer['trainee_required'] = $request['trainee_required'];
        $farmer['bank_account'] = $request['bank_account'];
        $farmer['kisan_credit_card'] = $request['kisan_credit_card'];
        $farmer['loan'] = $request['loan'];
        $farmer['shg'] = $request['shg'];
        $farmer['organic_farming'] =  $request['organic_farming'];
        // $farmer['informal_loan'] = 'NULL';
        $farmer['marital_status'] = $request['marital_status'];
        $farmer['literate'] = $request['literate'];
        $farmer['survey_no'] = $request['survey_no'];
        $farmer['source_of_income'] = $request['source_of_income'];
        $farmer['farmer_tools'] = $request['farmer_tools'];
        $farmer['isbod'] = $request['isbod'];
        farmer::where('id', $request['id'])->update($farmer);

        toastr()->success('Farmer has been updated successfully!');
        return redirect()->route('admin.farmer.index');
    }

    public function destroy(farmer $farmer)
    {
        if (!Gate::any(['employee_manage', 'company_manage', 'users_manage'])) {
            return abort(401);
        }

        $farmer->delete();
        toastr()->success('Farmer has been deleted successfully!');
        return redirect()->route('admin.farmer.index');
    }
}
