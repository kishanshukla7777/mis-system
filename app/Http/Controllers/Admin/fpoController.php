<?php

namespace App\Http\Controllers\Admin;

use Intervention\Image\Facades\Image;
use App\company;
use App\Customer;
use App\Fig;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\StoreFpoRequest;
use App\fpo;
use App\fpoother;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\UpdateFpoRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class fpoController extends Controller
{
    /**
     * Display a listing of User.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Gate::any(['users_manage', 'company_manage'])) {
            return abort(401);
        }

        if (Gate::any(['users_manage'])) {
            $fpos = fpo::with('company')->get();

            // dd($fpos);
            return view('admin.fpo.index', compact('fpos'));
        }
        if (Gate::any(['company_manage'])) {
            $company = company::where('user_id', Auth::id())->first();
            $fpos = fpo::with('company')->where('company_id', $company->id)->get();
            return view('admin.fpo.index', compact('fpos'));
        }
        $fpos = fpo::with('company')->get();

        return view('admin.fpo.index', compact('fpos'));
    }

    public function create()
    {
        if (!Gate::any(['users_manage', 'company_manage'])) {
            return abort(401);
        }
        if (Gate::any(['users_manage'])) {
            $companys = company::get();
            $customers = Customer::get();
            // dd($companys);
            return view('admin.fpo.create', compact('companys', 'customers'));
        }
        if (Gate::any(['company_manage'])) {

            $companys = company::where('user_id', Auth::id())->first();
            $customers = Customer::where('company_id', $companys->id)->get();

            return view('admin.fpo.create', compact('companys', 'customers'));
        }

        return view('admin.fpo.create');
    }

    public function store(StoreFpoRequest $request)
    {

        if (!Gate::any(['users_manage', 'company_manage'])) {
            return abort(401);
        }
        if (Gate::any(['users_manage'])) {
            $company = $request['company_id'];
        } else {
            $companyData = company::where('user_id', Auth::id())->first();
            $company = $companyData->id;
        }


        if (!empty($request->file('gst_doc'))) {
            $image = $request->file('gst_doc');
            $gst_doc = 'gst_doc_' . rand() . '.' . $image->getClientOriginalExtension();

            $destinationPath = public_path('uploads/fpo');
            $image->move($destinationPath, $gst_doc);
        } else {
            $gst_doc = NULL;
        }
        if (!empty($request->file('tan_doc'))) {
            $image = $request->file('tan_doc');
            $tan_doc = 'tan_doc' . rand() . '.' . $image->getClientOriginalExtension();

            $destinationPath = public_path('uploads/fpo');
            $image->move($destinationPath, $tan_doc);
        } else {
            $tan_doc = NULL;
        }
        if (!empty($request->file('pan_doc'))) {
            $image = $request->file('pan_doc');
            $pan_doc = 'pan_doc' . rand() . '.' . $image->getClientOriginalExtension();

            $destinationPath = public_path('uploads/fpo');
            $image->move($destinationPath, $pan_doc);
        } else {
            $pan_doc = NULL;
        }
        if (!empty($request->file('cin_doc'))) {
            $image = $request->file('cin_doc');
            $cin_doc = 'cin_doc' . rand() . '.' . $image->getClientOriginalExtension();

            $destinationPath = public_path('uploads/fpo');
            $image->move($destinationPath, $cin_doc);
        } else {
            $cin_doc = NULL;
        }

        $user = fpo::create(
            [
                'company_id' => $company,
                'fpo_name' => $request['fpo_name'],
                'address' => $request['address'],
                'taluka' => $request['taluka'],
                'district' => $request['district'],
                'state' => $request['state'],
                'email' => $request['email'],
                'contact_person' => $request['contact_person'],
                'mobile_no' => $request['mobile_no'],
                'gst_no' => $request['gst_no'],
                'tan_no' =>  $request['tan_no'],
                'pan_no' =>  $request['pan_no'],
                'cin_no' =>  $request['cin_no'],
                'assign_emp' => $request['assign_emp'],
                'gst_doc' => $gst_doc,
                'tan_doc' => $tan_doc,
                'pan_doc' => $pan_doc,
                'cin_doc' => $cin_doc,

            ]
        );

        if (!empty($request->file('other_doc'))) {
            foreach ($request->file('other_doc') as $key => $value) {
                $image =  $value;

                $fpoId = $user->id;
                $other_doc = 'other_doc' . rand() . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('uploads/fpo');
                $image->move($destinationPath, $other_doc);

                $fpoother = new fpoother();
                $data['fpo_id'] = $fpoId;

                $data['name'] = $request->other[$key];

                $data['other_doc'] = $other_doc;
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                $fpoother->insert($data);
            }
        } else if (!empty($request->other)) {
            foreach ($request->other as $key => $value) {
                $fpoother = new fpoother();
                $data['fpo_id'] = 1;
                $data['name'] = $request->other[$key];
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                $fpoother->insert($data);
            }
        }


        toastr()->success('Fpo has been saved successfully!');

        return redirect()->route('admin.fpo.index');
    }

    public function edit(fpo $fpo)
    {
        if (!Gate::any(['users_manage', 'company_manage'])) {
            return abort(401);
        }
        if (Gate::any(['users_manage'])) {
            $companys = company::get();
            $customers = Customer::get();
            $otherDocs = fpoother::where('fpo_id', $fpo->id)->get();

            $this->comapny($fpo->company_id);
            return view('admin.fpo.edit', compact('fpo', 'companys', 'customers', 'otherDocs'));
        }
        if (Gate::any(['company_manage'])) {

            $companys = company::where('user_id', Auth::id())->first();
            $customers = Customer::where('company_id', $fpo->company_id)->get();
            $otherDocs = fpoother::where('fpo_id', $fpo->id)->get();

            return view('admin.fpo.edit', compact('fpo', 'companys', 'customers', 'otherDocs'));
        }

        return view('admin.fpo.edit', compact('fpo'));
    }

    public function update(UpdateFpoRequest $request, fpo $fpo)
    {

        if (!Gate::any(['users_manage', 'company_manage'])) {
            return abort(401);
        }

        if ($request->gst_doc) {
            $image = $request->file('gst_doc');
            $gst_doc = rand() . '.' . $image->getClientOriginalExtension();

            $destinationPath = public_path('uploads/fpo');
            $image->move($destinationPath, $gst_doc);
        } else {
            $gst_doc = $fpo->gst_doc;
        }
        if ($request->tan_doc) {
            $image = $request->file('tan_doc');
            $tan_doc = rand() . '.' . $image->getClientOriginalExtension();

            $destinationPath = public_path('uploads/fpo');
            $image->move($destinationPath, $tan_doc);
        } else {
            $tan_doc = $fpo->tan_doc;
        }
        if ($request->pan_doc) {
            $image = $request->file('pan_doc');
            $pan_doc = rand() . '.' . $image->getClientOriginalExtension();

            $destinationPath = public_path('uploads/fpo');
            $image->move($destinationPath, $pan_doc);
        } else {
            $pan_doc =  $fpo->pan_doc;
        }
        if ($request->cin_doc) {
            $image = $request->file('cin_doc');
            $cin_doc = rand() . '.' . $image->getClientOriginalExtension();

            $destinationPath = public_path('uploads/fpo');
            $image->move($destinationPath, $cin_doc);
        } else {
            $cin_doc =  $fpo->cin_doc;
        }

        $fpos['company_id'] = $fpo->company_id;
        $fpos['fpo_name'] =  $request['fpo_name'];
        $fpos['address'] =  $request['address'];
        $fpos['taluka'] = $request['taluka'];
        $fpos['district'] = $request['district'];
        $fpos['state'] = $request['state'];
        $fpos['email'] = $request['email'];
        $fpos['contact_person'] = $request['contact_person'];
        $fpos['mobile_no'] = $request['mobile_no'];
        $fpos['gst_no'] = $request['gst_no'];
        $fpos['tan_no'] = $request['tan_no'];
        $fpos['pan_no'] = $request['pan_no'];
        $fpos['cin_no'] = $request['cin_no'];
        $fpos['assign_emp'] = $request['assign_emp'];

        $fpos['gst_doc'] = $gst_doc;
        $fpos['tan_doc'] = $tan_doc;
        $fpos['pan_doc'] = $pan_doc;
        $fpos['cin_doc'] = $cin_doc;

        $user = fpo::where('id', $fpo->id)->update($fpos);

        if (!empty($request->file('other_doc'))) {
            foreach ($request->file('other_doc') as $key => $value) {
                $image =  $value;

                $fpoId = $fpo->id;
                $other_doc = 'other_doc' . rand() . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('uploads/fpo');
                $image->move($destinationPath, $other_doc);

                $fpoother = new fpoother();
                $data['fpo_id'] = $fpoId;
                $data['name'] = $request->other[$key];
                $data['other_doc'] = $other_doc;
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                $fpoother->insert($data);
            }
        } else if (!empty($request->other)) {
            foreach ($request->other as $key => $value) {
                $fpoother = new fpoother();
                $data['fpo_id'] = 1;
                $data['name'] = $request->other[$key];
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                $fpoother->insert($data);
            }
        }

        toastr()->success('Fpo has been updated successfully!');
        return redirect()->route('admin.fpo.index');
    }

    public function destroy(fpo $fpo)
    {

        if (!Gate::any(['users_manage', 'company_manage'])) {
            return abort(401);
        }

        fpoother::whereIn('fpo_id', array($fpo->id))->delete();
        $fpo->delete();
        toastr()->success('Fpo has been deleted successfully!');
        return redirect()->route('admin.fpo.index');
    }

    public function massDestroy(Request $request)
    {
        if (!Gate::any(['users_manage', 'company_manage'])) {
            return abort(401);
        }
        fpo::whereIn('id', request('ids'))->delete();
        toastr()->success('Fpo has been deleted successfully!');
        return response()->noContent();
    }

    public function otherdoc($id)
    {
        if (!Gate::any(['users_manage', 'company_manage'])) {
            return abort(401);
        }
        fpoother::where('id', $id)->delete();
    }

    public function comapny($id)
    {
        $emp = Customer::where('company_id', $id)->get();

        return response()->json(["result" => "success", "status" => 200, 'html' => $emp]);
    }

    public function fig($id)
    {
        $emp = Fig::where('fpo_id', $id)->get();

        return response()->json(["result" => "success", "status" => 200, 'html' => $emp]);
    }
}
