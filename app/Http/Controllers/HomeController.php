<?php

namespace App\Http\Controllers;

use App\company;
use App\Customer;
use App\farmer;
use App\Fig;
use App\fpo;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::any(['company_manage'])) {

            $fig = array();
            $farmer = array();
            $maleCount = array();
            $farmerVellage = array();
            $share_price = array();
            $companies = company::where('user_id', Auth::id())->first();

            $fpo = fpo::with('fig', 'farmer')->where('company_id', $companies->id)->get();
            foreach ($fpo as $val) {
                $fig[] =  Fig::where('fpo_id', $val->id)->get();
            }

            foreach ($fpo as $val) {

                $farmer[] =  farmer::where('fpo_id', $val->id)->get();
            }
            if (!empty($farmer)) {
                $farmerCount = $farmer['0']->count();
            } else {
                $farmerCount = 0;
            }
            if (!empty($fig)) {
                $figCount = $fig['0']->count();
            } else {
                $figCount = 0;
            }


            $fpo = fpo::with('fig', 'farmer')->where('company_id', $companies->id)->get();
            $fpoGet = fpo::with('fig', 'farmer')->where('company_id', $companies->id)->get()->pluck('id')->implode(',');
            $fpoGetId = explode(',', $fpoGet);

            $farmer1 = farmer::whereIn('fpo_id', $fpoGetId)->get()->pluck('id')->implode(',');
            $farmerId = explode(',', $farmer1);

            $aaa = implode(',', $farmerId);

            if ($aaa) {
                $farmerVellages = DB::select("select village,MAX(fpo_id) as fpoId,COUNT(village) as villagecount,SUM(`gender` = 'male') as malecount,SUM(`gender` = 'female') as femalecount,
            SUM(`farmer_type` = 'medium') as mediumcount,
            SUM(`farmer_type` = 'large') as largecount,
            SUM(`farmer_type` = 'small') as smallcount,
            SUM(`caste` = 'general') as generalcount,SUM(`caste` = 'obc') as obccount,SUM(`caste` = 'sc') as sccount from farmers where `farmers`.`id` in($aaa) group by village");
            } else {
                $farmerVellages = [];
            }

            $fpoCount = $fpo->count();

            return view('home', compact('farmerVellages', 'share_price', 'fpo', 'farmerCount', 'figCount', 'fpoCount'));
        }

        if (Gate::any(['employee_manage'])) {
            $fig = array();
            $fpo = array();

            $customer = Customer::where('user_id', Auth::id())->first();
            $fpo = fpo::with('fig', 'farmer')
                ->where('assign_emp', $customer->id)
                ->get();

            foreach ($fpo as $val) {
                $farmer =  farmer::where('fpo_id', $val->id)->get();
            }

            $farmer = farmer::where('employee_id', Auth::id())->get();

            foreach ($farmer as $val) {

                $fig[] =  Fig::where('fpo_id', $val->fpo_id)->get();
            }

            if (!empty($farmer)) {
                $farmerCount = $farmer->count();
            } else {
                $farmerCount = 0;
            }
            if (!empty($fig)) {
                $figCount = count($fig);
            } else {
                $figCount = 0;
            }
            if (!empty($fpo)) {
                $fpoCount = $fpo->count();
            } else {
                $fpoCount = 0;
            }
            return view('home', compact('fpo', 'farmerCount', 'figCount', 'fpoCount'));
            //return view('home');
        }
        return view('home');
    }
}
