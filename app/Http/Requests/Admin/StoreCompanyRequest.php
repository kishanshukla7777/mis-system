<?php
namespace App\Http\Requests\Admin;
use App\Http\Requests\Admin\StoreCustomerRequest;
use Illuminate\Foundation\Http\FormRequest;

class StoreCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
            'company_name' => 'required',
            // 'contact_person' => 'required',
            'address' => 'required',
            'contact_no' => 'required|unique:companies,contact_no',
            'roles' => 'required'
        ];
    }
}
