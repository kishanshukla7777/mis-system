<?php
namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class StoreCropRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'crop_name' => 'required',
           /* 'area' => '',
            'total_production' => '',
            'average_yield' => '',
            'direct' => '',
            'through_intermediaries' => '',
            'total' => '' */
        ];
    }
}
