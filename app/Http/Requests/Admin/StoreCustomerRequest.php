<?php
namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class StoreCustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'name' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'company_id' => '',
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
            'address' => 'required',
            'mobile_no' => 'required|unique:customers,mobile_no',
            'roles' => 'required'
        ];
    }
}
