<?php
namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class StoreFpoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'fpo_name' => 'required',
            // 'address' => 'required',
            // 'taluka' => 'required',
            // 'district' => 'required',
            // 'state' => 'required',
            'assign_emp' => 'required',
            'email' => 'required',
           // 'contact_person' => 'required',
           'mobile_no' => 'required',
           
            // "gst_no" => '',
            // "tan_no" => '',
            // "pan_no" => '',
            // "cin_no" => '',
            'gst_doc' => 'mimes:pdf,jpg,png|max:5125',
            'tan_doc' => 'mimes:pdf,jpg,png|max:5125',
            'pan_doc' => 'mimes:pdf,jpg,png|max:5125',
            'cin_doc' => 'mimes:pdf,jpg,png|max:5125',
            '*.other' => '',
            '*.other_doc' =>'mimes:pdf,jpg,png|max:5125',
        ];
    }
}
