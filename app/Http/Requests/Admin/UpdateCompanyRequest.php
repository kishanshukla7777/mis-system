<?php
namespace App\Http\Requests\Admin;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;

class UpdateCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required', 'email', \Illuminate\Validation\Rule::unique('companies')->ignore($this->user()->id),
           // 'email' => 'required|email|unique:users,email,'.$this->route('user')->id,
         
            'company_name' => 'required',
            // 'contact_person' => 'required',
            'address' => 'required',
            'contact_no' => 'required', 'contact_no', \Illuminate\Validation\Rule::unique('companies')->ignore($this->user()->id),
           // 'contact_no' => 'required|unique:companies,contact_no',
          
        ];
      
    }
}
