<?php
namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;

class UpdateCustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            // 'name' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'company_id' => '',
            'email' => 'required', 'email', \Illuminate\Validation\Rule::unique('users')->ignore($this->user()->id),
            'address' => 'required',
            'mobile_no' => 'required', 'mobile_no', \Illuminate\Validation\Rule::unique('customers')->ignore($this->user()->id),
         ];
    }
}
