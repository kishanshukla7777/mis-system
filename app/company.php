<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\Model;

class company extends Model
{
    use Notifiable;
    use HasRoles;

    protected $fillable = ['user_id', 'company_name', 'address', 'contact_person','contact_no'];
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
