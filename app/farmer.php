<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class farmer extends Model
{

    protected $fillable = [
        'fpo_id', 'fig_id', 'employee_id', 'farmer_name',
        'dob', 'email', 'address', 'contact_no', 'block', 'village', 'city', 'district',
        'state', 'irrigated_area', 'unirrigated_area', 'infrastructure_detail', 'vegetables_grown',
        'aadhar_number', 'kharsa_no', 'irrigation', 'water_source', 'transport', 'seed_type', 'seed_buy_from', 'dependents', 'land_organic_farming', 'manure', 'fertilizer_type', 'fertilizer_obtain_from',
        'pesticides_used', 'cost_of_production', 'father_name', 'period', 'share_price', 'caste',
        'farmer_type', 'gender', 'trainee_required', 'bank_account', 'kisan_credit_card',
        'shg', 'loan', 'informal_loan', 'marital_status', 'literate', 'organic_farming', 'total_area', 'survey_no', 'source_of_income', 'farmer_tools', 'storage', 'total_family_no', 'isbod'
    ];

    public function fpo()
    {
        return $this->belongsTo(fpo::class);
    }
    public function employee()
    {
        return $this->belongsTo(Customer::class);
    }
    public function fig_farmer()
    {
        return $this->hasOne(fig_farmer::class);
    }
}
