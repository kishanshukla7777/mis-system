<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class fig_farmer extends Model
{   
    protected $table = 'fig_farmer';
    protected $primaryKey = 'id';
    protected $fillable = ['fpo_id', 'fig_id', 'farmer_id','customer_id'];

    public function fpo()
    {
        return $this->belongsTo(fpo::class);
    }

    public function fig(){
        return $this->belongsTo(Fig::class);
    }
    public function farmer(){
        return $this->belongsTo(farmer::class);
    }
  
}
