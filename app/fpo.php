<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\Model;

class fpo extends Model
{
    use Notifiable;
    use HasRoles;

    protected $fillable = ['company_id', 'fpo_name', 'address',
     'taluka','district','state','email',
     'contact_person','mobile_no',
     'gst_no','gst_doc',
     'tan_no','tan_doc','pan_no','pan_doc','cin_no','cin_doc','assign_emp'];
    
    public function company()
    {
        return $this->belongsTo(company::class);
    }

    public function fig(){
        return $this->hasMany(Fig::class);
    }
    public function farmer(){
        return $this->hasMany(farmer::class);
    }
    public function farmer1(){
        return $this->belongsTo(farmer::class);
    }
}
