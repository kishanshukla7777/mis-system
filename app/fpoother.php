<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class fpoother extends Model
{
    protected $table = 'fpoothers';
    protected $primaryKey = 'id';
    protected $fillable = ['fpo_id', 'name', 'other_doc'];
}
