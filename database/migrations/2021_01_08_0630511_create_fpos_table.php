<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fpos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->string('fpo_name');
            $table->text('address');
            $table->string('taluka');
            $table->string('district');
            $table->string('state');
            $table->string('email');
            $table->string('contact_person');
            $table->string('mobile_no', 20)->unique();
            $table->string('gst_no')->nullable();
            $table->text('gst_doc')->nullable();

            $table->string('tan_no')->nullable();
            $table->text('tan_doc')->nullable();

            $table->string('pan_no')->nullable();
            $table->text('pan_doc')->nullable();

            $table->string('cin_no')->nullable();
            $table->text('cin_doc')->nullable();

            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fpos');
    }
}
