<?php

use Doctrine\DBAL\Schema\Table;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFarmersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('farmers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('fpo_id')->nullable();
            $table->string('employee_id')->nullable();
            $table->string('farmer_name')->nullable();
            $table->string('dob')->nullable();
            $table->string('email')->nullable();
            $table->text('address')->nullable();
            $table->string('contact_no')->nullable();
            $table->string('block')->nullable();
            $table->string('village')->nullable();
            $table->string('city')->nullable();
            $table->string('district')->nullable();
            $table->string('state')->nullable();
            $table->string('irrigated_area')->nullable();
            $table->string('unirrigated_area')->nullable();
            $table->string('infrastructure_detail')->nullable();
            $table->string('vegetables_grown')->nullable();
            $table->string('aadhar_number')->nullable();
            $table->string('kharsa_no')->nullable();
            $table->string('irrigation')->nullable();
            $table->string('water_source')->nullable();
            $table->string('transport')->nullable();
            $table->string('seed_type')->nullable();
            $table->string('seed_buy_from')->nullable();
            $table->string('dependents')->nullable();
            $table->string('land_organic_farming')->nullable();
            $table->string('manure')->nullable();
            $table->string('fertilizer_type')->nullable();
            $table->string('fertilizer_obtain_from')->nullable();
            $table->string('pesticides_used')->nullable();
            $table->string('loan')->nullable();
            $table->string('cost_of_production')->nullable();
            $table->string('father_name')->nullable();
            $table->string('period')->nullable();

            $table->string('caste')->nullable();
            $table->string('farmer_type')->nullable();
            $table->string('gender')->nullable();
            $table->string('trainee_required')->nullable();
            $table->string('bank_account')->nullable();
            $table->string('kisan_credit_card')->nullable();
            $table->string('shg')->nullable();
            $table->string('informal_loan')->nullable();
            $table->string('marital_status')->nullable();
            $table->string('literate')->nullable();
            $table->string('organic_farming')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('farmers');
    }
}
