<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCropsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crops', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('farmer_id')->nullable();
            $table->string('season_id')->comment('0 =Kharif/Monsoon ,1 =Rabi/ Winter , 2=Zayad/ Summer')->nullable();
            $table->string('crop_name')->nullable();
            $table->string('area')->nullable();
            $table->string('total_production')->nullable();
            $table->string('average_yield')->nullable();
            $table->string('direct')->nullable();
            $table->string('through_intermediaries')->nullable();
            $table->string('total')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crops');
    }
}
