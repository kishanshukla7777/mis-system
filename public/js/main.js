$(document).ready(function () {
 
  window._token = $('meta[name="csrf-token"]').attr('content')

  var allEditors = document.querySelectorAll('.ckeditor');
  for (var i = 0; i < allEditors.length; ++i) {
    ClassicEditor.create(
      allEditors[i],
      {
        removePlugins: ['ImageUpload']
      }
    );
  }

  moment.updateLocale('en', {
    week: { dow: 1 } // Monday is the first day of the week
  })

  $('.date').datetimepicker({
    format: 'YYYY-MM-DD',
    locale: 'en'
  })

  $('.datetime').datetimepicker({
    format: 'YYYY-MM-DD HH:mm:ss',
    locale: 'en',
    sideBySide: true
  })

  $('.timepicker').datetimepicker({
    format: 'HH:mm:ss'
  })

  $('.select-all').click(function () {
    let $select2 = $(this).parent().siblings('.select2')
    $select2.find('option').prop('selected', 'selected')
    $select2.trigger('change')
  })
  $('.deselect-all').click(function () {
    let $select2 = $(this).parent().siblings('.select2')
    $select2.find('option').prop('selected', '')
    $select2.trigger('change')
  })

  $('.select2').select2()

  $('.treeview').each(function () {
    var shouldExpand = false
    $(this).find('li').each(function () {
      if ($(this).hasClass('active')) {
        shouldExpand = true
      }
    })
    if (shouldExpand) {
      $(this).addClass('active')
    }
  })
  $("#mywinterModal").click(function () {
    $("#mywinterInput").modal('show');

  });
  $("#mysummerModal").click(function () {
    $("#mysummerInput").modal('show');

  });
  $("#mymonsoonModal").click(function () {
    $("#mymonsoonInput").modal('show');

  });

  $("#addmonsoon").click(function () {

    $crop_name = $("#crop_name").val();
    $area = $("#area").val();
    $total_production = $("#total_production").val();
    $average_yield = $("#average_yield").val();
    $direct = $("#direct").val();
    $through_intermediaries = $("#through_intermediaries").val();
    $total = $("#total").val();
    console.log($crop_name, $area, $average_yield, $direct, $through_intermediaries, $total);
    $('#mymonsoonInput').modal('hide');

    row = "<tr><td>" + $crop_name + "</td><td>" + $area + "</td><td>" + $total_production + "</td><td>" + $average_yield + "</td><td>" + $direct + "</td><td>" + $through_intermediaries + "</td><td>" + $total + "</td></tr>";

    $("#table").append(row);

    document.getElementById("crop_name").value = "";
    document.getElementById("area").value = "";
    document.getElementById("total_production").value = "";
    document.getElementById("average_yield").value = "";
    document.getElementById("direct").value = "";
    document.getElementById("through_intermediaries").value = "";
    document.getElementById("total").value = "";
  });

  $("#addwinter").click(function () {

    $crop_name = $("#crop_namew").val();
    $area = $("#areaw").val();
    $total_production = $("#total_productionw").val();
    $average_yield = $("#average_yieldw").val();
    $direct = $("#directw").val();
    $through_intermediaries = $("#through_intermediariesw").val();
    $total = $("#totalw").val();
    console.log($crop_name, $area, $average_yield, $direct, $through_intermediaries, $total);


    row = "<tr><td>" + $crop_name + "</td><td>" + $area + "</td><td>" + $total_production + "</td><td>" + $average_yield + "</td><td>" + $direct + "</td><td>" + $through_intermediaries + "</td><td>" + $total + "</td></tr>";

    $("#tablew").append(row);

    document.getElementById("crop_namew").value = "";
    document.getElementById("areaw").value = "";
    document.getElementById("total_productionw").value = "";
    document.getElementById("average_yieldw").value = "";
    document.getElementById("directw").value = "";
    document.getElementById("through_intermediariesw").value = "";
    document.getElementById("totalw").value = "";
  });
  $("#addsummer").click(function () {

    $crop_name = $("#crop_names").val();
    $area = $("#areas").val();
    $total_production = $("#total_productions").val();
    $average_yield = $("#average_yields").val();
    $direct = $("#directs").val();
    $through_intermediaries = $("#through_intermediariess").val();
    $total = $("#totals").val();
    console.log($crop_name, $area, $average_yield, $direct, $through_intermediaries, $total);


    row = "<tr><td>" + $crop_name + "</td><td>" + $area + "</td><td>" + $total_production + "</td><td>" + $average_yield + "</td><td>" + $direct + "</td><td>" + $through_intermediaries + "</td><td>" + $total + "</td></tr>";

    $("#tables").append(row);

    document.getElementById("crop_names").value = "";
    document.getElementById("areas").value = "";
    document.getElementById("total_productions").value = "";
    document.getElementById("average_yields").value = "";
    document.getElementById("directs").value = "";
    document.getElementById("through_intermediariess").value = "";
    document.getElementById("totals").value = "";
  });

  
  
});
$( "#addotherDoc" ).click(function() {

  var newElement = ' <div class="form-group row align-items-end otherAppend" style="float: left;width: 100%;margin: 0px 0px;"><div class="col" style="float: left;width: 100%;margin: 13px 0px;"><label for="other">Other</label><input type="text" name="other[]" id="other" class="form-control"></div><div class="input-group mb-3 col"><div class="custom-file"><div class="form-group @if ($errors->has("other_doc")) has-error @endif "><input type="file" name="other_doc[]" class="custom-file-input form-control"><label class="custom-file-label" for="other_doc">Choose file</label></div></div></div></div>';
  $( ".otherAppendData" ).append( $(newElement) );
  $('input[type="file"]').change(function(e){
    var fileName = e.target.files[0].name;
    $(e.target).siblings('.custom-file-label').html(fileName);
 
});
});

   $(".removeDoc").click(function() {
    var removedoc = $('#removedoc').val();
    $.ajax({
        type: 'get',
        url: APP_URL+'/admin/otherdoc/' + removedoc,
        success: function(response) {
          location.reload();
        }
    });
});

$(document).ready(function(){
  $('input[type="file"]').change(function(e){
    var fileName = e.target.files[0].name;
    $(e.target).siblings('.custom-file-label').html(fileName);
 
});
});

$("#company_id").change(function() {
  var company_id = $('#company_id').val(); 
  $.ajax({
      type: 'get',
      url: APP_URL+'/admin/fpo/comapny/' + company_id,
      success: function(response) {
        if (response.result == 'success') {
            $('.purchaseRemove').remove();
            $.each(response.html, function(key, value) {
                $('#assign_emp1').append('<option class="purchaseRemove" value="' + value['id'] + '">' + value['first_name'] + '</option>');
            });
        }
    }
  });
});


$(".company_id").change(function() {
  var company_id = $('.company_id').val(); 
  $.ajax({
      type: 'get',
      url: APP_URL+'/admin/fpo/comapny/' + company_id,
      success: function(response) {
        console.log(response);
        if (response.result == 'success') {
            $('.purchaseRemove').remove();
            $.each(response.html, function(key, value) {
                $('#assign_emp1').append('<option class="purchaseRemove" value="' + value['id'] + '">' + value['first_name'] + '</option>');
            });
        }
    }
  });
});

$("#company_idedit").ready(function() {
 
    var company_id = $('#company_idedit').val(); 
    var assign_emp_edit = $('#assign_emp_edit').val();
   
    $.ajax({
        type: 'get',
        url: APP_URL+'/admin/fpo/comapny/' + company_id,
        success: function(response) {
         
          if (response.result == 'success') {
              $('.purchaseRemove').remove();
            
              $.each(response.html, function(key, value) {
             
                if (value['id'] == assign_emp_edit) {
                  var option = 'selected';
                }else{
                  var option = 'sss';
                }
                  $('#assign_emp1').append('<option class="purchaseRemove" '+ option +' value="' + value['id'] + '">' + value['first_name'] + '</option>');
              });
          }
      }
    });
   
  
});

$("#fpo_id").change(function() {
  var fpo_id = $('#fpo_id').val(); 
  $.ajax({
      type: 'get',
      url: APP_URL+'/admin/fpo/fig/' + fpo_id,
      success: function(response) {
        console.log(response);
        if (response.result == 'success') {
            $('.purchaseRemove1').remove();
            $.each(response.html, function(key, value) {
                $('#fig_id').append('<option class="purchaseRemove1" value="' + value['id'] + '">' + value['name'] + '</option>');
            });
        }
    }
  });
});


$("#fig_idEdit").ready(function() {
 
  var fpo_id = $('#fpo_id').val(); 
  var fig_emp_edit = $('#fig_emp_edit').val();
 
  $.ajax({
      type: 'get',
      url: APP_URL+'/admin/fpo/fig/' + fpo_id,
      success: function(response) {
       
        if (response.result == 'success') {
            $('.purchaseRemove1').remove();
          
            $.each(response.html, function(key, value) {
           
              if (value['id'] == fig_emp_edit) {
                var option = 'selected';
              }else{
                var option = 'sss';
              }
                $('#fig_idEdit').append('<option class="purchaseRemove1" '+ option +' value="' + value['id'] + '">' + value['name'] + '</option>');
            });
        }
    }
  });
 

});

