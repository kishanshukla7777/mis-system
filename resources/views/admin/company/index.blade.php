@extends('layouts.admin')
@section('content')
@can('users_manage')
<div style="margin-bottom: 10px;" class="row">
    <div class="col-lg-12">
        <a class="btn btn-success" href="{{ route("admin.company.create") }}">
            {{ trans('global.add') }} {{ trans('cruds.company.title_singular') }}
        </a>
    </div>
</div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('cruds.company.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                <thead>
                    <tr>
                        <th class="hide">
                        </th>
                        <th>
                            {{ trans('cruds.company.fields.id') }}
                        </th>
                        <th>
                            Company Name
                        </th>
                        <th>
                            Contact Person Name
                        </th>

                        <th>
                            {{ trans('cruds.company.fields.email') }}
                        </th>

                        <th>
                            {{ trans('cruds.company.fields.contact_no') }}
                        </th>

                        <th class="hide">
                            Address
                        </th>
                        <th class="hide">
                            Contact Person
                        </th>
                        <th>
                            {{ trans('cruds.company.fields.roles') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($companies as $key => $company)

                    <tr data-entry-id="{{ $company->id }}">
                        <td class="hide">
                        </td>
                        <td>
                            {{ $company->id ?? '' }}
                        </td>
                        <td>
                            {{ $company->company_name ?? '' }}
                        </td>
                        <td>
                            {{ $company->user->name ?? '' }}
                        </td>

                        <td>
                            {{ $company->user->email ?? '' }}
                        </td>

                        <td>
                            {{ $company->contact_no ?? '' }}
                        </td>
                        <td class="hide">
                            {{ $company->address ?? '' }}
                        </td>
                        <td class="hide">
                            {{ $company->contact_person ?? '' }}
                        </td>

                        <td>
                            @foreach($company->user->roles()->pluck('name') as $role)
                            <span class="badge badge-info">{{ $role }}</span>
                            @endforeach
                        </td>
                        <td>
                            <!-- <a class="btn btn-xs btn-primary" href="{{ route('admin.company.show', $company->id) }}">
                                    {{ trans('global.view') }}
                                </a> -->

                            <a class="btn btn-xs btn-info" href="{{ route('admin.company.edit', $company->id) }}">
                                {{ trans('global.edit') }}
                            </a>

                            <form action="{{ route('admin.company.destroy', $company->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                <input type="hidden" name="_method" value="DELETE">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                            </form>

                        </td>

                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>


    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    $(function() {
        let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)

        //   let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
        //   let deleteButton = {
        //     text: deleteButtonTrans,
        //     url: "{{ route('admin.company.mass_destroy') }}",
        //     className: 'btn-danger',
        //     action: function (e, dt, node, config) {
        //       var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
        //           return $(entry).data('entry-id')
        //       });

        //       if (ids.length === 0) {
        //         alert('{{ trans('global.datatables.zero_selected') }}')

        //         return
        //       }

        //       if (confirm('{{ trans('global.areYouSure') }}')) {
        //         $.ajax({
        //           headers: {'x-csrf-token': _token},
        //           method: 'POST',
        //           url: config.url,
        //           data: { ids: ids, _method: 'DELETE' }})
        //           .done(function () { location.reload() })
        //       }
        //     }
        //   }
        //   dtButtons.push(deleteButton)


        $.extend(true, $.fn.dataTable.defaults, {
            order: [
                [1, 'desc']
            ],
            pageLength: 10,
        });
        $('.datatable-User:not(.ajaxTable)').DataTable({
            buttons: dtButtons
        })
        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
            $($.fn.dataTable.tables(true)).DataTable()
                .columns.adjust();
        });
    })
</script>
@endsection