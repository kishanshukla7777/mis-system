@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.crop.title_singular') }}
    </div>

    <div class="card-body">
        <form action="{{ route("admin.crop.store") }}" method="POST" id="corpForm" enctype="multipart/form-data">
            @csrf
            <div class="form-group {{ $errors->has('season') ? 'has-error' : '' }}">
                <label for="season_id">season*</label>
                <select name="season_id" id="season_id" class="form-control">
                    <option selected disabled>Please select season</option>
                    @foreach ($cropData as $key => $cropDatas)
                    <option value="{{$cropDatas->id }}" {{ !empty(old('season_id')) && old('season_id') == $cropDatas->id
                            ? 'selected="selected"'
                            : ((!empty($crop) && !empty($crop->season_id)) && $crop->season_id == $cropDatas->id
                                ? 'selected="selected"'
                                : "")
                        }}>{{ $cropDatas->name }}</option>
                    @endforeach

                </select>

                @if($errors->has('season_id'))
                <em class="invalid-feedback">
                    {{ $errors->first('season_id') }}
                </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.roles_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('crop_name') ? 'has-error' : '' }}">
                <label for="crop_name">{{ trans('cruds.crop.fields.crop_name') }}*</label>
                <input type="hidden" name="farmer_id" value="{{$id}}">
                <input type="text" id="crop_name" name="crop_name" class="form-control" value="{{ old('crop_name', isset($crop) ? $crop->crop_name : '') }}" required>
                @if($errors->has('crop_name'))
                <em class="invalid-feedback">
                    {{ $errors->first('crop_name') }}
                </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.crop.fields.crop_name_helper') }}
                </p>
            </div>


            <div class="form-group {{ $errors->has('area') ? 'has-error' : '' }}">
                <label for="area">Area (Hectare)</label>
                <input type="text" id="area" name="area" class="form-control" value="{{ old('area', isset($crop) ? $crop->area : '') }}" required>
                @if($errors->has('area'))
                <em class="invalid-feedback">
                    {{ $errors->first('area') }}
                </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.crop.fields.area_helper') }}
                </p>
            </div>

            <div class="form-group {{ $errors->has('total_production') ? 'has-error' : '' }}">
                <label for="total_production"> Production (Kg)</label>
                <input type="text" id="total_production" name="total_production" class="form-control" value="{{ old('total_production', isset($crop) ? $crop->total_production : '') }}" required>
                @if($errors->has('total_production'))
                <em class="invalid-feedback">
                    {{ $errors->first('total_production') }}
                </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.crop.fields.total_production_helper') }}
                </p>
            </div>

            <div class="form-group {{ $errors->has('average_yield') ? 'has-error' : '' }}">
                <label for="average_yield">Average Yield (Kg/Hectare)</label>
                <input type="text" id="average_yield" name="average_yield" class="form-control" value="{{ old('average_yield', isset($crop) ? $crop->average_yield : '') }}" required>
                @if($errors->has('average_yield'))
                <em class="invalid-feedback">
                    {{ $errors->first('average_yield') }}
                </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.crop.fields.average_yield_helper') }}
                </p>
            </div>

            <div class="form-group {{ $errors->has('direct') ? 'has-error' : '' }}">
                <label for="direct">{{ trans('cruds.crop.fields.direct') }}</label>
                <input type="text" id="direct" name="direct" class="form-control" value="{{ old('direct', isset($crop) ? $crop->direct : '') }}" required>
                @if($errors->has('direct'))
                <em class="invalid-feedback">
                    {{ $errors->first('direct') }}
                </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.crop.fields.direct_helper') }}
                </p>
            </div>

            <div class="form-group {{ $errors->has('through_intermediaries') ? 'has-error' : '' }}">
                <label for="through_intermediaries">{{ trans('cruds.crop.fields.through_intermediaries') }}</label>
                <input type="text" id="through_intermediaries" name="through_intermediaries" class="form-control" value="{{ old('through_intermediaries', isset($crop) ? $crop->through_intermediaries : '') }}" required>
                @if($errors->has('through_intermediaries'))
                <em class="invalid-feedback">
                    {{ $errors->first('through_intermediaries') }}
                </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.crop.fields.through_intermediaries_helper') }}
                </p>
            </div>

            <div class="form-group {{ $errors->has('total') ? 'has-error' : '' }}">
                <label for="total">{{ trans('cruds.crop.fields.total') }}</label>
                <input type="text" id="total" name="total" class="form-control" value="{{ old('total', isset($crop) ? $crop->total : '') }}" required>
                @if($errors->has('total'))
                <em class="invalid-feedback">
                    {{ $errors->first('total') }}
                </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.crop.fields.total_helper') }}
                </p>
            </div>

            <div style="float:left; margin-right:15px;">
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
            <div style="float:left;">
                <a class="btn btn-primary" href="{{ route('admin.farmer.edit', $id) }}">Back</a>
            </div>
        </form>


    </div>
</div>
@endsection