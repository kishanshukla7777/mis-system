@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.customer.title_singular') }}
    </div>

    <div class="card-body">
        <form id="customerFormedit" action="{{ route("admin.customer.update", [$customer->id]) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            @canany(['users_manage'])
            <div class="row">
                <input type="hidden" name="id" value="{{$customer->id}}">
                <input type="hidden" name="user_id" value="{{$customer->user->id}}">
            <div class="form-group col{{ $errors->has('company_id') ? 'has-error' : '' }}">
                <label for="company_id">Company</label>
                <select  name="company_id" id="company_id" class="form-control">
                        <option selected disabled>Please select Company</option>
                        @foreach ($companys as $key => $company)
                      
                        <option value="{{ $company->id }}" <?php if($customer->company_id == $company->id) { ?> selected <?php } ?>>{{ $company->company_name }}</option>
                    @endforeach

                    </select>
                  
                @if($errors->has('fpo_id'))
                    <em class="invalid-feedback">
                        {{ $errors->first('fpo_id') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.roles_helper') }}
                </p>
            </div>
            </div>
            @endcanany
            {{-- <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name">{{ trans('cruds.user.fields.name') }}*</label>
                <input type="hidden" name="id" value="{{$customer->id}}">
                <input type="hidden" name="user_id" value="{{$customer->user->id}}">
                <input type="text" id="name" name="name" class="form-control" value="{{ old('name', isset($customer) ? $customer->user->name : '') }}">
                @if($errors->has('name'))
                    <em class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div> --}}
            <div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
                <label for="first_name">{{ trans('cruds.customer.fields.first_name') }}*</label>
                <input type="text" id="first_name" name="first_name" class="form-control" value="{{ old('first_name', isset($customer) ? $customer->first_name : '') }}">
                @if($errors->has('first_name'))
                    <em class="invalid-feedback">
                        {{ $errors->first('first_name') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.customer.fields.first_name_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
                <label for="last_name">{{ trans('cruds.customer.fields.last_name') }}*</label>
                <input type="text" id="last_name" name="last_name" class="form-control" value="{{ old('last_name', isset($customer) ? $customer->last_name : '') }}">
                @if($errors->has('last_name'))
                    <em class="invalid-feedback">
                        {{ $errors->first('last_name') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.customer.fields.last_name_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                <label for="email">{{ trans('cruds.user.fields.email') }}*</label>
                <input type="email" id="email" name="email" class="form-control" value="{{ old('email', isset($customer) ? $customer->user->email : '') }}">
                @if($errors->has('email'))
                    <em class="invalid-feedback">
                        {{ $errors->first('email') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.email_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                <label for="password">{{ trans('cruds.user.fields.password') }}</label>
                <input type="password" id="password" name="password" class="form-control">
                @if($errors->has('password'))
                    <em class="invalid-feedback">
                        {{ $errors->first('password') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.password_helper') }}
                </p>
            </div>

            <div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
                <label for="address">{{ trans('cruds.customer.fields.address') }}*</label>
                <input type="text" id="address" name="address" class="form-control" value="{{ old('address', isset($customer) ? $customer->address : '') }}">
                @if($errors->has('address'))
                    <em class="invalid-feedback">
                        {{ $errors->first('address') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.customer.fields.address_helper') }}
                </p>
            </div>

            <div class="form-group {{ $errors->has('mobile_no') ? 'has-error' : '' }}">
                <label for="mobile_no">{{ trans('cruds.customer.fields.mobile_no') }}*</label>
                <input type="text" id="mobile_no" name="mobile_no" class="form-control" value="{{ old('mobile_no', isset($customer) ? $customer->mobile_no : '') }}">
                @if($errors->has('mobile_no'))
                    <em class="invalid-feedback">
                        {{ $errors->first('mobile_no') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.customer.fields.mobile_no_helper') }}
                </p>
            </div>
            <!-- <div class="form-group {{ $errors->has('roles') ? 'has-error' : '' }}">
                <label for="roles">{{ trans('cruds.customer.fields.roles') }}*
                    <span class="btn btn-info btn-xs select-all">{{ trans('global.select_all') }}</span>
                    <span class="btn btn-info btn-xs deselect-all">{{ trans('global.deselect_all') }}</span></label>
                <select name="roles[]" id="roles" class="form-control select2" multiple="multiple" required>
                    @foreach($roles as $id => $roles)
                        <option value="{{ $id }}" {{ (in_array($id, old('roles', [])) || isset($customer) && $customer->user->roles()->pluck('name', 'id')->contains($id)) ? 'selected' : '' }}>{{ $roles }}</option>
                    @endforeach
                </select>
                @if($errors->has('roles'))
                    <em class="invalid-feedback">
                        {{ $errors->first('roles') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.roles_helper') }}
                </p>
            </div> -->
            <div style="float:left; margin-right:15px;">
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
            <div style="float:left;">
            <a class="btn btn-primary" href="{{ route('admin.customer.index') }}">Back</a>
        </div>
        </form>


    </div>
</div>
@endsection