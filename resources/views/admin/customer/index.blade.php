@extends('layouts.admin')
@section('content')
@canany(['users_manage','company_manage'])
<div style="margin-bottom: 10px;" class="row">
    <div class="col-lg-12">
        <a class="btn btn-success" href="{{ route("admin.customer.create") }}">
            {{ trans('global.add') }} {{ trans('cruds.customer.title_singular') }}
        </a>
    </div>
</div>
@endcanany
<div class="card">
    <div class="card-header">
        {{ trans('cruds.customer.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                <thead>
                    <tr>
                        <th class="hide">
                        </th>
                        <th>
                            {{ trans('cruds.customer.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.customer.fields.company') }}
                        </th>
                        <th>
                            {{ trans('cruds.customer.fields.first_name') }}
                        </th>
                        <th>
                            {{ trans('cruds.customer.fields.last_name') }}
                        </th>
                        <th>
                            {{ trans('cruds.customer.fields.email') }}
                        </th>
                        <th>
                            {{ trans('cruds.customer.fields.mobile_no') }}
                        </th>
                        <th class="hide">
                            Address
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($customers as $key => $customer)
                    <tr data-entry-id="{{ $customer->id }}">
                        <td class="hide">
                        </td>
                        <td>
                            {{ $customer->id ?? '' }}
                        </td>
                        <td>
                            {{ $customer->company->company_name ?? '' }}
                        </td>
                        <td>
                            {{ $customer->first_name ?? '' }}
                        </td>
                        <td>
                            {{ $customer->last_name ?? '' }}
                        </td>
                        <td>
                            {{ $customer->user->email ?? '' }}
                        </td>
                        <td>
                            {{ $customer->mobile_no ?? '' }}
                        </td>
                        <td class="hide">
                            {{ $customer->address ?? '' }}
                        </td>
                        <td>
                            @canany(['users_manage','company_manage'])
                            <a class="btn btn-xs btn-info" href="{{ route('admin.customer.edit', $customer->id) }}">
                                {{ trans('global.edit') }}
                            </a>

                            <form action="{{ route('admin.customer.destroy', $customer->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                <input type="hidden" name="_method" value="DELETE">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                            </form>
                            @endcanany
                        </td>

                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>


    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    $(function() {
        let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)

        //   let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
        //   let deleteButton = {
        //     text: deleteButtonTrans,
        //     url: "{{ route('admin.users.mass_destroy') }}",
        //     className: 'btn-danger',
        //     action: function (e, dt, node, config) {
        //       var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
        //           return $(entry).data('entry-id')
        //       });

        //       if (ids.length === 0) {
        //         alert('{{ trans('global.datatables.zero_selected') }}')

        //         return
        //       }

        //       if (confirm('{{ trans('global.areYouSure') }}')) {
        //         $.ajax({
        //           headers: {'x-csrf-token': _token},
        //           method: 'POST',
        //           url: config.url,
        //           data: { ids: ids, _method: 'DELETE' }})
        //           .done(function () { location.reload() })
        //       }
        //     }
        //   }
        //   dtButtons.push(deleteButton)


        $.extend(true, $.fn.dataTable.defaults, {
            order: [
                [1, 'desc']
            ],
            pageLength: 10,
        });
        $('.datatable-User:not(.ajaxTable)').DataTable({
            buttons: dtButtons
        })
        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
            $($.fn.dataTable.tables(true)).DataTable()
                .columns.adjust();
        });
    })
</script>
@endsection