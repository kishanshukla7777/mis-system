@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.farmer.title_singular') }}
    </div>

    <div class="card-body">

        <form action="{{ route("admin.farmer.store") }}" id="farmerForm" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="border rounded m-1">
                <div class="card-header">
                    {{ trans('cruds.farmer.title_farmer_detailse') }}
                </div>
                <div class="row p-2 ">
                    <div class="row col">
                        <div class="form-group col-sm-3{{ $errors->has('fpo_id') ? 'has-error' : '' }}">
                            <label for="fpo_id">Fpo*</label>
                            <select name="fpo_id" id="fpo_id" class="form-control" tabindex="1">
                                <option selected disabled>Please select fpo</option>
                                @foreach ($fpos as $key => $fpo)
                                <option value="{{ $fpo->id }}" {{ !empty(old('fpo_id')) && old('fpo_id') == $value->id
                            ? 'selected="selected"'
                            : ((!empty($farmer) && !empty($farmer->fpo_id)) && $fpo->fpo_id == $fpo->id
                                ? 'selected="selected"'
                                : "")
                        }}>{{ $fpo->fpo_name }}</option>
                                @endforeach

                            </select>
                            <span class=errorfpo_id></span>
                            @if($errors->has('fpo_id'))
                            <em class="invalid-feedback">
                                {{ $errors->first('fpo_id') }}
                            </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.user.fields.roles_helper') }}
                            </p>
                        </div>

                        <div class="form-group col-sm-3{{ $errors->has('fig_id') ? 'has-error' : '' }}">
                            <label for="fig_id">FIG</label>
                            <select name="fig_id" id="fig_id" class="form-control" tabindex="1">
                                <option selected disabled>Please select fig</option>
                            </select>
                            <span class=error></span>
                            @if($errors->has('fig_id'))
                            <em class="invalid-feedback">
                                {{ $errors->first('fig_id') }}
                            </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.user.fields.roles_helper') }}
                            </p>
                        </div>
                        <div class="form-group col-sm-3{{ $errors->has('farmer_name') ? 'has-error' : '' }}">
                            <label for="farmer_name">{{ trans('cruds.farmer.fields.farmer_name') }}*</label>
                            <input type="text" id="farmer_name" name="farmer_name" class="form-control" value="{{ old('farmer_name', isset($farmer) ? $farmer->farmer_name : '') }}" tabindex="1">
                            <span class=errorfarmer_name></span>
                            @if($errors->has('farmer_name'))
                            <em class="invalid-feedback">
                                {{ $errors->first('farmer_name') }}
                            </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.farmer.fields.farmer_name_helper') }}
                            </p>
                        </div>
                        <div class="form-group col-sm-3 {{ $errors->has('father_name') ? 'has-error' : '' }}">
                            <label for="father_name">{{ trans('cruds.farmer.fields.father_name') }}*</label>
                            <input type="text" id="father_name" name="father_name" class="form-control" tabindex="2" required>
                            <span class=errorfather_name></span>

                            @if($errors->has('father_name'))
                            <em class="invalid-feedback">
                                {{ $errors->first('father_name') }}
                            </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.farmer.fields.father_name_helper') }}
                            </p>
                        </div>
						
						
						<div class="form-group col-sm-3 {{ $errors->has('isbod') ? 'has-error' : '' }}">
                                {{ trans('cruds.farmer.fields.isbod') }}
                                <div class="d-flex form-control col  mt-2 p-2 ">
                                    <div class="form-check pr-2 d-flex">
                                        <input class="form-check-input" value="yes" id="yes_isbod" type="radio" tabindex="1" name="isbod">
                                        <label class="form-check-label" for="yes_isbod">Yes</label>
                                        @if($errors->has('isbod'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('isbod') }}
                                        </em>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.farmer.fields.isbod_helper') }}
                                        </p>
                                    </div>
                                    <div class="form-check pr-2 d-flex">
                                        <input class="form-check-input" value="no" id="no_isbod" type="radio" name="isbod" checked='checked'>
                                        <label class="form-check-label" for="no_loan">No</label>
                                        @if($errors->has('isbod'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('isbod') }}
                                        </em>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.farmer.fields.isbod_helper') }}
                                        </p>
                                    </div>
                                </div>
                                <span class="errorisbod"></span>
                            </div>
						
						
                        <div class="form-group col-sm-3{{ $errors->has('dob') ? 'has-error' : '' }}">
                            <label for="dob">{{ trans('cruds.farmer.fields.dob') }}</label>
                            <input type="date" id="dob" name="dob" class="form-control" value="{{ old('dob', isset($farmer) ? $farmer->dob : '') }}" tabindex="3">
                            <span class=errordob></span>
                            @if($errors->has('dob'))
                            <em class="invalid-feedback">
                                {{ $errors->first('dob') }}
                            </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.farmer.fields.dob_helper') }}
                            </p>
                        </div>
                        <div class="form-group col-sm-3 {{ $errors->has('email') ? 'has-error' : '' }}">
                            <label for="email">{{ trans('cruds.farmer.fields.email') }}</label>
                            <input type="email" id="email" name="email" class="form-control" tabindex="4">
                            <span class=erroremail></span>

                            @if($errors->has('email'))
                            <em class="invalid-feedback">
                                {{ $errors->first('email') }}
                            </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.farmer.fields.email_helper') }}
                            </p>
                        </div>
                        <div class="form-group col-sm-3{{ $errors->has('address') ? 'has-error' : '' }}">
                            <label for="address">{{ trans('cruds.farmer.fields.address') }}*</label>
                            <input type="text" id="address" name="address" class="form-control" value="{{ old('address', isset($farmer) ? $farmer->address : '') }}" tabindex="5">
                            <span class=erroraddress></span>

                            @if($errors->has('address'))
                            <em class="invalid-feedback">
                                {{ $errors->first('address') }}
                            </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.farmer.fields.address_helper') }}
                            </p>
                        </div>
                        <div class="form-group col-sm-3 {{ $errors->has('contact_no') ? 'has-error' : '' }}">
                            <label for="contact_no">{{ trans('cruds.farmer.fields.contact_no') }}</label>
                            <input type="text" id="contact_no" name="contact_no" class="form-control" tabindex="6">
                            <span class=errorcontact_no></span>

                            @if($errors->has('contact_no'))
                            <em class="invalid-feedback">
                                {{ $errors->first('contact_no') }}
                            </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.farmer.fields.contact_no_helper') }}
                            </p>
                        </div>
                        <div class="form-group col-sm-3 {{ $errors->has('block') ? 'has-error' : '' }}">
                            <label for="block">{{ trans('cruds.farmer.fields.block') }}*</label>
                            <input type="text" id="block" name="block" class="form-control" tabindex="7">
                            <span class=errorblock></span>

                            @if($errors->has('block'))
                            <em class="invalid-feedback">
                                {{ $errors->first('block') }}
                            </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.farmer.fields.block_helper') }}
                            </p>
                        </div>
                        <div class="form-group col-sm-3 {{ $errors->has('village') ? 'has-error' : '' }}">
                            <label for="village">{{ trans('cruds.farmer.fields.village') }}*</label>
                            <input type="text" id="village" name="village" class="form-control" tabindex="8">
                            <span class=errorvillage></span>
                            @if($errors->has('village'))
                            <em class="invalid-feedback">
                                {{ $errors->first('village') }}
                            </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.farmer.fields.village_helper') }}
                            </p>
                        </div>
                        <div class="form-group col-sm-3 {{ $errors->has('district') ? 'has-error' : '' }}">
                            <label for="district">{{ trans('cruds.farmer.fields.district') }}*</label>
                            <input type="text" id="district" name="district" class="form-control" tabindex="9">
                            <span class=errordistrict></span>

                            @if($errors->has('district'))
                            <em class="invalid-feedback">
                                {{ $errors->first('district') }}
                            </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.farmer.fields.district_helper') }}
                            </p>
                        </div>
                        <div class="form-group col-sm-3 {{ $errors->has('state') ? 'has-error' : '' }}">
                            <label for="state">{{ trans('cruds.farmer.fields.state') }}*</label>
                            <input type="text" id="state" name="state" class="form-control" tabindex="10">
                            <span class=errorstate></span>

                            @if($errors->has('state'))
                            <em class="invalid-feedback">
                                {{ $errors->first('state') }}
                            </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.farmer.fields.state_helper') }}
                            </p>
                        </div>
                        <div class="form-group col-sm-3 {{ $errors->has('gender') ? 'has-error' : '' }}">
                            {{ trans('cruds.farmer.fields.gender') }}*
                            <div class="d-flex form-control col  mt-2 p-2 ">
                                <div class="form-check pr-2 d-flex">
                                    <input class="form-check-input" id="male" value="male" type="radio" name="gender" tabindex="11">
                                    <label class="form-check-label" for="male">Male</label>
                                    @if($errors->has('gender'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('gender') }}
                                    </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.farmer.fields.gender_helper') }}
                                    </p>
                                </div>
                                <div class="form-check pr-2 d-flex">
                                    <input class="form-check-input" id="female" value="female" type="radio" name="gender">
                                    <label class="form-check-label" for="female">Female</label>
                                    @if($errors->has('gender'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('gender') }}
                                    </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.farmer.fields.gender_helper') }}
                                    </p>
                                </div>
                            </div>
                            <span class=errorgender></span>
                        </div>
                        <div class="form-group col-sm-3 {{ $errors->has('marital_status') ? 'has-error' : '' }}">
                            {{ trans('cruds.farmer.fields.marital_status') }}*
                            <div class="d-flex form-control col  mt-2 p-2 ">
                                <div class="form-check pr-2 d-flex">
                                    <input class="form-check-input" value="yes" id="yes_marital_status" type="radio" name="marital_status" tabindex="12">
                                    <label class="form-check-label" for="yes_marital_status">Married</label>
                                    @if($errors->has('marital_status'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('marital_status') }}
                                    </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.farmer.fields.marital_status_helper') }}
                                    </p>
                                </div>
                                <div class="form-check pr-2 d-flex">
                                    <input class="form-check-input" value="no" id="no_marital_status" type="radio" name="marital_status">
                                    <label class="form-check-label" for="no_marital_status">Unmarried</label>
                                    @if($errors->has('marital_status'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('marital_status') }}
                                    </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.farmer.fields.marital_status_helper') }}
                                    </p>
                                </div>
                            </div>
                            <span class=errormarital_status></span>
                        </div>
                        <div class="form-group col-sm-3 {{ $errors->has('caste') ? 'has-error' : '' }}">
                            {{ trans('cruds.farmer.fields.caste') }}*
                            <div class="d-flex  col form-control mt-2 p-2 " id="">
                                <div class="form-check pr-2 d-flex">
                                    <input class="form-check-input" id="general" value="general" type="radio" name="caste" tabindex="13">
                                    <label class="form-check-label" for="general">General</label>
                                    @if($errors->has('caste'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('caste') }}
                                    </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.farmer.fields.caste_helper') }}
                                    </p>
                                </div>
                                <div class="form-check pr-2 d-flex">
                                    <input class="form-check-input" id="sc" value="sc" type="radio" name="caste">
                                    <label class="form-check-label" for="sc">SC</label>
                                    @if($errors->has('caste'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('caste') }}
                                    </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.farmer.fields.caste_helper') }}
                                    </p>
                                </div>
                                <div class="form-check d-flex">
                                    <input class="form-check-input" value="obc" id="obc" type="radio" name="caste">
                                    <label class="form-check-label" for="obc">OBC</label>
                                    @if($errors->has('caste'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('caste') }}
                                    </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.farmer.fields.caste_helper') }}
                                    </p>
                                </div>
                            </div>
                            <span class=errorcaste></span>
                        </div>
                        <div class="form-group col-sm-3 {{ $errors->has('aadhar_number') ? 'has-error' : '' }}">
                            <label for="aadhar_number">{{ trans('cruds.farmer.fields.aadhar_number') }}</label>
                            <input type="text" id="aadhar_number" name="aadhar_number" class="form-control" tabindex="14" required>
                            <span class=erroraadhar_number></span>

                            @if($errors->has('aadhar_number'))
                            <em class="invalid-feedback">
                                {{ $errors->first('aadhar_number') }}
                            </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.farmer.fields.aadhar_number_helper') }}
                            </p>
                        </div>
                        <div class="form-group col-sm-3 {{ $errors->has('total_family_no') ? 'has-error' : '' }}">
                            <label for="total_family_no">{{ trans('cruds.farmer.fields.total_family_no') }}</label>
                            <input type="text" id="total_family_no" name="total_family_no" class="form-control" tabindex="15" required>
                            <span class=errortotal_family_no></span>

                            @if($errors->has('total_family_no'))
                            <em class="invalid-feedback">
                                {{ $errors->first('total_family_no') }}
                            </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.farmer.fields.total_family_no_helper') }}
                            </p>
                        </div>

                        <div class="form-group col-sm-3 {{ $errors->has('share_price') ? 'has-error' : '' }}">
                            <label for="share_price">Share Price (Rs.)*</label>
                            <input type="text" id="share_price" name="share_price" class="form-control" tabindex="39">
                            <span class=errorshare_price></span>

                            @if($errors->has('share_price'))
                            <em class="invalid-feedback">
                                {{ $errors->first('share_price') }}
                            </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.farmer.fields.period_helper') }}
                            </p>
                        </div>
                        <div class="form-group col-sm-3 {{ $errors->has('farmer_type') ? 'has-error' : '' }}">
                            {{ trans('cruds.farmer.fields.farmer_type') }}*
                            <div class="d-flex col  p-2 mt-2 form-control">
                                <div class="form-check pr-2 d-flex">
                                    <input class="form-check-input" value="small" id="small" type="radio" tabindex="29" name="farmer_type">
                                    <label class="form-check-label" for="small">Small</label>
                                    @if($errors->has('farmer_type'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('farmer_type') }}
                                    </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.farmer.fields.farmer_type_helper') }}
                                    </p>
                                </div>
                                <div class="form-check pr-2 d-flex">
                                    <input class="form-check-input" id="medium" value="medium" type="radio" name="farmer_type">
                                    <label class="form-check-label" for="medium">Medium</label>
                                    @if($errors->has('farmer_type'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('farmer_type') }}
                                    </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.farmer.fields.farmer_type_helper') }}
                                    </p>
                                </div>
                                <div class="form-check d-flex">
                                    <input class="form-check-input" id="large" value="large" type="radio" name="farmer_type">
                                    <label class="form-check-label" for="large">Large</label>
                                    @if($errors->has('farmer_type'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('farmer_type') }}
                                    </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.farmer.fields.farmer_type_helper') }}
                                    </p>
                                </div>
                            </div>
                            <span class=errorfarmer_type></span>

                        </div>
                    </div>
                </div>
            </div>
            <div class="border rounded m-1 mt-4">
                <div class="card-header">
                    {{ trans('cruds.farmer.title_land_detailse') }}
                </div>
                <div class="row">
                    <div class="row col">
                        <div class="row h-35  pt-2 ml-2">
                            <div class="form-group col-sm-3 {{ $errors->has('survey_no') ? 'has-error' : '' }}">
                                <label for="survey_no">{{ trans('cruds.farmer.fields.survey_no') }}</label>
                                <input type="text" id="survey_no" name="survey_no" class="form-control" tabindex="16" required>
                                <span class=errorsurvey_no></span>
                                @if($errors->has('survey_no'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('survey_no') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.survey_no_helper') }}
                                </p>
                            </div>
                            <div class="form-group col-sm-3 {{ $errors->has('kharsa_no') ? 'has-error' : '' }}">
                                <label for="kharsa_no">{{ trans('cruds.farmer.fields.kharsa_no') }}</label>
                                <input type="text" id="kharsa_no" name="kharsa_no" class="form-control" tabindex="17" required>
                                <span class=errorkharsa_no></span>

                                @if($errors->has('kharsa_no'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('kharsa_no') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.kharsa_no_helper') }}
                                </p>
                            </div>
                            <div class="form-group col-sm-3 {{ $errors->has('total_area') ? 'has-error' : '' }}">
                                <label for="total_area">{{ trans('cruds.farmer.fields.total_area') }}</label>
                                <input type="text" id="total_area" name="total_area" class="form-control" tabindex="18">
                                <span class=errortotal_area></span>

                                @if($errors->has('total_area'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('total_area') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.total_area_helper') }}
                                </p>
                            </div>
                            <div class="form-group col-sm-3 {{ $errors->has('irrigated_area') ? 'has-error' : '' }}">
                                <label for="irrigated_area">{{ trans('cruds.farmer.fields.irrigated_area') }}</label>
                                <input type="text" id="irrigated_area" name="irrigated_area" class="form-control" tabindex="19">
                                <span class=errorirrigated_area></span>

                                @if($errors->has('irrigated_area'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('irrigated_area') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.irrigated_area_helper') }}
                                </p>
                            </div>
                            <div class="form-group col-sm-3 {{ $errors->has('unirrigated_area') ? 'has-error' : '' }}">
                                <label for="unirrigated_area">{{ trans('cruds.farmer.fields.unirrigated_area') }}</label>
                                <input type="text" id="unirrigated_area" name="unirrigated_area" class="form-control" tabindex="20" required>
                                <span class=errorunirrigated_area></span>

                                @if($errors->has('unirrigated_area'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('unirrigated_area') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.unirrigated_area_helper') }}
                                </p>
                            </div>
                            <div class="form-group col-sm-3 {{ $errors->has('water_source') ? 'has-error' : '' }}">

                                <label>{{ trans('cruds.farmer.fields.water_source') }} </label>
                                <select class="form-control" name="water_source" tabindex="21">
                                    <option value="">Select Water Source</option>
                                    <option value="borewell"> Bore Well</option>
                                    <option value="openwell"> Open Well</option>
                                    <option value="river">River</option>
                                    <option value="canal">Canal</option>
                                    <option value="tank">Tank</option>
                                    <option value="pond">Pond</option>
                                    <option value="checkdam">Check Dam</option>
                                    <option value="drip">Drip</option>
                                    <option value="other">Other</option>
                                    <option value="none">None</option>

                                </select>
                                <span class=errorwater_source></span>

                                @if($errors->has('water_source'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('water_source') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.water_source_helper') }}
                                </p>
                            </div>
                            <div class="form-group col-sm-3 {{ $errors->has('storage') ? 'has-error' : '' }}">
                                <label for="storage">{{ trans('cruds.farmer.fields.storage')
                            }}</label>
                                <!-- <input type="text" id="storage" name="storage" class="form-control" tabindex="22"> -->
								<select class="form-control" name="storage" tabindex="22">
                                    <option value="">Select Storage Facility</option>
                                    <option value="WDRA Approved Warehouse">WDRA Approved Warehouse</option>
                                    <option value="Central Warehousing Corporation">Central Warehousing Corporation</option>
                                    <option value="State Warehouse Corporation">State Warehouse Corporation</option>
                                    <option value="Private Warehouses">Private Warehouses</option>
                                    <option value="Cold Stores">Cold Stores</option>
                                    <option value="Controlled Atmospheric Cold Stores">Controlled Atmospheric Cold Stores</option>
                                </select>
                                <span class=errorstorage></span>

                                @if($errors->has('storage'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('storage') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.storage_helper') }}
                                </p>
                            </div>
                            <div class="form-group col-sm-3 {{ $errors->has('fertilizer_type') ? 'has-error' : '' }}">
                                <label for="fertilizer_type">{{ trans('cruds.farmer.fields.fertilizer_type') }}</label>
								<select class="form-control" name="fertilizer_type" tabindex="23">
                                    <option value="">Select Fertilizer Type</option>
                                    <option value="Chemical">Chemical</option>
                                    <option value="Manure">Manure</option>
                                    <option value="Bio Fertilizer">Bio Fertilizer</option>
                                    <option value="Organic">Organic</option>
                                </select>
                                <span class=errorfertilizer_type></span>

                                @if($errors->has('fertilizer_type'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('fertilizer_type') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.fertilizer_type_helper') }}
                                </p>
                            </div>
                            <div class="form-group col-sm-3 {{ $errors->has('fertilizer_obtain_from') ? 'has-error' : '' }}">
                                <label for="fertilizer_obtain_from">{{ trans('cruds.farmer.fields.fertilizer_obtain_from')
                            }}</label>
                                <!-- <input type="text" id="fertilizer_obtain_from" name="fertilizer_obtain_from" class="form-control" tabindex="24" required> -->
								<select class="form-control" name="fertilizer_obtain_from" tabindex="24">
                                    <option value="">Select Fertilizer Obtain From</option>
                                    <option value="Agro">Agro</option>
                                    <option value="Govt Agency">Govt Agency</option>
                                    <option value="Other">Other</option>
                                </select>
                                <span class=errorfertilizer_obtain_from></span>

                                @if($errors->has('fertilizer_obtain_from'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('fertilizer_obtain_from') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.fertilizer_obtain_from_helper') }}
                                </p>
                            </div>
                            <div class="form-group col-sm-3 {{ $errors->has('organic_farming') ? 'has-error' : '' }}">
                                {{ trans('cruds.farmer.fields.organic_farming') }}
                                <div class="d-flex form-control col  mt-2 p-2 ">
                                    <div class="form-check pr-2 d-flex">
                                        <input class="form-check-input" value="yes" id="yes" type="radio" name="organic_farming" tabindex="25">
                                        <label class="form-check-label" for="yes">Yes</label>
                                        @if($errors->has('organic_farming'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('organic_farming') }}
                                        </em>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.farmer.fields.organic_farming_helper') }}
                                        </p>
                                    </div>
                                    <div class="form-check pr-2 d-flex">
                                        <input class="form-check-input" value="no" id="no" type="radio" name="organic_farming">
                                        <label class="form-check-label" for="no">No</label>
                                        @if($errors->has('organic_farming'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('organic_farming') }}
                                        </em>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.farmer.fields.organic_farming_helper') }}
                                        </p>
                                    </div>
                                </div>
                                <span class=errororgaic_farming></span>
                            </div>
                            <div class="form-group col-sm-3 {{ $errors->has('land_organic_farming') ? 'has-error' : '' }}">
                                <label for="land_organic_farming">{{ trans('cruds.farmer.fields.land_organic_farming')
                            }}</label>
                                <input type="text" id="land_organic_farming" name="land_organic_farming" class="form-control" tabindex="26">
                                <span class=errorland_organic_farming></span>

                                @if($errors->has('land_organic_farming'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('land_organic_farming') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.land_organic_farming_helper') }}
                                </p>
                            </div>
                            <div class="form-group col-sm-3 {{ $errors->has('transport') ? 'has-error' : '' }}">
                                {{ trans('cruds.farmer.fields.transport') }}
                                <div class="d-flex form-control col  mt-2 p-2 ">
                                    <div class="form-check pr-2 d-flex">
                                        <input class="form-check-input" value="yes" id="yes_transport" type="radio" tabindex="27" name="transport">
                                        <label class="form-check-label" for="yes_transport">Yes</label>
                                        @if($errors->has('transport'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('transport') }}
                                        </em>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.farmer.fields.transport_helper') }}
                                        </p>
                                    </div>
                                    <div class="form-check pr-2 d-flex">
                                        <input class="form-check-input" value="no" id="no_transport" type="radio" name="transport">
                                        <label class="form-check-label" for="no_transport">No</label>
                                        @if($errors->has('transport'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('transport') }}
                                        </em>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.farmer.fields.transport_helper') }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-sm-3 {{ $errors->has('bank_account') ? 'has-error' : '' }}">
                                {{ trans('cruds.farmer.fields.bank_account') }}
                                <div class="d-flex form-control col  mt-2 p-2 ">
                                    <div class="form-check pr-2 d-flex">
                                        <input class="form-check-input" value="yes" id="yes_bank_account" type="radio" tabindex="28" name="bank_account">
                                        <label class="form-check-label" for="yes_bank_account">Yes</label>
                                        @if($errors->has('bank_account'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('bank_account') }}
                                        </em>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.farmer.fields.bank_account_helper') }}
                                        </p>
                                    </div>
                                    <div class="form-check pr-2 d-flex">
                                        <input class="form-check-input" value="no" id="no_bank_account" type="radio" name="bank_account">
                                        <label class="form-check-label" for="no_bank_account">No</label>
                                        @if($errors->has('bank_account'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('bank_account') }}
                                        </em>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.farmer.fields.bank_account_helper') }}
                                        </p>
                                    </div>
                                </div>
                                <span class=errorbank_account></span>
                            </div>

                            <div class="form-group col-sm-3 {{ $errors->has('trainee_required') ? 'has-error' : '' }}">
                                {{ trans('cruds.farmer.fields.trainee_required') }}
                                <div class="d-flex form-control col  mt-2 p-2 ">
                                    <div class="form-check pr-2 d-flex">
                                        <input class="form-check-input" value="yes" id="yes_trainee" type="radio" tabindex="30" name="trainee_required">
                                        <label class="form-check-label" for="yes_trainee">Yes</label>
                                        @if($errors->has('trainee_required'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('trainee_required') }}
                                        </em>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.farmer.fields.trainee_required_helper') }}
                                        </p>
                                    </div>
                                    <div class="form-check pr-2 d-flex">
                                        <input class="form-check-input" value="no" id="no_trainee" type="radio" name="trainee_required">
                                        <label class="form-check-label" for="no_trainee">No</label>
                                        @if($errors->has('trainee_required'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('trainee_required') }}
                                        </em>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.farmer.fields.trainee_required_helper') }}
                                        </p>
                                    </div>
                                </div>
                                <span class=errortrainee_required></span>
                            </div>
                            <div class="form-group col-sm-3 {{ $errors->has('kisan_credit_card') ? 'has-error' : '' }}">
                                {{ trans('cruds.farmer.fields.kisan_credit_card') }}
                                <div class="d-flex form-control col  mt-2 p-2 ">
                                    <div class="form-check pr-2 d-flex">
                                        <input class="form-check-input" value="yes" id="yes_kisan_credit_card" type="radio" tabindex="31" name="kisan_credit_card">
                                        <label class="form-check-label" for="yes_kisan_credit_card">Yes</label>
                                        @if($errors->has('kisan_credit_card'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('kisan_credit_card') }}
                                        </em>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.farmer.fields.kisan_credit_card_helper') }}
                                        </p>
                                    </div>
                                    <div class="form-check pr-2 d-flex">
                                        <input class="form-check-input" value="no" id="no_kisan_credit_card" type="radio" name="kisan_credit_card">
                                        <label class="form-check-label" for="no_kisan_credit_card">No</label>
                                        @if($errors->has('kisan_credit_card'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('kisan_credit_card') }}
                                        </em>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.farmer.fields.kisan_credit_card_helper') }}
                                        </p>
                                    </div>
                                </div>
                                <span class=errorkisan_credit_card></span>
                            </div>
                            <div class="form-group col-sm-3 {{ $errors->has('loan') ? 'has-error' : '' }}">
                                {{ trans('cruds.farmer.fields.loan') }}
                                <div class="d-flex form-control col  mt-2 p-2 ">
                                    <div class="form-check pr-2 d-flex">
                                        <input class="form-check-input" value="yes" id="yes_loan" type="radio" tabindex="32" name="loan">
                                        <label class="form-check-label" for="yes_loan">Yes</label>
                                        @if($errors->has('loan'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('loan') }}
                                        </em>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.farmer.fields.loan_helper') }}
                                        </p>
                                    </div>
                                    <div class="form-check pr-2 d-flex">
                                        <input class="form-check-input" value="no" id="no_loan" type="radio" name="loan">
                                        <label class="form-check-label" for="no_loan">No</label>
                                        @if($errors->has('loan'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('loan') }}
                                        </em>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.farmer.fields.loan_helper') }}
                                        </p>
                                    </div>
                                </div>
                                <span class=errorloan></span>
                            </div>
                            <div class="form-group col-sm-3 {{ $errors->has('shg') ? 'has-error' : '' }}">
                                {{ trans('cruds.farmer.fields.shg') }}
                                <div class="d-flex form-control col  mt-2 p-2 ">
                                    <div class="form-check pr-2 d-flex">
                                        <input class="form-check-input" value="yes" id="yes_shg" type="radio" tabindex="33" name="shg">
                                        <label class="form-check-label" for="yes_shg">Yes</label>
                                        @if($errors->has('shg'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('shg') }}
                                        </em>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.farmer.fields.shg_helper') }}
                                        </p>
                                    </div>
                                    <div class="form-check pr-2 d-flex">
                                        <input class="form-check-input" value="no" id="no_shg" type="radio" name="shg">
                                        <label class="form-check-label" for="no_shg">No</label>
                                        @if($errors->has('shg'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('shg') }}
                                        </em>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.farmer.fields.shg_helper') }}
                                        </p>
                                    </div>
                                </div>
                                <span class=errorshg></span>
                            </div>
                            <div class="form-group col-sm-3 {{ $errors->has('literate') ? 'has-error' : '' }}">
                                {{ trans('cruds.farmer.fields.literate') }}
                                <div class="d-flex form-control col  mt-2 p-2 ">
                                    <div class="form-check pr-2 d-flex">
                                        <input class="form-check-input" value="yes" id="yes_literate" type="radio" name="literate" tabindex="34">
                                        <label class="form-check-label" for="yes_literate">Yes</label>
                                        @if($errors->has('literate'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('literate') }}
                                        </em>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.farmer.fields.literate_helper') }}
                                        </p>
                                    </div>
                                    <div class="form-check pr-2 d-flex">
                                        <input class="form-check-input" value="no" id="no_literate" type="radio" name="literate">
                                        <label class="form-check-label" for="no_literate">No</label>
                                        @if($errors->has('literate'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('literate') }}
                                        </em>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.farmer.fields.literate_helper') }}
                                        </p>
                                    </div>
                                </div>
                                <span class=errorliterate></span>
                            </div>
                            <div class="form-group col-sm-3 {{ $errors->has('seed_buy_from') ? 'has-error' : '' }}">
                                <label for="seed_buy_from">{{ trans('cruds.farmer.fields.seed_buy_from') }}</label>
                                <!-- <input type="text" id="seed_buy_from" name="seed_buy_from" class="form-control" tabindex="35"> -->
								<select class="form-control" name="seed_buy_from" tabindex="35">
                                    <option value="">Select Seeds Buy From</option>
                                    <option value="Agro">Agro</option>
                                    <option value="Govt Agency">Govt Agency</option>
                                    <option value="SAU">SAU</option>
                                    <option value="KVK">KVK</option>
                                    <option value="Own">Own</option>
                                    <option value="Other">Other</option>
                                </select>
                                <span class=errorseed_buy_from></span>

                                @if($errors->has('seed_buy_from'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('seed_buy_from') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.seed_buy_from_helper') }}
                                </p>
                            </div>

                            <div class="form-group col-sm-3 {{ $errors->has('manure') ? 'has-error' : '' }}">
                                <label for="manure">{{ trans('cruds.farmer.fields.manure') }}</label>
                                <input type="text" id="manure" name="manure" class="form-control" tabindex="36">
                                <span class=errormanure></span>

                                @if($errors->has('manure'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('manure') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.manure_helper') }}
                                </p>
                            </div>
                            <div class="form-group col-sm-3 {{ $errors->has('pesticides_used') ? 'has-error' : '' }}">
                                <label for="pesticides_used">{{ trans('cruds.farmer.fields.pesticides_used') }}</label>
                                <input type="text" id="pesticides_used" name="pesticides_used" class="form-control" tabindex="37">
                                <span class=errorpesticides_used></span>

                                @if($errors->has('pesticides_used'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('pesticides_used') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.pesticides_used_helper') }}
                                </p>
                            </div>
                            <div class="form-group col-sm-3 {{ $errors->has('cost_of_production') ? 'has-error' : '' }}">
                                <label for="cost_of_production">{{ trans('cruds.farmer.fields.cost_of_production') }}</label>
                                <input type="text" id="cost_of_production" name="cost_of_production" class="form-control" tabindex="38">
                                <span class=errorcost_of_production></span>

                                @if($errors->has('cost_of_production'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('cost_of_production') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.cost_of_production_helper') }}
                                </p>
                            </div>

                            <div class="form-group col-sm-3 {{ $errors->has('source_of_income') ? 'has-error' : '' }}">
                                <label for="source_of_income">{{ trans('cruds.farmer.fields.source_of_income') }}</label>
                                <input type="text" id="source_of_income" name="source_of_income" class="form-control" tabindex="40">

                                @if($errors->has('source_of_income'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('source_of_income') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.source_of_income_helper') }}
                                </p>
                            </div>
                            <div class="form-group col-sm-3 {{ $errors->has('farmer_tools') ? 'has-error' : '' }}">
                                <label for="farmer_tools">{{ trans('cruds.farmer.fields.farmer_tools') }}</label>
                                <input type="text" id="farmer_tools" name="farmer_tools" class="form-control" tabindex="41">

                                @if($errors->has('farmer_tools'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('farmer_tools') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.farmer_tools_helper') }}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mt-2" style="float:left; margin-right:15px;">
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}" tabindex="42">
            </div>

            <div class="mt-2" style="float:left;">
                <a class="btn btn-primary" href="{{ route('admin.farmer.index') }}">Back</a>
            </div>
        </form>
    </div>


</div>
</div>
@endsection