@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.farmer.title_singular') }}
    </div>

    <div class="card-body">
        <form action="{{ route("admin.farmer.update", [$farmer->id]) }}" id="farmerForm" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="border rounded m-1">
                <div class="card-header">
                    {{ trans('cruds.farmer.title_farmer_detailse') }}
                </div>
                <div class="row p-2">
                    <div class="row col">
                        <div class="form-group col-sm-3{{ $errors->has('fpo_id') ? 'has-error' : '' }}">
                            <label for="fpo_id">Fpo*</label>
                            <input type="hidden" name="id" value="{{$farmer->id}}">
                            <select name="fpo_id" id="fpo_id" class="form-control">
                                <option selected disabled>Please select fpo</option>
                                @foreach ($fpos as $key => $fpo)
                                <option value="{{ $fpo->id }}" {{ !empty(old('fpo_id')) && old('fpo_id') == $fpo->id
                            ? 'selected="selected"'
                            : ((!empty($farmer) && !empty($farmer->fpo_id)) && $fpo->id == $farmer->fpo_id
                                ? 'selected="selected"'
                                : "")
                        }}>{{ $fpo->fpo_name }}</option>
                                @endforeach

                            </select>
                            <span class=errorfpo_id></span>

                            @if($errors->has('fpo_id'))
                            <em class="invalid-feedback">
                                {{ $errors->first('fpo_id') }}
                            </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.user.fields.roles_helper') }}
                            </p>
                        </div>
                        <input type="hidden" id="fig_emp_edit" name="fig_emp_edit" value="{{$farmer->fig_id}}">
                        <div class="form-group col-sm-3{{ $errors->has('fig_id') ? 'has-error' : '' }}">
                            <label for="fig_id">FIG</label>
                            <select name="fig_id" id="fig_idEdit" class="form-control" tabindex="1">
                                <option selected disabled>Please select fig</option>
                            </select>
                            <span class=error></span>
                            @if($errors->has('fig_id'))
                            <em class="invalid-feedback">
                                {{ $errors->first('fig_id') }}
                            </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.user.fields.roles_helper') }}
                            </p>
                        </div>
                        <div class="form-group col-sm-3{{ $errors->has('farmer_name') ? 'has-error' : '' }}">
                            <label for="farmer_name">{{ trans('cruds.farmer.fields.farmer_name') }}*</label>
                            <input type="text" id="farmer_name" name="farmer_name" class="form-control" value="{{ old('farmer_name', isset($farmer) ? $farmer->farmer_name : '') }}" required>
                            <span class=errorfarmer_name></span>

                            @if($errors->has('farmer_name'))
                            <em class="invalid-feedback">
                                {{ $errors->first('farmer_name') }}
                            </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.farmer.fields.farmer_name_helper') }}
                            </p>
                        </div>
                        <div class="form-group col-sm-3 {{ $errors->has('father_name') ? 'has-error' : '' }}">
                            <label for="father_name">{{ trans('cruds.farmer.fields.father_name') }}*</label>
                            <input type="text" value="{{ old('father_name', isset($farmer) ? $farmer->father_name : '') }}" id="father_name" name="father_name" class="form-control" required>
                            <span class=errorfather_name></span>

                            @if($errors->has('father_name'))
                            <em class="invalid-feedback">
                                {{ $errors->first('father_name') }}
                            </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.farmer.fields.father_name_helper') }}
                            </p>
                        </div>
                 						
						<div class="form-group col-sm-3 {{ $errors->has('isbod') ? 'has-error' : '' }}">
                                {{ trans('cruds.farmer.fields.isbod') }}
                                <div class="d-flex form-control col  mt-2 p-2 ">
                                    <div class="form-check pr-2 d-flex">
                                        <input class="form-check-input" <?php if ($farmer->isbod == 'yes') { ?> checked <?php } ?> value="yes" id="yes_isbod" type="radio" name="isbod">
                                        <label class="form-check-label" for="yes_isbod">Yes</label>
                                        @if($errors->has('isbod'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('isbod') }}
                                        </em>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.farmer.fields.isbod_helper') }}
                                        </p>
                                    </div>
                                    <div class="form-check pr-2 d-flex">
                                        <input class="form-check-input" <?php if ($farmer->isbod == 'no') { ?> checked <?php } ?> value="no" id="no_isbod" type="radio" name="isbod">
                                        <label class="form-check-label" for="no_isbod">No</label>
                                        @if($errors->has('isbod'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('isbod') }}
                                        </em>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.farmer.fields.isbod_helper') }}
                                        </p>
                                    </div>

                                </div>

                            </div>
						
						
						<div class="form-group col-sm-3{{ $errors->has('dob') ? 'has-error' : '' }}">
                            <label for="dob">{{ trans('cruds.farmer.fields.dob') }}</label>
                            <input type="date" id="dob" name="dob" class="form-control" value="{{ old('dob', isset($farmer) ? $farmer->dob : '') }}" required>
                            <span class=errordob></span>

                            @if($errors->has('dob'))
                            <em class="invalid-feedback">
                                {{ $errors->first('dob') }}
                            </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.farmer.fields.dob_helper') }}
                            </p>
                        </div>
						
                        <div class="form-group col-sm-3{{ $errors->has('email') ? 'has-error' : '' }}">
                            <label for="email">{{ trans('cruds.farmer.fields.email') }}</label>
                            <input type="email" value="{{ old('dob', isset($farmer) ? $farmer->email : '') }}" id="email" name="email" class="form-control" required>
                            <span class=erroremail></span>

                            @if($errors->has('email'))
                            <em class="invalid-feedback">
                                {{ $errors->first('email') }}
                            </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.farmer.fields.email_helper') }}
                            </p>
                        </div>
                        <div class="form-group col-sm-3{{ $errors->has('address') ? 'has-error' : '' }}">
                            <label for="address">{{ trans('cruds.farmer.fields.address') }}*</label>
                            <input type="address" id="address" name="address" class="form-control" value="{{ old('address', isset($farmer) ? $farmer->address : '') }}" required>
                            <span class=erroraddress></span>

                            @if($errors->has('address'))
                            <em class="invalid-feedback">
                                {{ $errors->first('address') }}
                            </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.farmer.fields.address_helper') }}
                            </p>
                        </div>
                        <div class="form-group col-sm-3{{ $errors->has('contact_no') ? 'has-error' : '' }}">
                            <label for="contact_no">{{ trans('cruds.farmer.fields.contact_no') }}</label>
                            <input type="text" value="{{ old('contact_no', isset($farmer) ? $farmer->contact_no : '') }}" id="contact_no" name="contact_no" class="form-control" required>
                            <span class=errorcontact_no></span>

                            @if($errors->has('contact_no'))
                            <em class="invalid-feedback">
                                {{ $errors->first('contact_no') }}
                            </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.farmer.fields.contact_no_helper') }}
                            </p>
                        </div>
                        <div class="form-group col-sm-3{{ $errors->has('block') ? 'has-error' : '' }}">
                            <label for="block">{{ trans('cruds.farmer.fields.block') }}*</label>
                            <input type="text" value="{{ old('block', isset($farmer) ? $farmer->block : '') }}" id="block" name="block" class="form-control" required>
                            <span class=errorblock></span>

                            @if($errors->has('block'))
                            <em class="invalid-feedback">
                                {{ $errors->first('block') }}
                            </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.farmer.fields.block_helper') }}
                            </p>
                        </div>
                        <div class="form-group col-sm-3{{ $errors->has('village') ? 'has-error' : '' }}">
                            <label for="village">{{ trans('cruds.farmer.fields.village') }}*</label>
                            <input type="text" id="taluka" value="{{ old('village', isset($farmer) ? $farmer->village : '') }}" name="village" class="form-control" required>
                            <span class=errorvillage></span>

                            @if($errors->has('village'))
                            <em class="invalid-feedback">
                                {{ $errors->first('village') }}
                            </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.farmer.fields.village_helper') }}
                            </p>
                        </div>
                        <div class="form-group col-sm-3{{ $errors->has('district') ? 'has-error' : '' }}">
                            <label for="district">{{ trans('cruds.farmer.fields.district') }}*</label>
                            <input type="text" id="district" name="district" value="{{ old('district', isset($farmer) ? $farmer->district : '') }}" class="form-control" required>
                            <span class=errordistrict></span>

                            @if($errors->has('district'))
                            <em class="invalid-feedback">
                                {{ $errors->first('district') }}
                            </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.farmer.fields.district_helper') }}
                            </p>
                        </div>
                        <div class="form-group col-sm-3{{ $errors->has('state') ? 'has-error' : '' }}">
                            <label for="state">{{ trans('cruds.farmer.fields.state') }}*</label>
                            <input type="text" id="state" name="state" value="{{ old('state', isset($farmer) ? $farmer->state : '') }}" class="form-control" required>
                            <span class=errorstate></span>

                            @if($errors->has('state'))
                            <em class="invalid-feedback">
                                {{ $errors->first('state') }}
                            </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.farmer.fields.state_helper') }}
                            </p>
                        </div>
                        <div class="form-group col-sm-3 {{ $errors->has('gender') ? 'has-error' : '' }}">
                            {{ trans('cruds.farmer.fields.gender') }}*
                            <div class="d-flex form-control col  mt-2 p-2 ">
                                <div class="form-check pr-2 d-flex">
                                    <input class="form-check-input" <?php if ($farmer->gender == 'male') { ?> checked <?php } ?> id="male" value="male" type="radio" name="gender">
                                    <label class="form-check-label" for="male">Male</label>
                                    @if($errors->has('gender'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('gender') }}
                                    </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.farmer.fields.gender_helper') }}
                                    </p>
                                </div>
                                <div class="form-check pr-2 d-flex">
                                    <input class="form-check-input" <?php if ($farmer->gender == 'female') { ?> checked <?php } ?> id="female" value="female" type="radio" name="gender">
                                    <label class="form-check-label" for="female">Female</label>
                                    @if($errors->has('gender'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('gender') }}
                                    </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.farmer.fields.gender_helper') }}
                                    </p>
                                </div>

                            </div>
                            <span class=errorgender></span>

                        </div>
                        <div class="form-group col-sm-3 {{ $errors->has('marital_status') ? 'has-error' : '' }}">
                            {{ trans('cruds.farmer.fields.marital_status') }}*
                            <div class="d-flex form-control col  mt-2 p-2 ">
                                <div class="form-check pr-2 d-flex">

                                    <input class="form-check-input" value="yes" <?php if ($farmer->marital_status == 'yes') { ?> checked <?php } ?> id="yes_marital_status" type="radio" name="marital_status">
                                    <label class="form-check-label" for="yes_marital_status">Married</label>
                                    @if($errors->has('marital_status'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('marital_status') }}
                                    </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.farmer.fields.marital_status_helper') }}
                                    </p>
                                </div>
                                <div class="form-check pr-2 d-flex">
                                    <input class="form-check-input" value="no" <?php if ($farmer->marital_status == 'no') { ?> checked <?php } ?> id="no_marital_status" type="radio" name="marital_status">
                                    <label class="form-check-label" for="no_marital_status">Unmarried</label>
                                    @if($errors->has('marital_status'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('marital_status') }}
                                    </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.farmer.fields.marital_status_helper') }}
                                    </p>
                                </div>

                            </div>
                            <span class=errormarital_status></span>

                        </div>
                        <div class="form-group col-sm-3 {{ $errors->has('caste') ? 'has-error' : '' }}">
                            {{ trans('cruds.farmer.fields.caste') }}*
                            <div class="d-flex  col form-control mt-2 p-2 ">

                                <div class="form-check pr-2 d-flex">
                                    <input class="form-check-input" id="general" value="general" <?php if ($farmer->caste == 'general') { ?> checked <?php } ?> type="radio" name="caste">
                                    <label class="form-check-label" for="general">General</label>
                                    @if($errors->has('caste'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('caste') }}
                                    </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.farmer.fields.caste_helper') }}
                                    </p>
                                </div>
                                <div class="form-check pr-2 d-flex">
                                    <input class="form-check-input" id="sc" value="sc" <?php if ($farmer->caste == 'sc') { ?> checked <?php } ?> type="radio" name="caste">
                                    <label class="form-check-label" for="sc">SC</label>
                                    @if($errors->has('caste'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('caste') }}
                                    </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.farmer.fields.caste_helper') }}
                                    </p>
                                </div>
                                <div class="form-check d-flex">
                                    <input class="form-check-input" value="obc" id="obc" <?php if ($farmer->caste == 'obc') { ?> checked <?php } ?> type="radio" name="caste">
                                    <label class="form-check-label" for="obc">OBC</label>
                                    @if($errors->has('caste'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('caste') }}
                                    </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.farmer.fields.caste_helper') }}
                                    </p>
                                </div>
                            </div>
                            <span class=errorcaste></span>

                        </div>
                        <div class="form-group col-sm-3 {{ $errors->has('aadhar_number') ? 'has-error' : '' }}">
                            <label for="aadhar_number">{{ trans('cruds.farmer.fields.aadhar_number') }}</label>
                            <input type="text" id="aadhar_number" name="aadhar_number" value="{{ old('aadhar_number', isset($farmer) ? $farmer->aadhar_number : '') }}" class="form-control" required>
                            <span class=erroraadhar_number></span>

                            @if($errors->has('aadhar_number'))
                            <em class="invalid-feedback">
                                {{ $errors->first('aadhar_number') }}
                            </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.farmer.fields.aadhar_number_helper') }}
                            </p>
                        </div>
                        <div class="form-group col-sm-3{{ $errors->has('total_family_no') ? 'has-error' : '' }}">
                            <label for="total_family_no">{{ trans('cruds.farmer.fields.total_family_no') }}</label>
                            <input type="text" id="total_family_no" value="{{ old('total_family_no', isset($farmer) ? $farmer->total_family_no : '') }}" name="total_family_no" class="form-control">
                            <span class=errortotal_family_no></span>

                            @if($errors->has('total_family_no'))
                            <em class="invalid-feedback">
                                {{ $errors->first('total_family_no') }}
                            </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.farmer.fields.total_family_no_helper') }}
                            </p>
                        </div>
                        <div class="form-group col-sm-3 {{ $errors->has('share_price') ? 'has-error' : '' }}">
                            <label for="share_price">Share Price (Rs.)*</label>
                            <input type="text" id="share_price" value="{{ old('share_price', isset($farmer) ? $farmer->share_price : '') }}" name="share_price" class="form-control" required>
                            <span class=errorshare_price></span>

                            @if($errors->has('share_price'))
                            <em class="invalid-feedback">
                                {{ $errors->first('share_price') }}
                            </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.farmer.fields.share_price_helper') }}
                            </p>
                        </div>
                        <div class="form-group col-sm-3 {{ $errors->has('farmer_type') ? 'has-error' : '' }}">
                            {{ trans('cruds.farmer.fields.farmer_type') }}*
                            <div class="d-flex col  p-2 mt-2 form-control">
                                <div class="form-check pr-2 d-flex">
                                    <input class="form-check-input" value="small" <?php if ($farmer->farmer_type == 'small') { ?> checked <?php } ?> id="small" type="radio" name="farmer_type">
                                    <label class="form-check-label" for="small">Small</label>
                                    @if($errors->has('farmer_type'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('farmer_type') }}
                                    </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.farmer.fields.farmer_type_helper') }}
                                    </p>
                                </div>
                                <div class="form-check pr-2 d-flex">
                                    <input class="form-check-input" <?php if ($farmer->farmer_type == 'medium') { ?> checked <?php } ?> id="medium" value="medium" type="radio" name="farmer_type">
                                    <label class="form-check-label" for="medium">Medium</label>
                                    @if($errors->has('farmer_type'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('farmer_type') }}
                                    </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.farmer.fields.farmer_type_helper') }}
                                    </p>
                                </div>
                                <div class="form-check d-flex">
                                    <input class="form-check-input" <?php if ($farmer->farmer_type == 'large') { ?> checked <?php } ?> id="large" value="large" type="radio" name="farmer_type">
                                    <label class="form-check-label" for="large">Large</label>
                                    @if($errors->has('farmer_type'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('farmer_type') }}
                                    </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.farmer.fields.farmer_type_helper') }}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="border rounded m-1 mt-4">
                <div class="card-header">
                    {{ trans('cruds.farmer.title_land_detailse') }}
                </div>
                <div class="row">
                    <div class="row col">
                        <div class="row h-35  pt-2 ml-2">
                            <div class="form-group col-sm-3 {{ $errors->has('survey_no') ? 'has-error' : '' }}">
                                <label for="survey_no">{{ trans('cruds.farmer.fields.survey_no') }}</label>
                                <input type="text" id="survey_no" value="{{ old('survey_no', isset($farmer) ? $farmer->survey_no : '') }}" name="survey_no" class="form-control" required>
                                <span class=errorsurvey_no></span>

                                @if($errors->has('survey_no'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('survey_no') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.survey_no_helper') }}
                                </p>
                            </div>
                            <div class="form-group col-sm-3 {{ $errors->has('kharsa_no') ? 'has-error' : '' }}">
                                <label for="kharsa_no">{{ trans('cruds.farmer.fields.kharsa_no') }}</label>
                                <input type="text" id="kharsa_no" value="{{ old('kharsa_no', isset($farmer) ? $farmer->kharsa_no : '') }}" name="kharsa_no" class="form-control" required>
                                <span class=errorkharsa_no></span>


                                @if($errors->has('kharsa_no'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('kharsa_no') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.kharsa_no_helper') }}
                                </p>
                            </div>
                            <div class="form-group col-sm-3 {{ $errors->has('total_area') ? 'has-error' : '' }}">
                                <label for="total_area">{{ trans('cruds.farmer.fields.total_area') }}</label>
                                <input type="text" id="total_area" value="{{ old('total_area', isset($farmer) ? $farmer->total_area : '') }}" name="total_area" class="form-control">
                                <span class=errortotal_area></span>

                                @if($errors->has('total_area'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('total_area') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.total_area_helper') }}
                                </p>
                            </div>
                            <div class="form-group col-sm-3{{ $errors->has('irrigated_area') ? 'has-error' : '' }}">
                                <label for="irrigated_area">{{ trans('cruds.farmer.fields.irrigated_area') }}</label>
                                <input type="text" id="irrigated_area" value="{{ old('irrigated_area', isset($farmer) ? $farmer->irrigated_area : '') }}" name="irrigated_area" class="form-control" required>
                                @if($errors->has('irrigated_area'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('irrigated_area') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.irrigated_area_helper') }}
                                </p>
                            </div>
                            <div class="form-group col-sm-3 {{ $errors->has('unirrigated_area') ? 'has-error' : '' }}">
                                <label for="unirrigated_area">{{ trans('cruds.farmer.fields.unirrigated_area') }}</label>
                                <input type="text" id="unirrigated_area" value="{{ old('unirrigated_area', isset($farmer) ? $farmer->unirrigated_area : '') }}" name="unirrigated_area" class="form-control">
                                @if($errors->has('unirrigated_area'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('unirrigated_area') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.unirrigated_area_helper') }}
                                </p>
                            </div>
                            <div class="form-group col-sm-3 {{ $errors->has('water_source') ? 'has-error' : '' }}">
                                <label>{{ trans('cruds.farmer.fields.water_source') }} </label>
                                <select class="form-control" name="water_source">
                                    <option value=" ">Select Water Source</option>
                                    <option value="borewell" <?php if ($farmer->water_source == 'borewell') { ?> selected <?php } ?>> Bore Well</option>
                                    <option value="openwell" <?php if ($farmer->water_source == 'openwell') { ?> selected <?php } ?>> Open Well</option>
                                    <option value="river" <?php if ($farmer->water_source == 'river') { ?> selected <?php } ?>>River</option>
                                    <option value="canal" <?php if ($farmer->water_source == 'canal') { ?> selected <?php } ?>>Canal</option>
                                    <option value="tank" <?php if ($farmer->water_source == 'tank') { ?> selected <?php } ?>>Tank</option>
                                    <option value="pond" <?php if ($farmer->water_source == 'pond') { ?> selected <?php } ?>>Pond</option>
                                    <option value="checkdam" <?php if ($farmer->water_source == 'checkdam') { ?> selected <?php } ?>>Check Dam</option>
                                    <option value="drip" <?php if ($farmer->water_source == 'drip') { ?> selected <?php } ?>>Drip</option>
                                    <option value="other" <?php if ($farmer->water_source == 'other') { ?> selected <?php } ?>>Other</option>
                                    <option value="none" <?php if ($farmer->water_source == 'none') { ?> selected <?php } ?>>None</option>
                                </select>
                                @if($errors->has('water_source'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('water_source') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.water_source_helper') }}
                                </p>
                            </div>
                            <div class="form-group col-sm-3 {{ $errors->has('storage') ? 'has-error' : '' }}">
                                <label for="storage">{{ trans('cruds.farmer.fields.storage') }}</label>
                                <!-- <input type="text" value="{{ old('storage', isset($farmer) ? $farmer->storage : '') }}" id="storage" name="storage" class="form-control"> -->
								<select class="form-control" name="storage" tabindex="22">
                                    <option value="">Select Storage Facility</option>
                                    <option value="WDRA Approved Warehouse" <?php if ($farmer->storage == 'WDRA Approved Warehouse') { ?> selected <?php } ?>>WDRA Approved Warehouse</option>
                                    <option value="Central Warehousing Corporation" <?php if ($farmer->storage == 'Central Warehousing Corporation') { ?> selected <?php } ?>>Central Warehousing Corporation</option>
                                    <option value="State Warehouse Corporation" <?php if ($farmer->storage == 'State Warehouse Corporation') { ?> selected <?php } ?>>State Warehouse Corporation</option>
                                    <option value="Private Warehouses" <?php if ($farmer->storage == 'Private Warehouses') { ?> selected <?php } ?>>Private Warehouses</option>
                                    <option value="Cold Stores" <?php if ($farmer->storage == 'Cold Stores') { ?> selected <?php } ?>>Cold Stores</option>
                                    <option value="Controlled Atmospheric Cold Stores" <?php if ($farmer->storage == 'Controlled Atmospheric Cold Stores') { ?> selected <?php } ?>>Controlled Atmospheric Cold Stores</option>
                                </select>
                                @if($errors->has('storage'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('storage') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.storage_helper') }}
                                </p>
                            </div>
                            <div class="form-group col-sm-3 {{ $errors->has('fertilizer_type') ? 'has-error' : '' }}">
                                <label for="fertilizer_type">{{ trans('cruds.farmer.fields.fertilizer_type') }}</label>
                                <!-- <input type="text" value="{{ old('fertilizer_type', isset($farmer) ? $farmer->fertilizer_type : '') }}" id="fertilizer_type" name="fertilizer_type" class="form-control"> -->
								<select class="form-control" name="fertilizer_type" tabindex="23">
                                    <option value="">Select Fertilizer Type</option>
                                    <option value="Chemical" <?php if ($farmer->fertilizer_type == 'Chemical') { ?> selected <?php } ?>>Chemical</option>
                                    <option value="Manure" <?php if ($farmer->fertilizer_type == 'Manure') { ?> selected <?php } ?>>Manure</option>
                                    <option value="Bio Fertilizer" <?php if ($farmer->fertilizer_type == 'Bio Fertilizer') { ?> selected <?php } ?>>Bio Fertilizer</option>
                                    <option value="Organic" <?php if ($farmer->fertilizer_type == 'Organic') { ?> selected <?php } ?>>Organic</option>
                                </select>
                                @if($errors->has('fertilizer_type'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('fertilizer_type') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.fertilizer_type_helper') }}
                                </p>
                            </div>
                            <div class="form-group col-sm-3 {{ $errors->has('fertilizer_obtain_from') ? 'has-error' : '' }}">
                                <label for="fertilizer_obtain_from">{{ trans('cruds.farmer.fields.fertilizer_obtain_from')
                            }}
                                </label>
                                <!-- <input type="text" value="{{ old('fertilizer_obtain_from', isset($farmer) ? $farmer->fertilizer_obtain_from : '') }}" id="fertilizer_obtain_from" name="fertilizer_obtain_from" class="form-control"> -->
								<select class="form-control" name="fertilizer_obtain_from" tabindex="24">
                                    <option value="">Select Fertilizer Obtain From</option>
                                    <option value="Agro" <?php if ($farmer->fertilizer_obtain_from == 'Agro') { ?> selected <?php } ?>>Agro</option>
                                    <option value="Govt Agency" <?php if ($farmer->fertilizer_obtain_from == 'Govt Agency') { ?> selected <?php } ?>>Govt Agency</option>
                                    <option value="Other" <?php if ($farmer->fertilizer_obtain_from == 'Other') { ?> selected <?php } ?>>Other</option>
                                </select>
                                @if($errors->has('fertilizer_obtain_from'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('fertilizer_obtain_from') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.fertilizer_obtain_from_helper') }}
                                </p>
                            </div>
                            <div class="form-group col-sm-3 {{ $errors->has('organic_farming') ? 'has-error' : '' }}">
                                {{ trans('cruds.farmer.fields.organic_farming') }}
                                <div class="d-flex form-control col  mt-2 p-2 ">
                                    <div class="form-check pr-2 d-flex">
                                        <input class="form-check-input" value="yes" <?php if ($farmer->organic_farming == 'yes') { ?> checked <?php } ?> id="yes" type="radio" name="organic_farming">
                                        <label class="form-check-label" for="yes">Yes</label>
                                        @if($errors->has('organic_farming'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('organic_farming') }}
                                        </em>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.farmer.fields.organic_farming_helper') }}
                                        </p>
                                    </div>
                                    <div class="form-check pr-2 d-flex">
                                        <input class="form-check-input" value="no" id="no" <?php if ($farmer->organic_farming == 'no') { ?> checked <?php } ?> type="radio" name="organic_farming">
                                        <label class="form-check-label" for="no">No</label>
                                        @if($errors->has('organic_farming'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('organic_farming') }}
                                        </em>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.farmer.fields.organic_farming_helper') }}
                                        </p>
                                    </div>

                                </div>

                            </div>
                            <div class="form-group col-sm-3 {{ $errors->has('land_organic_farming') ? 'has-error' : '' }}">
                                <label for="land_organic_farming">{{ trans('cruds.farmer.fields.land_organic_farming')
                            }}</label>
                                <input type="text" id="land_organic_farming" value="{{ old('land_organic_farming', isset($farmer) ? $farmer->land_organic_farming : '') }}" name="land_organic_farming" class="form-control">
                                @if($errors->has('land_organic_farming'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('land_organic_farming') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.land_organic_farming_helper') }}
                                </p>
                            </div>
                            <div class="form-group col-sm-3 {{ $errors->has('transport') ? 'has-error' : '' }}">
                                {{ trans('cruds.farmer.fields.transport') }}
                                <div class="d-flex form-control col  mt-2 p-2 ">
                                    <div class="form-check pr-2 d-flex">
                                        <input class="form-check-input" <?php if ($farmer->transport == 'yes') { ?> checked <?php } ?> value="yes" id="yes_transport" type="radio" name="transport">
                                        <label class="form-check-label" for="yes_transport">Yes</label>
                                        @if($errors->has('transport'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('transport') }}
                                        </em>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.farmer.fields.transport_helper') }}
                                        </p>
                                    </div>
                                    <div class="form-check pr-2 d-flex">
                                        <input class="form-check-input" <?php if ($farmer->transport == 'no') { ?> checked <?php } ?> value="no" id="no_transport" type="radio" name="transport">
                                        <label class="form-check-label" for="no_transport">No</label>
                                        @if($errors->has('transport'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('transport') }}
                                        </em>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.farmer.fields.transport_helper') }}
                                        </p>
                                    </div>

                                </div>

                            </div>
                            <div class="form-group col-sm-3 {{ $errors->has('bank_account') ? 'has-error' : '' }}">
                                {{ trans('cruds.farmer.fields.bank_account') }}
                                <div class="d-flex form-control col  mt-2 p-2 ">
                                    <div class="form-check pr-2 d-flex">
                                        <input class="form-check-input" <?php if ($farmer->bank_account == 'yes') { ?> checked <?php } ?> value="yes" id="yes_bank_account" type="radio" name="bank_account">
                                        <label class="form-check-label" for="yes_bank_account">Yes</label>
                                        @if($errors->has('bank_account'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('bank_account') }}
                                        </em>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.farmer.fields.bank_account_helper') }}
                                        </p>
                                    </div>
                                    <div class="form-check pr-2 d-flex">
                                        <input class="form-check-input" <?php if ($farmer->bank_account == 'no') { ?> checked <?php } ?> value="no" id="no_bank_account" type="radio" name="bank_account">
                                        <label class="form-check-label" for="no_bank_account">No</label>
                                        @if($errors->has('bank_account'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('bank_account') }}
                                        </em>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.farmer.fields.bank_account_helper') }}
                                        </p>
                                    </div>

                                </div>

                            </div>

                            <div class="form-group col-sm-3 {{ $errors->has('trainee_required') ? 'has-error' : '' }}">
                                {{ trans('cruds.farmer.fields.trainee_required') }}
                                <div class="d-flex form-control col  mt-2 p-2 ">
                                    <div class="form-check pr-2 d-flex">
                                        <input class="form-check-input" <?php if ($farmer->trainee_required == 'yes') { ?> checked <?php } ?> value="yes" id="yes_trainee" type="radio" name="trainee_required">
                                        <label class="form-check-label" for="yes_trainee">Yes</label>
                                        @if($errors->has('trainee_required'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('trainee_required') }}
                                        </em>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.farmer.fields.trainee_required_helper') }}
                                        </p>
                                    </div>
                                    <div class="form-check pr-2 d-flex">
                                        <input class="form-check-input" <?php if ($farmer->trainee_required == 'no') { ?> checked <?php } ?> value="no" id="no_trainee" type="radio" name="trainee_required">
                                        <label class="form-check-label" for="no_trainee">No</label>
                                        @if($errors->has('trainee_required'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('trainee_required') }}
                                        </em>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.farmer.fields.trainee_required_helper') }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-sm-3 {{ $errors->has('kisan_credit_card') ? 'has-error' : '' }}">
                                {{ trans('cruds.farmer.fields.kisan_credit_card') }}
                                <div class="d-flex form-control col  mt-2 p-2 ">
                                    <div class="form-check pr-2 d-flex">
                                        <input class="form-check-input" value="yes" <?php if ($farmer->kisan_credit_card == 'yes') { ?> checked <?php } ?> id="yes_kisan_credit_card" type="radio" name="kisan_credit_card">
                                        <label class="form-check-label" for="yes_kisan_credit_card">Yes</label>
                                        @if($errors->has('kisan_credit_card'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('kisan_credit_card') }}
                                        </em>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.farmer.fields.kisan_credit_card_helper') }}
                                        </p>
                                    </div>
                                    <div class="form-check pr-2 d-flex">
                                        <input class="form-check-input" value="no" <?php if ($farmer->kisan_credit_card == 'no') { ?> checked <?php } ?> id="no_kisan_credit_card" type="radio" name="kisan_credit_card">
                                        <label class="form-check-label" for="no_kisan_credit_card">No</label>
                                        @if($errors->has('kisan_credit_card'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('kisan_credit_card') }}
                                        </em>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.farmer.fields.kisan_credit_card_helper') }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-sm-3 {{ $errors->has('loan') ? 'has-error' : '' }}">
                                {{ trans('cruds.farmer.fields.loan') }}
                                <div class="d-flex form-control col  mt-2 p-2 ">
                                    <div class="form-check pr-2 d-flex">
                                        <input class="form-check-input" <?php if ($farmer->loan == 'yes') { ?> checked <?php } ?> value="yes" id="yes_loan" type="radio" name="loan">
                                        <label class="form-check-label" for="yes_loan">Yes</label>
                                        @if($errors->has('loan'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('loan') }}
                                        </em>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.farmer.fields.loan_helper') }}
                                        </p>
                                    </div>
                                    <div class="form-check pr-2 d-flex">
                                        <input class="form-check-input" <?php if ($farmer->loan == 'no') { ?> checked <?php } ?> value="no" id="no_loan" type="radio" name="loan">
                                        <label class="form-check-label" for="no_loan">No</label>
                                        @if($errors->has('loan'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('loan') }}
                                        </em>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.farmer.fields.loan_helper') }}
                                        </p>
                                    </div>

                                </div>

                            </div>
                            <div class="form-group col-sm-3 {{ $errors->has('shg') ? 'has-error' : '' }}">
                                {{ trans('cruds.farmer.fields.shg') }}
                                <div class="d-flex form-control col  mt-2 p-2 ">
                                    <div class="form-check pr-2 d-flex">
                                        <input class="form-check-input" <?php if ($farmer->shg == 'yes') { ?> checked <?php } ?> value="yes" id="yes_shg" type="radio" name="shg">
                                        <label class="form-check-label" for="yes_shg">Yes</label>
                                        @if($errors->has('shg'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('shg') }}
                                        </em>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.farmer.fields.shg_helper') }}
                                        </p>
                                    </div>
                                    <div class="form-check pr-2 d-flex">
                                        <input class="form-check-input" value="no" <?php if ($farmer->shg == 'no') { ?> checked <?php } ?> id="no_shg" type="radio" name="shg">
                                        <label class="form-check-label" for="no_shg">No</label>
                                        @if($errors->has('shg'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('shg') }}
                                        </em>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.farmer.fields.shg_helper') }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-sm-3 {{ $errors->has('literate') ? 'has-error' : '' }}">
                                {{ trans('cruds.farmer.fields.literate') }}
                                <div class="d-flex form-control col  mt-2 p-2 ">
                                    <div class="form-check pr-2 d-flex">
                                        <input class="form-check-input" value="yes" <?php if ($farmer->literate == 'yes') { ?> checked <?php } ?> id="yes_literate" type="radio" name="literate">
                                        <label class="form-check-label" for="yes_literate">Yes</label>
                                        @if($errors->has('literate'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('literate') }}
                                        </em>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.farmer.fields.literate_helper') }}
                                        </p>
                                    </div>
                                    <div class="form-check pr-2 d-flex">
                                        <input class="form-check-input" value="no" <?php if ($farmer->literate == 'no') { ?> checked <?php } ?> id="no_literate" type="radio" name="literate">
                                        <label class="form-check-label" for="no_literate">No</label>
                                        @if($errors->has('literate'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('literate') }}
                                        </em>
                                        @endif
                                        <p class="helper-block">
                                            {{ trans('cruds.farmer.fields.literate_helper') }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-sm-3 {{ $errors->has('seed_buy_from') ? 'has-error' : '' }}">
                                <label for="seed_buy_from">{{ trans('cruds.farmer.fields.seed_buy_from') }}</label>
                                <!-- <input type="text" value="{{ old('seed_buy_from', isset($farmer) ? $farmer->seed_buy_from : '') }}" id="seed_buy_from" name="seed_buy_from" class="form-control" required> -->
								<select class="form-control" name="seed_buy_from" tabindex="35">
                                    <option value="">Select Seeds Buy From</option>
                                    <option value="Agro" <?php if ($farmer->seed_buy_from == 'Agro') { ?> selected <?php } ?>>Agro</option>
                                    <option value="Govt Agency" <?php if ($farmer->seed_buy_from == 'Govt Agency') { ?> selected <?php } ?>>Govt Agency</option>
                                    <option value="SAU" <?php if ($farmer->seed_buy_from == 'SAU') { ?> selected <?php } ?>>SAU</option>
                                    <option value="KVK" <?php if ($farmer->seed_buy_from == 'KVK') { ?> selected <?php } ?>>KVK</option>
                                    <option value="Own" <?php if ($farmer->seed_buy_from == 'Own') { ?> selected <?php } ?>>Own</option>
                                    <option value="Other" <?php if ($farmer->seed_buy_from == 'Other') { ?> selected <?php } ?>>Other</option>
                                </select>
                                @if($errors->has('seed_buy_from'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('seed_buy_from') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.seed_buy_from_helper') }}
                                </p>
                            </div>
                            <div class="form-group col-sm-3 {{ $errors->has('manure') ? 'has-error' : '' }}">
                                <label for="manure">{{ trans('cruds.farmer.fields.manure') }}</label>
                                <input type="text" value="{{ old('manure', isset($farmer) ? $farmer->manure : '') }}" id="manure" name="manure" class="form-control" required>
                                @if($errors->has('manure'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('manure') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.manure_helper') }}
                                </p>
                            </div>
                            <div class="form-group col-sm-3 {{ $errors->has('pesticides_used') ? 'has-error' : '' }}">
                                <label for="pesticides_used">{{ trans('cruds.farmer.fields.pesticides_used') }}</label>
                                <input type="text" value="{{ old('pesticides_used', isset($farmer) ? $farmer->pesticides_used : '') }}" id="pesticides_used" name="pesticides_used" class="form-control" required>
                                @if($errors->has('pesticides_used'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('pesticides_used') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.pesticides_used_helper') }}
                                </p>
                            </div>
                            <div class="form-group col-sm-3 {{ $errors->has('cost_of_production') ? 'has-error' : '' }}">
                                <label for="cost_of_production">{{ trans('cruds.farmer.fields.cost_of_production') }}</label>
                                <input type="text" value="{{ old('cost_of_production', isset($farmer) ? $farmer->cost_of_production : '') }}" id="cost_of_production" name="cost_of_production" class="form-control" required>
                                @if($errors->has('cost_of_production'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('cost_of_production') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.cost_of_production_helper') }}
                                </p>
                            </div>

                            <div class="form-group col-sm-3 {{ $errors->has('source_of_income') ? 'has-error' : '' }}">
                                <label for="source_of_income">{{ trans('cruds.farmer.fields.source_of_income') }}</label>
                                <input type="text" value="{{ old('source_of_income', isset($farmer) ? $farmer->source_of_income : '') }}" id="source_of_income" name="source_of_income" class="form-control">
                                @if($errors->has('source_of_income'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('source_of_income') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.source_of_income_helper') }}
                                </p>
                            </div>
                            <div class="form-group col-sm-3 {{ $errors->has('farmer_tools') ? 'has-error' : '' }}">
                                <label for="farmer_tools">{{ trans('cruds.farmer.fields.farmer_tools') }}</label>
                                <input type="text" value="{{ old('farmer_tools', isset($farmer) ? $farmer->farmer_tools : '') }}" id="farmer_tools" name="farmer_tools" class="form-control">
                                @if($errors->has('farmer_tools'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('farmer_tools') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.farmer_tools_helper') }}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="float:left; margin-right:15px;">
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>

            <div style="float:left;">
                <a class="btn btn-primary" href="{{ route('admin.farmer.index') }}">Back</a>
            </div>

        </form>



    </div>

    <div class="card">
        <div class="card-header">
            {{ trans('cruds.crop.title_singular') }} {{ trans('global.list') }}
            <span style="float:right;"><a class="btn btn-xs btn-info" href="{{ route('admin.crop.edit', $farmer->id) }}">
                    Add Crop
                </a> </span>
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                    <thead>
                        <tr>


                            <th>
                                Season
                            </th>

                            <th>
                                {{ trans('cruds.crop.fields.crop_name') }}
                            </th>
                            <th>
                                Area (ha)
                            </th>
                            <th>
                                Production (M.T)
                            </th>
                            <th>
                                Average Yield (kg/ha)
                            </th>
                            <th>
                                {{ trans('cruds.crop.fields.direct') }}
                            </th>
                            <th>
                                {{ trans('cruds.crop.fields.through_intermediaries') }}
                            </th>
                            <th>
                                {{ trans('cruds.crop.fields.total') }}
                            </th>
                            <th>
                                Action
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($crops as $key => $crop)
                        <tr data-entry-id="{{ $crop->id }}">

                            <td>
                                {{ $crop->Season->name ?? '' }}
                            </td>

                            <td>
                                {{ $crop->crop_name ?? '' }}
                            </td>
                            <td>
                                {{ $crop->area ?? '' }}
                            </td>
                            <td>
                                {{ trans($crop->total_production ?? '' ) }}
                            </td>
                            <td>
                                {{ trans($crop->average_yield ?? '') }}
                            </td>
                            <td>
                                {{ trans($crop->direct ?? '') }}
                            </td>
                            <td>
                                {{ trans($crop->through_intermediaries ?? '') }}
                            </td>
                            <td>
                                {{ trans($crop->total ?? '') }}
                            </td>
                            <td>

                                <form action="{{ route('admin.crop.destroy', $crop->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                </form>

                            </td>

                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @endsection