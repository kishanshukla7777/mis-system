@extends('layouts.admin')
@section('content')
@canany(['users_manage','employee_manage','company_manage'])
<div style="margin-bottom: 10px;" class="row">
    <div class="col-lg-12">
        <a class="btn btn-success" href="{{ route("admin.farmer.create") }}">
            {{ trans('global.add') }} {{ trans('cruds.farmer.title_singular') }}
        </a>
    </div>
</div>
@endcanany
<div class="card">
    <div class="card-header">
        {{ trans('cruds.farmer.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                <thead>
                    <tr>
                        <th class="hide">
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.farmer.fields.fpo') }}
                        </th>
                        <th>
                            {{ trans('cruds.farmer.fields.farmer_name') }}
                        </th>
                        <th>
                            Village
                        </th>
                        <th>
                            Block
                        </th>

                        <th>
                            District
                        </th>
                        <th>
                            Share Price
                        </th>
                        <th>
                            Contact No.
                        </th>
                        <th class="hide">
                            Father Name
                        </th>
                        <th class="hide">
                            {{ trans('cruds.farmer.fields.email') }}
                        </th>
                        <th class="hide">
                            Caste
                        </th>
                        <th class="hide">
                            Farmer Type
                        </th>
                        <th class="hide">
                            Gender
                        </th>

                        <th class="hide">
                            Trainee Required
                        </th>
                        <th class="hide">
                            Date of birth
                        </th>
                        <th class="hide">
                            Address
                        </th>


                        <th class="hide">
                            State
                        </th>
                        <th class="hide">
                            Irrigated Area
                        </th>
                        <th class="hide">
                            Unirrigated Area
                        </th>
                        <th class="hide">
                            Aadhar Card Number
                        </th>
                        <th class="hide">
                            Kharsa No.
                        </th>
                        <th class="hide">
                            Water Source
                        </th>
                        <th class="hide">
                            Transport
                        </th>
                        <th class="hide">
                            Seed Buy From
                        </th>
                        <th class="hide">
                            Land Organic Farming
                        </th>
                        <th class="hide">
                            Manure
                        </th>
                        <th class="hide">
                            Fertilizer Type
                        </th>
                        <th class="hide">
                            Fertilizer Obtain From
                        </th>
                        <th class="hide">
                            Pesticides Used
                        </th>
                        <th class="hide">
                            Loan
                        </th class="hide">
                        <th class="hide">
                            Cost Of Production
                        </th>

                        <th class="hide">
                            Bank Account
                        </th>
                        <th class="hide">
                            Kisan credit card
                        </th>
                        <th class="hide">
                            Shg
                        </th>
                        <th class="hide">
                            Marital Status
                        </th>

                        <th class="hide">
                            Literate
                        </th>
                        <th class="hide">
                            Organic Farming
                        </th>

                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($farmers as $key => $farmer)
                    <tr data-entry-id="{{ $farmer->id }}">
                        <td class="hide">
                        </td>
                        <td>
                            {{ $farmer->id ?? '' }}
                        </td>
                        <td>
                            {{ $farmer->fpo->fpo_name ?? '' }}
                        </td>
                        <td>
                            {{ $farmer->farmer_name ?? '' }}
                        </td>
                        <td>
                            {{ $farmer->village ?? '' }}
                        </td>
                        <td>
                            {{ $farmer->block ?? '' }}
                        </td>

                        <td>
                            {{ $farmer->district ?? '' }}
                        </td>
                        <td>
                            {{ $farmer->share_price ?? '' }}
                        </td>
                        <td>
                            {{ $farmer->contact_no ?? '' }}
                        </td>
                        <td class="hide">
                            {{ $farmer->father_name ?? '' }}
                        </td>
                        <td class="hide">
                            {{ $farmer->email ?? '' }}
                        </td>
                        <td class="hide">
                            {{ $farmer->caste ?? '' }}
                        </td>
                        <td class="hide">
                            {{ $farmer->farmer_type ?? '' }}
                        </td>

                        <td class="hide">
                            {{ $farmer->gender ?? '' }}
                        </td>
                        <td class="hide">
                            {{ $farmer->trainee_required ?? '' }}
                        </td>
                        <td class="hide">
                            {{ $farmer->dob ?? '' }}
                        </td>
                        <td class="hide">
                            {{ $farmer->address ?? '' }}
                        </td>

                        <td class="hide">
                            {{ $farmer->state ?? '' }}
                        </td>
                        <td class="hide">
                            {{ $farmer->irrigated_area ?? '' }}
                        </td>
                        <td class="hide">
                            {{ $farmer->unirrigated_area ?? '' }}
                        </td>
                        <td class="hide">
                            {{ $farmer->aadhar_number ?? '' }}
                        </td>
                        <td class="hide">
                            {{ $farmer->kharsa_no ?? '' }}
                        </td>
                        <td class="hide">
                            {{ $farmer->water_source ?? '' }}
                        </td>
                        <td class="hide">
                            {{ $farmer->transport ?? '' }}
                        </td>
                        <td class="hide">
                            {{ $farmer->seed_buy_from ?? '' }}
                        </td>
                        <td class="hide">
                            {{ $farmer->land_organic_farming ?? '' }}
                        </td>
                        <td class="hide">
                            {{ $farmer->manure ?? '' }}
                        </td>
                        <td class="hide">
                            {{ $farmer->fertilizer_type ?? '' }}
                        </td>
                        <td class="hide">
                            {{ $farmer->fertilizer_obtain_from ?? '' }}
                        </td>
                        <td class="hide">
                            {{ $farmer->pesticides_used ?? '' }}
                        </td>
                        <td class="hide">
                            {{ $farmer->loan ?? '' }}
                        </td>
                        <td class="hide">
                            {{ $farmer->cost_of_production ?? '' }}
                        </td>
                        <td class="hide">
                            {{ $farmer->bank_account ?? '' }}
                        </td>
                        <td class="hide">
                            {{ $farmer->kisan_credit_card ?? '' }}
                        </td>
                        <td class="hide">
                            {{ $farmer->shg ?? '' }}
                        </td>
                        <td class="hide">
                            {{ $farmer->marital_status ?? '' }}
                        </td>
                        <td class="hide">
                            {{ $farmer->literate ?? '' }}
                        </td>
                        <td class="hide">
                            {{ $farmer->organic_farming ?? '' }}
                        </td>

                        <td>
                            <!-- <a class="btn btn-xs btn-primary" href="{{ route('admin.farmer.show', $farmer->id) }}">
                                    {{ trans('global.view') }}
                                </a> -->
                            @canany(['users_manage','employee_manage','company_manage'])

                            <a class="btn btn-xs btn-info" href="{{ route('admin.crop.edit', $farmer->id) }}">
                                Add Crop
                            </a>

                            <a class="btn btn-xs btn-info" href="{{ route('admin.farmer.edit', $farmer->id) }}">
                                {{ trans('global.edit') }}
                            </a>

                            <form action="{{ route('admin.farmer.destroy', $farmer->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                <input type="hidden" name="_method" value="DELETE">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                            </form>
                            @endcanany
                        </td>

                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>


    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    $(function() {
        let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
        @can('users_manage')
        //   let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
        //   let deleteButton = {
        //     text: deleteButtonTrans,
        //     url: "{{ route('admin.users.mass_destroy') }}",
        //     className: 'btn-danger',
        //     action: function (e, dt, node, config) {
        //       var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
        //           return $(entry).data('entry-id')
        //       });

        //       if (ids.length === 0) {
        //         alert('{{ trans('global.datatables.zero_selected') }}')

        //         return
        //       }

        //       if (confirm('{{ trans('global.areYouSure') }}')) {
        //         $.ajax({
        //           headers: {'x-csrf-token': _token},
        //           method: 'POST',
        //           url: config.url,
        //           data: { ids: ids, _method: 'DELETE' }})
        //           .done(function () { location.reload() })
        //       }
        //     }
        //   }
        //   dtButtons.push(deleteButton)
        @endcan

        $.extend(true, $.fn.dataTable.defaults, {
            order: [
                [1, 'desc']
            ],
            pageLength: 100,
        });
        $('.datatable-User:not(.ajaxTable)').DataTable({
            buttons: dtButtons
        })
        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
            $($.fn.dataTable.tables(true)).DataTable()
                .columns.adjust();
        });
    })
</script>
@endsection