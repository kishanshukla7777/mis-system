@extends('layouts.admin')
@section('content')
@canany(['users_manage','employee_manage','company_manage'])
<div style="margin-bottom: 10px;" class="row">
    <div class="col-lg-12">
        <a class="btn btn-success" href="{{ route("admin.farmer.create") }}">
            {{ trans('global.add') }} {{ trans('cruds.farmer.title_singular') }}
        </a>
    </div>
</div>
@endcanany
<div class="card">
    <div class="card-header">
        {{ trans('cruds.farmer.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.user.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.farmer.fields.fpo') }}
                        </th>
                        <th>
                            {{ trans('cruds.farmer.fields.farmer_name') }}
                        </th>
                        <th>
                            {{ trans('cruds.farmer.fields.email') }}
                        </th>

                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($farmers as $key => $farmer)
                    <tr data-entry-id="{{ $farmer->id }}">
                        <td>

                        </td>
                        <td>
                            {{ $farmer->id ?? '' }}
                        </td>
                        <td>
                            {{ $farmer->fpo_name ?? '' }}
                        </td>
                        <td>
                            {{ $farmer->farmer_name ?? '' }}
                        </td>
                        <td>
                            {{ $farmer->email ?? '' }}
                        </td>

                        <td>
                            <!-- <a class="btn btn-xs btn-primary" href="{{ route('admin.farmer.show', $farmer->id) }}">
                                    {{ trans('global.view') }}
                                </a> -->
                            @canany(['users_manage','employee_manage','company_manage'])

                            <a class="btn btn-xs btn-info" href="{{ route('admin.crop.edit', $farmer->id) }}">
                                Add Crop
                            </a>

                            <a class="btn btn-xs btn-info" href="{{ route('admin.farmer.edit', $farmer->id) }}">
                                {{ trans('global.edit') }}
                            </a>

                            <form action="{{ route('admin.farmer.destroy', $farmer->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                <input type="hidden" name="_method" value="DELETE">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                            </form>
                            @endcanany
                        </td>

                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>


    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    $(function() {
        let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
        @can('users_manage')
        //   let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
        //   let deleteButton = {
        //     text: deleteButtonTrans,
        //     url: "{{ route('admin.users.mass_destroy') }}",
        //     className: 'btn-danger',
        //     action: function (e, dt, node, config) {
        //       var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
        //           return $(entry).data('entry-id')
        //       });

        //       if (ids.length === 0) {
        //         alert('{{ trans('global.datatables.zero_selected') }}')

        //         return
        //       }

        //       if (confirm('{{ trans('global.areYouSure') }}')) {
        //         $.ajax({
        //           headers: {'x-csrf-token': _token},
        //           method: 'POST',
        //           url: config.url,
        //           data: { ids: ids, _method: 'DELETE' }})
        //           .done(function () { location.reload() })
        //       }
        //     }
        //   }
        //   dtButtons.push(deleteButton)
        @endcan

        $.extend(true, $.fn.dataTable.defaults, {
            order: [
                [0, 'desc']
            ],
            pageLength: 10,
        });
        $('.datatable-User:not(.ajaxTable)').DataTable({
            buttons: dtButtons
        })
        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
            $($.fn.dataTable.tables(true)).DataTable()
                .columns.adjust();
        });
    })
</script>
@endsection