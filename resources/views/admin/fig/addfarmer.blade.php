@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.fig.title_singular') }}
    </div>

    <div class="card-body">
        <form action="{{ route("admin.fig.savefarmer") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group {{ $errors->has('fpo_id') ? 'has-error' : '' }}">
                <label for="fpo_id">Fpo : </label>
                <input type="hidden" name="fpo_id" id="fpo_id" value="{{$fig->fpo->id}}" readonly>
                <span><b>{{$fig->fpo->fpo_name}}</b></span>
            </div>
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name">Group name : </label>
                <input type="hidden" name="fig_id" id="fig_id" value="{{$fig->id}}" readonly>
                <span><b>{{$fig->name}}</b></span>
            </div>

            <div class="form-group {{ $errors->has('fpo_id') ? 'has-error' : '' }}">
                <label for="fpo_id">Farmer</label>

                <select multiple="multiple" size="10" id="duallistbox_demo1" name="duallistbox_demo1[]" required>
                    <option selected disabled>Please select Farmer</option>

                    @foreach ($farmers as $key => $farmer)
                    <option value="{{$farmer->id}}" <?php if ($farmer->fig_farmer != null) {
                                                        if (
                                                            $farmer->id == $farmer->fig_farmer->farmer_id &&
                                                            $farmer->fig_farmer->fig_id == $fig->id
                                                        ) { ?> selected <?php } ?> <?php
                                                                                } ?>>{{$farmer->farmer_name}} - {{$farmer->village}}</option>


                    @endforeach
                </select>

                @if($errors->has('fpo_id'))
                <em class="invalid-feedback">
                    {{ $errors->first('fpo_id') }}
                </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.roles_helper') }}
                </p>
            </div>


            <div style="float:left;margin-right:15px;">
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
            <div style="float:left;">
                <a class="btn btn-primary" href="{{ route('admin.fig.index') }}">Back</a>
            </div>
        </form>


    </div>
</div>
@endsection