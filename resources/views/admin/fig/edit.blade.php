@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.fig.title_singular') }}
    </div>

    <div class="card-body">
        <form action="{{ route("admin.fig.update", [$fig->id]) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <input type="hidden" name="created_by" value="{{Auth::id()}}">
            <div class="form-group {{ $errors->has('fpo_id') ? 'has-error' : '' }}">
                <label for="fpo_id">Fpo*</label>
                <select  name="fpo_id" id="fpo_id" class="form-control">
                        <option selected disabled>Please select fpo</option>
                        @foreach ($fpos as $key => $fpo)
                        <option value="{{ $fpo->id }}"
                        {{ !empty(old('fpo_id')) && old('fpo_id') == $value->id
                            ? 'selected="selected"'
                            : ((!empty($fig) && !empty($fig->fpo_id)) && $fig->fpo_id == $fpo->id
                                ? 'selected="selected"'
                                : "")
                        }}>{{ $fpo->fpo_name }}</option>
                    @endforeach

                    </select>
                  
                @if($errors->has('fpo_id'))
                    <em class="invalid-feedback">
                        {{ $errors->first('fpo_id') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.roles_helper') }}
                </p>
            </div>

            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name">{{ trans('cruds.fig.fields.name_group') }}*</label>
                <input type="text" id="name" name="name" class="form-control" value="{{ old('name', isset($fig) ? $fig->name : '') }}" required>
                @if($errors->has('name'))
                    <em class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.fig.fields.name_group_helper') }}
                </p>
            </div>
            <div style="float:left;margin-right:15px;">
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
            <div style="float:left;">
                <a class="btn btn-primary" href="{{ route('admin.fig.index') }}">Back</a>
            </div>
        </form>


    </div>
</div>
@endsection