@extends('layouts.admin')
@section('content')
@canany(['users_manage','employee_manage','company_manage'])
<div style="margin-bottom: 10px;" class="row">
    <div class="col-lg-12">
        <a class="btn btn-success" href="{{ route("admin.fig.create") }}">
            {{ trans('global.add') }} {{ trans('cruds.fig.title_singular') }}
        </a>
    </div>
</div>
@endcanany
<div class="card">
    <div class="card-header">
        {{ trans('cruds.fig.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                <thead>
                    <tr>
                        <th class="hide">
                        </th>
                        <th>
                            {{ trans('cruds.fig.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.fig.fields.fpo_id') }}
                        </th>
                        <th>
                            FIG Name
                        </th>
                        <th>
                            LRP Name
                        </th>
                        <th>
                            LRP Mobile
                        </th>

                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($figs as $key => $fig)
                    <tr data-entry-id="{{ $fig->id }}">
                        <td class="hide">
                        </td>
                        <td>
                            {{ $fig->id ?? '' }}
                        </td>
                        <td>
                            {{ $fig->fpo->fpo_name ?? '' }}
                        </td>
                        <td>
                            {{ $fig->name ?? '' }}
                        </td>
                        <td>
                            <?php

                            foreach ($fig->fig_farmer as $val) {
                                if ($val->lrp == 1) {
                                    $farmer = App\farmer::where('id', $val->farmer_id)->first();
                            ?>
                                    {{$farmer->farmer_name ?? ''}}
                                <?php } else { ?>

                            <?php }
                            } ?>
                        </td>
                        <td>
                            <?php
                            foreach ($fig->fig_farmer as $val) {
                                if ($val->lrp == 1) {
                                    $farmer =  App\farmer::where('id', $val->farmer_id)->first();
                            ?>
                                    {{$farmer->contact_no ?? ''}}
                                <?php } else { ?>

                            <?php }
                            } ?>

                        </td>


                        <td>
                            @canany(['users_manage','employee_manage','company_manage'])
                            <a class="btn btn-xs btn-info" href="{{ route('admin.fig.addfarmer', $fig->id) }}">
                                Add Farmers
                            </a>

                            <a class="btn btn-xs btn-info" href="{{ route('admin.fig.addlrp', $fig->id) }}">
                                Add LRP
                            </a>

                            <a class="btn btn-xs btn-info" href="{{ route('admin.fig.edit', $fig->id) }}">
                                {{ trans('global.edit') }}
                            </a>

                            <form action="{{ route('admin.fig.destroy', $fig->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                <input type="hidden" name="_method" value="DELETE">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                            </form>
                            @endcanany
                        </td>

                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>


    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    $(function() {
        let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
        // @can('users_manage')
        //   let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
        //   let deleteButton = {
        //     text: deleteButtonTrans,
        //     url: "{{ route('admin.users.mass_destroy') }}",
        //     className: 'btn-danger',
        //     action: function (e, dt, node, config) {
        //       var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
        //           return $(entry).data('entry-id')
        //       });

        //       if (ids.length === 0) {
        //         alert('{{ trans('global.datatables.zero_selected') }}')

        //         return
        //       }

        //       if (confirm('{{ trans('global.areYouSure') }}')) {
        //         $.ajax({
        //           headers: {'x-csrf-token': _token},
        //           method: 'POST',
        //           url: config.url,
        //           data: { ids: ids, _method: 'DELETE' }})
        //           .done(function () { location.reload() })
        //       }
        //     }
        //   }
        //   dtButtons.push(deleteButton)
        // @endcan

        $.extend(true, $.fn.dataTable.defaults, {
            order: [
                [1, 'desc']
            ],
            pageLength: 10,
        });
        $('.datatable-User:not(.ajaxTable)').DataTable({
            buttons: dtButtons
        })
        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
            $($.fn.dataTable.tables(true)).DataTable()
                .columns.adjust();
        });
    })
</script>
@endsection