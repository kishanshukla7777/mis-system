@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} Show LRP
    </div>

    <div class="card-body">
        <form action="{{ route("admin.fig.savelrp") }}" method="POST" enctype="multipart/form-data">
            @csrf
         
            <div class="form-group {{ $errors->has('fpo_id') ? 'has-error' : '' }}">
                <label for="fpo_id">Farmer</label>
               
                <select id="selectFarmer"  class="form-control" name="selectFarmer" required>
                <option selected disabled>Please select Farmer</option>
                    
                    @foreach ($figFarmers as $key => $figFarmer)
                    <option value="{{$figFarmer->id}}" <?php if($figFarmer->lrp == 1) { ?> selected <?php } ?>>{{$figFarmer->farmer->farmer_name}}</option>
                    @endforeach
                    </select>
                  
                @if($errors->has('fpo_id'))
                    <em class="invalid-feedback">
                        {{ $errors->first('fpo_id') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.roles_helper') }}
                </p>
            </div>

            <div style="float:left;margin-right:15px;">
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
            <div style="float:left;">
                <a class="btn btn-primary" href="{{ route('admin.fig.index') }}">Back</a>
            </div>
        </form>


    </div>
</div>
@endsection