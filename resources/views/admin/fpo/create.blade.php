@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.fpo.title_singular') }}
    </div>

    <div class="card-body">
        <form id="fpoForm" enctype="multipart/form-data" action="{{ route("admin.fpo.store") }}" method="POST">
            @csrf
            
            @canany(['users_manage'])
            <div class="row">
            <div class="form-group col{{ $errors->has('company_id') ? 'has-error' : '' }}">
                <label for="company_id">Company</label>
                <select  name="company_id" id="company_id" class="form-control">
                        <option selected disabled>Please select Company</option>
                        @foreach ($companys as $key => $company)
                        <option value="{{ $company->id }}"
                        {{ !empty(old('company')) && old('company_id') == $company->id
                            ? 'selected="selected"'
                            : ((!empty($farmer) && !empty($farmer->fpo_id)) && $fpo->id == $company->id
                                ? 'selected="selected"'
                                : "")
                        }}>{{ $company->company_name }}</option>
                    @endforeach

                    </select>
                  
                @if($errors->has('fpo_id'))
                    <em class="invalid-feedback">
                        {{ $errors->first('fpo_id') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.roles_helper') }}
                </p>
            </div>
            </div>
            @endcanany


            <div class="row">
            <div class="form-group col{{ $errors->has('fpo_name') ? 'has-error' : '' }}">
                <label for="fpo_name">{{ trans('cruds.fpo.fields.fpo_name') }}*</label>
                <input type="text" id="fpo_name" name="fpo_name" class="form-control"
                    value="{{ old('fpo_name', isset($fpo) ? $fpo->fpo_name : '') }}">
                @if($errors->has('fpo_name'))
                <em class="invalid-feedback">
                    {{ $errors->first('fpo_name') }}
                </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.fpo.fields.fpo_name_helper') }}
                </p>
            </div>
            <div class="form-group col{{ $errors->has('address') ? 'has-error' : '' }}">
                <label for="address">{{ trans('cruds.fpo.fields.address') }}*</label>
                <input type="text" id="address" name="address" class="form-control"
                    value="{{ old('address', isset($fpo) ? $fpo->address : '') }}">
                @if($errors->has('address'))
                <em class="invalid-feedback">
                    {{ $errors->first('address') }}
                </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.fpo.fields.address_helper') }}
                </p>
            </div>
        </div>
        <div class="row">
            <div class="form-group col {{ $errors->has('taluka') ? 'has-error' : '' }}">
                <label for="taluka">{{ trans('cruds.fpo.fields.taluka') }}*</label>
                <input type="text" id="taluka" value="{{ old('taluka', isset($fpo) ? $fpo->taluka : '') }}" name="taluka" class="form-control">
                @if($errors->has('taluka'))
                <em class="invalid-feedback">
                    {{ $errors->first('taluka') }}
                </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.fpo.fields.taluka_helper') }}
                </p>
            </div>
            <div class="form-group col {{ $errors->has('district') ? 'has-error' : '' }}">
                <label for="district">{{ trans('cruds.fpo.fields.district') }}*</label>
                <input type="text" id="district" value="{{ old('district', isset($fpo) ? $fpo->district : '') }}" name="district" class="form-control">
                @if($errors->has('district'))
                <em class="invalid-feedback">
                    {{ $errors->first('district') }}
                </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.fpo.fields.district_helper') }}
                </p>
            </div>
        </div>
        <div class="row">
            <div class="form-group col {{ $errors->has('state') ? 'has-error' : '' }}">
                <label for="state">{{ trans('cruds.fpo.fields.state') }}*</label>
                <input type="text" id="state" name="state" value="{{ old('state', isset($fpo) ? $fpo->state : '') }}" class="form-control">
                @if($errors->has('state'))
                <em class="invalid-feedback">
                    {{ $errors->first('state') }}
                </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.fpo.fields.state_helper') }}
                </p>
            </div>
            <div class="form-group col {{ $errors->has('email') ? 'has-error' : '' }}">
                <label for="email">{{ trans('cruds.fpo.fields.email') }}*</label>
                <input type="email" id="email" name="email" value="{{ old('email', isset($fpo) ? $fpo->email : '') }}" class="form-control">
                
                @if($errors->has('email'))
                <em class="invalid-feedback">
                    {{ $errors->first('email') }}
                </em>
                 @endif
                    <p class="helper-block">
                {{ trans('cruds.user.fields.email_helper') }}
                    </p>
            </div>
        </div>
        <div class="row">
            <div class="form-group col {{ $errors->has('contact_person') ? 'has-error' : '' }}">
                <label for="contact_person">{{ trans('cruds.fpo.fields.contact_person') }}*</label>
                <input type="text" id="contact_person" value="{{ old('contact_person', isset($fpo) ? $fpo->contact_person : '') }}" name="contact_person" class="form-control">
                @if($errors->has('contact_person'))
                <em class="invalid-feedback">
                    {{ $errors->first('contact_person') }}
                </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.fpo.fields.contact_person_helper') }}
                </p>
            </div>
            <div class="form-group col {{ $errors->has('mobile_no') ? 'has-error' : '' }}">
                <label for="mobile_no">{{ trans('cruds.fpo.fields.mobile_no') }}*</label>
                <input type="number" id="mobile_no" name="mobile_no"  value="{{ old('mobile_no', isset($fpo) ? $fpo->mobile_no : '') }}" class="form-control">
                @if($errors->has('mobile_no'))
                <em class="invalid-feedback">
                    {{ $errors->first('mobile_no') }}
                </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.fpo.fields.mobile_no_helper') }}
                </p>

            </div>
        </div>
            <div class="form-group row align-items-end{{ $errors->has('gst_no') ? 'has-error' : '' }}">
                <div class="col">
                <label for="gst_no">{{ trans('cruds.fpo.fields.gst_no') }}</label>
                <input type="text" id="gst_no"  value="{{ old('gst_no', isset($fpo) ? $fpo->gst_no : '') }}" name="gst_no" class="form-control">
                @if($errors->has('gst_no'))
                <em class="invalid-feedback">
                    {{ $errors->first('gst_no') }}
                </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.fpo.fields.gst_no_helper') }}
                </p>
            </div>

                <div class="input-group mb-3 col">
                       <div class="form-control">
                            <div class="form-group @if ($errors->has('gst_doc')) has-error @endif ">
                            <input type="file" name="gst_doc" id="gst_doc" class="custom-file-input" value="{{ old('gst_doc', isset($fpo) ? $fpo->gst_doc : '') }}"/>
                            @if ($errors->has('gst_doc'))
                                <span class="text-danger">{{ $errors->first('gst_doc') }}</span>
                            @endif
                            <label class="custom-file-label" for="gst_doc">
                            Choose file</label>
                            </div>
                       </div>
                </div>
            </div>
      

            <div class="form-group  row align-items-end {{ $errors->has('tan_no') ? 'has-error' : '' }}">
                <div class="col">
                <label for="tan_no">{{ trans('cruds.fpo.fields.tan_no') }}</label>
                <input type="text" id="tan_no" value="{{ old('tan_no', isset($fpo) ? $fpo->tan_no : '') }}" name="tan_no" class="form-control">
                @if($errors->has('tan_no'))
                <em class="invalid-feedback">
                    {{ $errors->first('tan_no') }}
                </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.fpo.fields.tan_no_helper') }}
                </p>
            </div>

                <div class="input-group mb-3 col ">
                    <div class="custom-file">

                    <div class="form-group @if ($errors->has('tan_doc')) has-error @endif ">
                     
                     <input type="file" name="tan_doc" id="tan_doc" class="custom-file-input mb-5" value="{{ old('tan_doc', isset($fpo) ? $fpo->tan_doc : '') }}"/>
                     @if ($errors->has('tan_doc'))
                         <span class="text-danger">{{ $errors->first('tan_doc') }}</span>
                     @endif
                     <label class="custom-file-label" for="tan_doc">Choose
                     file</label>
                    </div>
              
                    </div>
                 
                </div>
            </div>
            <div class="form-group  row align-items-end {{ $errors->has('pan_no') ? 'has-error' : '' }}">

               <div class="col">
                <label for="pan_no">{{ trans('cruds.fpo.fields.pan_no') }}</label>
                <input type="text" id="pan_no" name="pan_no" value="{{ old('pan_no', isset($fpo) ? $fpo->pan_no : '') }}" class="form-control">
                @if($errors->has('pan_no'))
                <em class="invalid-feedback">
                    {{ $errors->first('pan_no') }}
                </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.fpo.fields.pan_no_helper') }}
                </p>
            </div>

                <div class="input-group mb-3 col ">
                    <div class="custom-file">
                    <div class="form-group @if ($errors->has('pan_doc')) has-error @endif ">
                     
                     <input type="file" name="pan_doc" value="{{ old('pan_doc', isset($fpo) ? $fpo->pan_doc : '') }}" id="pan_doc" class="custom-file-input mb-5" />
                     @if ($errors->has('pan_doc'))
                         <span class="text-danger">{{ $errors->first('pan_doc') }}</span>
                     @endif
                     <label class="custom-file-label" for="pan_doc">Choose
                     file</label>
                 
             </div>
   
                    </div>
                 
                </div>
            </div>
            <div class="form-group row align-items-end {{ $errors->has('cin_no') ? 'has-error' : '' }}">
                <div class=" col">
                    <label for="cin_no">{{ trans('cruds.fpo.fields.cin_no') }}</label>
                    <input type="text" id="cin_no" value="{{ old('cin_no', isset($fpo) ? $fpo->cin_no : '') }}" name="cin_no" class="form-control">
                    @if($errors->has('cin_no'))
                    <em class="invalid-feedback">
                        {{ $errors->first('cin_no') }}
                    </em>
                    @endif
                    <p class="helper-block">
                        {{ trans('cruds.fpo.fields.cin_no_helper') }}
                    </p>
                </div>
                <div class="input-group mb-3 col">
                    <div class="custom-file">
                    <div class="form-group @if ($errors->has('cin_doc')) has-error @endif ">
                     
                     <input type="file" name="cin_doc" id="cin_doc" class="custom-file-input mb-5" value="{{ old('cin_doc', isset($fpo) ? $fpo->cin_doc : '') }}"/>
                     @if ($errors->has('cin_doc'))
                         <span class="text-danger">{{ $errors->first('cin_doc') }}</span>
                     @endif
                     <label class="custom-file-label" for="cin_doc">Choose
                     file</label>
                 
             </div>
   
                    </div>
                   
                </div>
            </div>

           
            <div class="form-group row align-items-end {{ $errors->has('other') ? 'has-error' : '' }}">
                <div class=" col">
                    <label for="other">{{ trans('cruds.fpo.fields.other') }}</label>
                    <a id="addotherDoc" style="float:right;"><i class="fa fa-plus" aria-hidden="true"></i></a>
                    <input type="text" name="other[]" id="other" value="{{ old('other', isset($fpo) ? $fpo->other : '') }}" class="form-control">
                    @if($errors->has('other'))
                    <em class="invalid-feedback">
                        {{ $errors->first('other') }}
                    </em>
                    @endif
                    <p class="helper-block">
                        {{ trans('cruds.fpo.fields.other_helper') }}
                    </p>
                </div>
                <div class="input-group mb-3 col">
                    <div class="custom-file">
                    <div class="form-group @if ($errors->has('other_doc')) has-error @endif ">
                     
                     <input type="file" name="other_doc[]" id="other_doc" class="custom-file-input mb-5" value="{{ old('other_doc', isset($fpo) ? $fpo->other_doc : '') }}"/>
                     @if ($errors->has('other_doc'))
                         <span class="text-danger">{{ $errors->first('other_doc') }}</span>
                     @endif
                     <label class="custom-file-label" for="other_doc">Choose
                     file</label>
                 
             </div>
   
                    </div>
                   
                </div>
                <div class="otherAppendData" style="float: left;width: 100%;margin: 0px 0px;">
               
                </div>
               </div>

               @canany(['users_manage'])
               <div class="row">
            <div class="form-group col{{ $errors->has('assign_emp') ? 'has-error' : '' }}">
                <label for="assign_emp">Assign Employee</label>
                <select  name="assign_emp" id="assign_emp1" class="form-control">
                        <option selected disabled>Please select Employee</option>
                </select>
                  
                @if($errors->has('fpo_id'))
                    <em class="invalid-feedback">
                        {{ $errors->first('fpo_id') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.roles_helper') }}
                </p>
            </div>
            </div>
               @endcanany
            @canany(['company_manage'])
            <div class="row">
            <div class="form-group col{{ $errors->has('assign_emp') ? 'has-error' : '' }}">
                <label for="assign_emp">Assign Employee</label>
                <select  name="assign_emp" id="assign_emp" class="form-control">
                        <option selected disabled>Please select Employee</option>
                        @foreach ($customers as $key => $customer)
                        <option value="{{ $customer->id }}"
                        {{ !empty(old('assign_emp')) && old('assign_emp') == $customer->id
                            ? 'selected="selected"'
                            : ((!empty($farmer) && !empty($farmer->fpo_id)) && $fpo->id == $customer->id
                                ? 'selected="selected"'
                                : "")
                        }}>{{ $customer->first_name }} {{ $customer->last_name }}</option> 
                    @endforeach

                    </select>
                  
                @if($errors->has('fpo_id'))
                    <em class="invalid-feedback">
                        {{ $errors->first('fpo_id') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.roles_helper') }}
                </p>
            </div>
            </div>
            @endcanany


            <div style="float:left; margin-right:15px;">
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
            <div style="float:left;">
                <a class="btn btn-primary" href="{{ route('admin.fpo.index') }}">Back</a>
            </div>
        </form>


    </div>
</div>
<script>

</script>
@endsection

