@extends('layouts.admin')
@section('content')

@canany(['users_manage','company_manage'])
<div style="margin-bottom: 10px;" class="row">
    <div class="col-lg-12">
        <a class="btn btn-success" href="{{ route("admin.fpo.create") }}">
            {{ trans('global.add') }} {{ trans('cruds.fpo.title_singular') }}
        </a>
    </div>
</div>
@endcanany
<div class="card">
    <div class="card-header">
        {{ trans('cruds.fpo.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                <thead>
                    <tr>
                        <td class="hide">
                        </td>
                        <th>
                            {{ trans('cruds.fpo.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.fpo.fields.fpo_company') }}
                        </th>

                        <th>
                            {{ trans('cruds.fpo.fields.fpo_name') }}
                        </th>
                        <th>
                            {{ trans('cruds.fpo.fields.email') }}
                        </th>

                        <th class="hide">
                            Address
                        </th>
                        <th>
                            {{ trans('cruds.fpo.fields.taluka') }}
                        </th>
                        <th>
                            {{ trans('cruds.fpo.fields.district') }}
                        </th>
                        <th>
                            {{ trans('cruds.fpo.fields.state') }}
                        </th>

                        <th>
                            {{ trans('cruds.fpo.fields.contact_person') }}
                        </th>
                        <th>
                            {{ trans('cruds.fpo.fields.mobile_no') }}
                        </th>
                        <th>
                            {{ trans('cruds.fpo.fields.gst_no') }}
                        </th>
                        <th>
                            {{ trans('cruds.fpo.fields.tan_no') }}
                        </th>
                        <th>
                            {{ trans('cruds.fpo.fields.pan_no') }}
                        </th>
                        <th>
                            {{ trans('cruds.fpo.fields.cin_no') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($fpos as $key => $fpo)
                    <tr data-entry-id="{{ $fpo->id }}">
                        <th class="hide">
                        </th>
                        <td>
                            {{ $fpo->id ?? '' }}
                        </td>
                        <td>
                            {{ $fpo->company->company_name ?? '' }}
                        </td>
                        <td>
                            {{ $fpo->fpo_name ?? '' }}
                        </td>
                        <td>
                            {{ $fpo->email ?? '' }}
                        </td>

                        <td class="hide">
                            {{ $fpo->address ?? '' }}
                        </td>
                        <td>
                            {{ $fpo->taluka ?? '' }}
                        </td>
                        <td>
                            {{ $fpo->district ?? '' }}
                        </td>
                        <td>
                            {{ $fpo->state ?? '' }}
                        </td>

                        <td>
                            {{ $fpo->contact_person ?? '' }}
                        </td>
                        <td>
                            {{ $fpo->mobile_no ?? '' }}
                        </td>
                        <td>
                            {{ $fpo->gst_no ?? '' }}
                        </td>
                        <td>
                            {{ $fpo->tan_no ?? '' }}
                        </td>
                        <td>
                            {{ $fpo->pan_no ?? '' }}
                        </td>
                        <td>
                            {{ $fpo->cin_no ?? '' }}
                        </td>

                        @canany(['users_manage','company_manage'])
                        <td>
                            <!-- <a class="btn btn-xs btn-primary" href="{{ route('admin.fpo.show', $fpo->id) }}">
                                    {{ trans('global.view') }}
                                </a> -->

                            <a class="btn btn-xs btn-info" href="{{ route('admin.fpo.edit', $fpo->id) }}">
                                {{ trans('global.edit') }}
                            </a>

                            <form action="{{ route('admin.fpo.destroy', $fpo->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                <input type="hidden" name="_method" value="DELETE">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                            </form>

                        </td>
                        @endcanany

                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>


    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    $(function() {
        let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
        @canany(['1company_manage'])
        let deleteButtonTrans = '{{ trans('
        global.datatables.delete ') }}'
        let deleteButton = {
            text: deleteButtonTrans,
            url: "{{ route('admin.fpo.mass_destroy') }}",
            className: 'btn-danger',
            action: function(e, dt, node, config) {
                var ids = $.map(dt.rows({
                    selected: true
                }).nodes(), function(entry) {
                    return $(entry).data('entry-id')
                });

                if (ids.length === 0) {
                    alert('{{ trans('
                        global.datatables.zero_selected ') }}')

                    return
                }

                if (confirm('{{ trans('
                        global.areYouSure ') }}')) {
                    $.ajax({
                            headers: {
                                'x-csrf-token': _token
                            },
                            method: 'POST',
                            url: config.url,
                            data: {
                                ids: ids,
                                _method: 'DELETE'
                            }
                        })
                        .done(function() {
                            location.reload()
                        })
                }
            }
        }
        dtButtons.push(deleteButton)
        @endcanany

        $.extend(true, $.fn.dataTable.defaults, {
            order: [
                [1, 'desc']
            ],
            pageLength: 10,
        });
        $('.datatable-User:not(.ajaxTable)').DataTable({
            buttons: dtButtons
        })
        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
            $($.fn.dataTable.tables(true)).DataTable()
                .columns.adjust();
        });
    })
</script>
@endsection