@extends('layouts.admin')

@section('content')
<div class="content">
    <div class="row">
        @can('users_manage')
        <div class="col-lg-12">
            Home
        </div>
        @endcan


        @can('company_manage')
        <div class="col-lg-12">
            <div class="row justify-content-center">
                <div class="col-lg-3 col-sm-6 col-xs-12">
                    <div class="white-box analytics-info">
                        <h3 class="box-title">FPOS</h3>
                        <ul class="list-inline two-part d-flex align-items-center mb-0">

                            <li class="ml-auto"><span class="counter text-success">
                                    <h4>{{$fpoCount}}</h4>
                                </span></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 col-xs-12">
                    <div class="white-box analytics-info">
                        <h3 class="box-title">FIG</h3>
                        <ul class="list-inline two-part d-flex align-items-center mb-0">
                            <li class="ml-auto"><span class="counter text-purple">
                                    <h4>
                                        <?php
                                        $a = array();
                                        foreach ($fpo as $key => $fpos) {
                                            $a[] = $fpos->fig->count();
                                        ?>


                                        <?php } ?>

                                        <?php $aa = array_sum($a); ?>
                                        {{$aa}}

                                    </h4>
                                </span></li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-xs-12">
                    <div class="white-box analytics-info">
                        <h3 class="box-title">FARMERS</h3>
                        <ul class="list-inline two-part d-flex align-items-center mb-0">
                            <li class="ml-auto"><span class="counter text-info">
                                    <h4> <?php
                                            $a = array();
                                            foreach ($fpo as $key => $fpos) {
                                                $a[] = $fpos->farmer->count()
                                            ?>


                                        <?php } ?>

                                        <?php $aa = array_sum($a); ?>
                                        {{$aa}}
                                    </h4>
                                </span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 col-xs-12">
                    <div class="white-box analytics-info">
                        <h3 class="box-title">Share Price</h3>
                        <ul class="list-inline two-part d-flex align-items-center mb-0">
                            <li class="ml-auto"><span class="counter text-info">
                                    <h4>
                                        <?php
                                        $a = array();
                                        foreach ($fpo as $key => $fpos) {
                                            $a[] = $fpos->farmer->sum('share_price');
                                        ?>


                                        <?php } ?>

                                        <?php $aa = array_sum($a); ?>
                                        {{$aa}}
                                    </h4>
                                </span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="card" style="margin-top:50px;">

                <div class="card-body">
                    <div class="table-responsive">
                        <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                            <thead>
                                <tr>

                                    <th>
                                        {{ trans('cruds.user.fields.id') }}
                                    </th>
                                    <th>
                                        FPO Name
                                    </th>
                                    <th>
                                        FIG
                                    </th>
                                    <th>
                                        Farmer
                                    </th>
                                    <th>
                                        Share Price
                                    </th>

                                </tr>
                            </thead>
                            <tbody>
                                @foreach($fpo as $key => $fpos)
                                <tr data-entry-id="{{ $fpos->id }}">

                                    <td>
                                        {{ $fpos->id ?? '' }}
                                    </td>
                                    <td>
                                        {{ $fpos->fpo_name ?? '' }}
                                    </td>
                                    <td>
                                        {{ $fpos->fig->count() ?? '' }}
                                    </td>
                                    <td>
                                        {{ $fpos->farmer->count() ?? '' }}
                                    </td>
                                    <td>
                                        {{ $fpos->farmer->sum('share_price') ?? '' }}
                                    </td>

                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>


                </div>
            </div>


            <div class="card" style="margin-top:50px;">

                <div class="card-body">
                    <div class="table-responsive">
                        <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                            <thead>
                                <tr>
                                    <th>
                                        FPO Name
                                    </th>
                                    <th>
                                        Village
                                    </th>
                                    <th>
                                        Total Farmer
                                    </th>
                                    <th>
                                        Male
                                    </th>
                                    <th>
                                        Female
                                    </th>
                                    <th>
                                        Large Farmer
                                    </th>
                                    <th>
                                        Medium Farmer
                                    </th>
                                    <th>
                                        Small Farmer
                                    </th>
                                    <th>
                                        General
                                    </th>
                                    <th>
                                        OBS Caste
                                    </th>
                                    <th>
                                        SC Caste
                                    </th>

                                </tr>
                            </thead>
                            <tbody>
                                @foreach($farmerVellages as $key => $farmerVellage)
                                <tr data-entry-id="{{ $farmerVellage->village }}">

                                    <td>
                                        <?php
                                        $fpoName = App\fpo::where('id', $farmerVellage->fpoId)->first();

                                        ?>
                                        {{$fpoName->fpo_name}}
                                    </td>
                                    <td>
                                        {{ $farmerVellage->village }}
                                    </td>
                                    <td>
                                        {{ $farmerVellage->villagecount }}
                                    </td>
                                    <td>
                                        {{ $farmerVellage->malecount }}

                                    </td>
                                    <td>
                                        {{ $farmerVellage->femalecount }}
                                    </td>
                                    <td>
                                        {{ $farmerVellage->largecount }}
                                    </td>
                                    <td>
                                        {{ $farmerVellage->mediumcount }}
                                    </td>
                                    <td>
                                        {{ $farmerVellage->smallcount }}
                                    </td>
                                    <td>
                                        {{ $farmerVellage->generalcount}}
                                    </td>
                                    <td>
                                        {{ $farmerVellage->obccount}}
                                    </td>
                                    <td>
                                        {{ $farmerVellage->sccount}}
                                    </td>


                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>


                </div>
            </div>
        </div>
        @endcan

        @can('employee_manage')
        <div class="col-lg-12">
            <div class="row justify-content-center">
                <div class="col-lg-3 col-sm-6 col-xs-12">
                    <div class="white-box analytics-info">
                        <h3 class="box-title">FPOS</h3>
                        <ul class="list-inline two-part d-flex align-items-center mb-0">

                            <li class="ml-auto"><span class="counter text-info">
                                    <h4>{{$fpoCount}}</h4>
                                </span></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 col-xs-12">
                    <div class="white-box analytics-info">
                        <h3 class="box-title">FIG</h3>
                        <ul class="list-inline two-part d-flex align-items-center mb-0">
                            <li class="ml-auto"><span class="counter text-info">
                                    <h4>
                                        <?php
                                        $a = array();
                                        foreach ($fpo as $key => $fpos) {
                                            $a[] = $fpos->fig->count();
                                        ?>


                                        <?php } ?>

                                        <?php $aa = array_sum($a); ?>
                                        {{$aa}}

                                    </h4>
                                </span></li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-xs-12">
                    <div class="white-box analytics-info">
                        <h3 class="box-title">FARMERS</h3>
                        <ul class="list-inline two-part d-flex align-items-center mb-0">
                            <li class="ml-auto"><span class="counter text-info">
                                    <h4> <?php
                                            $a = array();
                                            foreach ($fpo as $key => $fpos) {
                                                $a[] = $fpos->farmer->count()
                                            ?>


                                        <?php } ?>

                                        <?php $aa = array_sum($a); ?>
                                        {{$aa}}
                                    </h4>
                                </span></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 col-xs-12">
                    <div class="white-box analytics-info">
                        <h3 class="box-title">Share Price</h3>
                        <ul class="list-inline two-part d-flex align-items-center mb-0">
                            <li class="ml-auto"><span class="counter text-info">
                                    <h4>
                                        <?php
                                        $a = array();
                                        foreach ($fpo as $key => $fpos) {
                                            $a[] = $fpos->farmer->sum('share_price');
                                        ?>


                                        <?php } ?>

                                        <?php $aa = array_sum($a); ?>
                                        {{$aa}}
                                    </h4>
                                </span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="card" style="margin-top:50px;">

                <div class="card-body">
                    <div class="table-responsive">
                        <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                            <thead>
                                <tr>
                                    <th width="10">

                                    </th>
                                    <th>
                                        {{ trans('cruds.user.fields.id') }}
                                    </th>
                                    <th>
                                        FPO Name
                                    </th>
                                    <th>
                                        FIG
                                    </th>
                                    <th>
                                        Farmer
                                    </th>
                                    <th>
                                        Share Price
                                    </th>

                                </tr>
                            </thead>
                            <tbody>
                                @foreach($fpo as $key => $fpos)
                                <tr data-entry-id="{{ $fpos->id }}">
                                    <td>

                                    </td>
                                    <td class="hide">
                                    </td>
                                    <td>
                                        {{ $fpos->id ?? '' }}
                                    </td>
                                    <td>
                                        {{ $fpos->fpo_name ?? '' }}
                                    </td>
                                    <td>
                                        {{ $fpos->fig->count() ?? '' }}
                                    </td>
                                    <td>
                                        {{ $fpos->farmer->count() ?? '' }}
                                    </td>
                                    <td>
                                        {{ $fpos->farmer->sum('share_price') ?? '' }}
                                    </td>

                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>


                </div>
            </div>
        </div>


        @endcan


    </div>
</div>

@endsection
@section('scripts')
@parent

@endsection
@section('scripts')
@parent
<script>
    $(function() {
        let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
        $.extend(true, $.fn.dataTable.defaults, {
            order: [
                [0, 'desc']
            ],
            pageLength: 10,
        });
        $('.datatable-User:not(.ajaxTable)').DataTable({
            buttons: dtButtons
        })
        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
            $($.fn.dataTable.tables(true)).DataTable()
                .columns.adjust();
        });
    })
</script>
@endsection