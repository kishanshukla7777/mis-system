<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ env('APP_NAME', 'MIS System') }}</title>

  <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" />
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet" />
  <link href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" rel="stylesheet" />
  <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
  <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet" />
  <link href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css" rel="stylesheet" />
  <link href="https://cdn.datatables.net/select/1.3.0/css/select.dataTables.min.css" rel="stylesheet" />
  <link href="https://unpkg.com/@coreui/coreui@2.1.16/dist/css/coreui.min.css" rel="stylesheet" />
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" rel="stylesheet" />
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
  <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.css" rel="stylesheet" />
  <link href="{{ asset('css/custom.css') }}" rel="stylesheet" />
  <link href="https://www.jqueryscript.net/demo/Responsive-jQuery-Dual-Select-Boxes-For-Bootstrap-Bootstrap-Dual-Listbox/src/bootstrap-duallistbox.css" rel="stylesheet" />
  @yield('styles')
  @toastr_css
</head>

<body class="app header-fixed sidebar-fixed aside-menu-fixed pace-done sidebar-lg-show">
  <header class="app-header navbar">
    <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
      <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="#">
      <span class="navbar-brand-full"><img style="width:110px;" src="{{url('/images/logo.png')}}" /></span>
      <span class="navbar-brand-minimized"><img style="width:110px;" src="{{url('/images/logo.png')}}" /></span>
    </a>
    <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
      <span class="navbar-toggler-icon"></span>
    </button>

    <ul class="nav navbar-nav ml-auto">
      @if(count(config('panel.available_languages', [])) > 1)
      <li class="nav-item dropdown d-md-down-none">
        <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
          {{ strtoupper(app()->getLocale()) }}
        </a>
        <div class="dropdown-menu dropdown-menu-right">
          @foreach(config('panel.available_languages') as $langLocale => $langName)
          <a class="dropdown-item" href="{{ url()->current() }}?change_language={{ $langLocale }}">{{ strtoupper($langLocale) }} ({{ $langName }})</a>
          @endforeach
        </div>
      </li>
      @endif


    </ul>
    @can('employee_manage')
    <div style="    margin-right: 21px;">
      <?php

      $customer = App\Customer::where('user_id', Illuminate\Support\Facades\Auth::id())->first();
      ?>
      <p style="width: 100%;font-size: 18px;margin-bottom: 0px;text-align: center;"><a href="{{ route('admin.user.profile',Auth::id()) }}">{{$customer->first_name}}</a></p>
      @foreach(Auth::user()->roles()->pluck('name') as $role)
      <p style="width:100%;" class="badge badge-info">{{ $role }}</p>
      @endforeach
    </div>
    @endcan
    @canany(['users_manage','company_manage'])
    <div style="    margin-right: 21px;">
      <p style="width: 100%;font-size: 18px;margin-bottom: 0px;text-align: center;"><a href="{{ route('admin.user.profile',Auth::id()) }}">{{Auth::user()->name}}</a></p>
      @foreach(Auth::user()->roles()->pluck('name') as $role)
      <p style="width:100%;" class="badge badge-info">{{ $role }}</p>
      @endforeach
    </div>
    @endcanany
  </header>

  <div class="app-body">
    @include('partials.menu')
    <main class="main">


      <div style="padding-top: 20px" class="container-fluid">
        @if(session('message'))
        <div class="row mb-2">
          <div class="col-lg-12">
            <div class="alert alert-success" role="alert">{{ session('message') }}</div>
          </div>
        </div>
        @endif
        @if($errors->count() > 0)
        <div class="alert alert-danger">
          <ul class="list-unstyled">
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
        @endif
        @yield('content')

      </div>


    </main>
    <form id="logoutform" action="{{ route('logout') }}" method="POST" style="display: none;">
      {{ csrf_field() }}
    </form>
  </div>
  <script type="text/javascript">
	var APP_URL = {!! json_encode(url('/')) !!}
</script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://unpkg.com/@coreui/coreui@2.1.16/dist/js/coreui.min.js"></script>
  <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
  <script src="//cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
  <script src="//cdn.datatables.net/buttons/1.2.4/js/buttons.flash.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.colVis.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
  <script src="https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js"></script>
  <script src="https://cdn.ckeditor.com/ckeditor5/11.0.1/classic/ckeditor.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.full.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js"></script>
  <script src="{{ asset('js/main.js') }}"></script>
  <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
  <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
  <script src="https://www.jqueryscript.net/demo/Responsive-jQuery-Dual-Select-Boxes-For-Bootstrap-Bootstrap-Dual-Listbox/src/jquery.bootstrap-duallistbox.js"></script>
  <script>
    $(function() {
      let copyButtonTrans = 'copy'
      let csvButtonTrans = 'csv'
      let excelButtonTrans = 'excel'
      let pdfButtonTrans = 'pdf'
      let printButtonTrans = 'print'
      let colvisButtonTrans = 'Column visibility'

      let languages = {
        'en': 'https://cdn.datatables.net/plug-ins/1.10.19/i18n/English.json'
      };

      $.extend(true, $.fn.dataTable.Buttons.defaults.dom.button, {
        className: 'btn'
      })
      $.extend(true, $.fn.dataTable.defaults, {
        language: {
          url: languages['{{ app()->getLocale() }}']
        },
        columnDefs: [{
          orderable: false,
          //className: 'select-checkbox',
          targets: 0
        }, {
          orderable: true,
          searchable: false,
          targets: -1
        }],
        select: {
          style: 'multi+shift',
          selector: 'td:first-child'
        },
        order: [
          [0, 'desc']
        ],
        pageLength: 10,
        scrollX: true,

        dom: 'lBfrtip<"actions">',
        buttons: [{
            extend: 'copy',
            className: 'btn btn-default',
            text: copyButtonTrans,
            modifier: {
              selected: null
            },
            exportOptions: {
              columns: 'th:not(:last-child)'
            }
          },
          {
            extend: 'csv',
            className: 'btn btn-default',
            text: csvButtonTrans,
            modifier: {
              selected: null
            },
            exportOptions: {
              columns: 'th:not(:last-child)'
            }
          },
          {
            extend: 'excel',
            className: 'btn btn-default',
            text: excelButtonTrans,
            modifier: {
              selected: null
            },
            exportOptions: {
              columns: 'th:not(:last-child)'
            }
          },
          {
            extend: 'pdf',
            className: 'btn btn-default',
            text: pdfButtonTrans,
            modifier: {
              selected: null
            },
            exportOptions: {
              columns: 'th:not(:last-child)'
            }
          },
          {
            extend: 'print',
            className: 'btn btn-default',
            text: printButtonTrans,
            modifier: {
              selected: null
            },
            exportOptions: {
              columns: 'th:not(:last-child)'
            }
          },
          {
            extend: 'colvis',
            className: 'btn btn-default',
            text: colvisButtonTrans,
            modifier: {
              selected: null
            },
            exportOptions: {
              columns: 'th:not(:last-child)'
            }
          }
        ]
      });

      $.fn.dataTable.ext.classes.sPageButton = '';
    });


    $('#duallistbox_demo1').bootstrapDualListbox({

      // default text
      filterTextClear: 'show all',
      filterPlaceHolder: 'Filter',
      moveSelectedLabel: 'Move selected',
      moveAllLabel: 'Move all',
      removeSelectedLabel: 'Remove selected',
      removeAllLabel: 'Remove all',

      // true/false (forced true on androids, see the comment later)
      moveOnSelect: true,

      // 'all' / 'moved' / false                                           
      preserveSelectionOnMove: false,

      // 'string', false                                     
      selectedListLabel: false,

      // 'string', false
      nonSelectedListLabel: false,

      // 'string_of_postfix' / false                                                      
      helperSelectNamePostfix: '_helper',

      // minimal height in pixels
      selectorMinimalHeight: 100,

      // whether to show filter inputs
      showFilterInputs: true,

      // string, filter the non selected options                                                 
      nonSelectedFilter: '',

      // string, filter the selected options                                              
      selectedFilter: '',

      // text when all options are visible / false for no info text                      
      infoText: 'Showing all {0}',

      // when not all of the options are visible due to the filter                                         
      infoTextFiltered: '<span class="badge badge-warning">Filtered</span> {0} from {1}',

      // when there are no options present in the list
      infoTextEmpty: 'Empty list',

      // sort by input order
      sortByInputOrder: false,

      // filter by selector's values, boolean
      filterOnValues: false,

      // boolean, allows user to unbind default event behaviour and run their own instead                                                   
      eventMoveOverride: false,

      // boolean, allows user to unbind default event behaviour and run their own instead                                                
      eventMoveAllOverride: false,

      // boolean, allows user to unbind default event behaviour and run their own instead
      eventRemoveOverride: false,

      // boolean, allows user to unbind default event behaviour and run their own instead                                              
      eventRemoveAllOverride: false,

      // sets the button style class for all the buttons
      btnClass: 'btn-outline-secondary',

      // string, sets the text for the "Move" button                                            
      btnMoveText: '&gt;',

      // string, sets the text for the "Remove" button                                                        
      btnRemoveText: '&lt;',

      // string, sets the text for the "Move All" button
      btnMoveAllText: '&gt;&gt;',

      // string, sets the text for the "Remove All" button
      btnRemoveAllText: '&lt;&lt;'

    });


    if ($("#companyForm").length > 0) {
      $('#companyForm').validate({
        rules: {
          name: {
            required: true,
            maxlength: 190,

          },
          email: {
            required: true,
            maxlength: 190,
            email: true
          },
          password: {
            required: true,
            minlength: 8
          },
          company_name: {
            required: true,
            maxlength: 190,
          },

          address: {
            required: true,
            maxlength: 500,
          },
          contact_no: {

            required: true,
            minlength: 9,
            maxlength: 13,
            number: true
          }

        },
        messages: {
          name: {
            required: 'Name is required',
            maxlength: 'Name should not be more than 50 character',
            minlenght: 'Invalid Name ',
          },
          email: {
            required: 'Email is required',
            maxlength: 'Email should not be more than 50 character'
          },
          password: {
            required: "Please provide a password",
            minlength: "Your password must be at least 8 characters long",
          },
          company_name: {
            required: 'Company Name is required',
            maxlength: 'Invalid Company Name ',
          },

          address: {
            required: 'Address is required',
            maxlength: 'Invalid Address',
          },
          contact_no: {
            required: 'Mobile Number is required',
            maxlength: 'Invalid Mobile Number',
            minlength: 'Invalid Mobile Number'
          }

        }
      })
    }

    if ($("#companyFormedit").length > 0) {
      $('#companyFormedit').validate({
        rules: {
          name: {
            required: true,
            maxlength: 190,

          },
          email: {
            required: true,
            maxlength: 190,
            email: true
          },

          company_name: {
            required: true,
            maxlength: 190,
          },
          contact_person: {
            required: true,
            maxlength: 190,
          },
          address: {
            required: true,
            maxlength: 500,
          },
          contact_no: {

            required: true,
            minlength: 9,
            maxlength: 13,
            number: true
          }

        },
        messages: {
          name: {
            required: 'Name is required',
            maxlength: 'Name should not be more than 50 character',
            minlenght: 'Invalid Name ',
          },
          email: {
            required: 'Email is required',
            maxlength: 'Email should not be more than 50 character'
          },

          company_name: {
            required: 'Company Name is required',
            maxlength: 'Invalid Company Name ',
          },
          contact_person: {
            required: 'Contact Person is required',
            maxlength: 'Invalid Contact Person ',
          },
          address: {
            required: 'Address is required',
            maxlength: 'Invalid Address',
          },
          contact_no: {
            required: 'Mobile Number is required',
            maxlength: 'Invalid Mobile Number',
            minlength: 'Invalid Mobile Number'
          }

        }
      })
    }

    if ($("#fpoForm").length > 0) {
      // Add the following code if you want the name of the file appear on select
      // $(".custom-file-input").on("change", function() {
      //   var fileName = $(this).val().split("\\").pop();
      //   $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
      // });
      $.validator.addMethod('filesize', function(value, element, param) {
        // param = size (in bytes) 
        // element = element to validate (<input>)
        // value = value of the element (file name)
        return this.optional(element) || (element.files[0].size <= param)
      });
      $('#fpoForm').validate({
        rules: {
          company_id: {
            required: true,
          },
          assign_emp: {
            required: true,
          },
          fpo_name: {
            maxlength: 190,
            required: true,

          },
          address: {
            maxlength: 500,
            required: true,
          },
          taluka: {
            maxlength: 190,
            required: true,
          },
          district: {
            maxlength: 190,
            required: true,
          },
          state: {
            maxlength: 190,
            required: true,
          },
          email: {
            maxlength: 190,
            required: true,
            email: true
          },
          contact_person: {
            maxlength: 190,
            required: true,
          },
          mobile_no: {
            minlength: 9,
            maxlength: 13,
            required: true,
            number: true
          },
          gst_doc: {
            required: false,
            extension: "png|jpeg|jpg|pdf",
            filesize: 5242880
          },
          tan_doc: {
            required: false,
            extension: "png|jpeg|jpg|pdf",
            filesize: 5242880
          },
          pan_doc: {
            required: false,
            extension: "png|jpeg|jpg|pdf",
            filesize: 5242880
          },
          cin_doc: {
            required: false,
            extension: "png|jpeg|jpg|pdf",
            filesize: 5242880
          },
          'other_doc[]': {
            required: false,
            extension: "png|jpeg|jpg|pdf",
            filesize: 5242880
          }
        },
        messages: {
          company_id: {
            required: "Please select Company",
          },
          assign_emp: {
            required: "Please select Employee",
          },
          fpo_name: {
            minlenght: 'Invalid FPO Name ',
            maxlength: 'FPO Name should not be more than 50 character',
            required: 'FPO Name is required',
          },
          address: {
            maxlength: 'Invalid Address',
            required: 'Address is required',
          },

          taluka: {
            maxlength: "Taluka Name should not be more than 50 character",
            required: "Taluka Name is required",
          },
          district: {
            maxlength: "District Name should not be more than 50 character",
            required: "District Name is required",
          },
          state: {
            maxlength: "State Name should not be more than 50 character",
            required: "State Name is required",
          },
          email: {
            maxlength: 'Email should not be more than 50 character',
            required: 'Email is required',
          },
          contact_person: {
            maxlength: 'Invalid Contact Person ',
            required: 'Contact Person is required',
          },

          mobile_no: {
            minlength: 'Invalid Mobile Number',
            maxlength: 'Invalid Mobile Number',
            required: 'Mobile Number is required',
          },
          gst_doc: {
            extension: "Sorry, only JPG, JPEG, PNG & PDF files are allowed.",
            filesize: "Uploding file size max 5MB",
          },
          tan_doc: {
            extension: "Sorry, only JPG, JPEG, PNG & PDF files are allowed.",
            filesize: "Uploding file size max 5MB",
          },
          pan_doc: {
            extension: "Sorry, only JPG, JPEG, PNG & PDF files are allowed.",
            filesize: "Uploding file size max 5MB",
          },
          cin_doc: {
            extension: "Sorry, only JPG, JPEG, PNG & PDF files are allowed.",
            filesize: "Uploding file size max 5MB",
          },
          'other_doc[]': {
            extension: "Sorry, only JPG, JPEG, PNG & PDF files are allowed.",
            filesize: "Uploding file size max 5MB",
          }

        }
      })
    }

    if ($("#customerForm").length > 0) {
      $('#customerForm').validate({
        rules: {
          company_id: {
            required: true,
          },
          name: {
            maxlength: 190,
            required: true,

          },
          first_name: {
            maxlength: 190,
            required: true,
          },
          last_name: {
            maxlength: 190,
            required: true,
          },
          email: {
            maxlength: 190,
            required: true,
            email: true
          },
          password: {
            required: true,
            minlength: 8
          },
          address: {
            maxlength: 500,
            required: true,
          },
          mobile_no: {
            minlength: 9,
            maxlength: 13,
            required: true,
            number: true
          }
        },
        messages: {
          company_id: {
            required: "Please select company",
          },
          name: {
            minlenght: 'Invalid employee Name ',
            maxlength: 'employee Name should not be more than 50 character',
            required: 'employee Name is required',
          },
          first_name: {
            maxlength: 'first Name should not be more than 50 character',
            required: 'first Name is required',
          },
          last_name: {
            maxlength: 'last name should not be more than 50 character',
            required: 'last name is required',
          },
          email: {
            required: 'Email is required',
            maxlength: 'Email should not be more than 50 character'
          },
          password: {
            required: "Please provide a password",
            minlength: "Your password must be at least 8 characters long",
          },
          address: {
            maxlength: 'Invalid Address',
            required: 'Address is required',
          },

          mobile_no: {
            minlength: 'Invalid Mobile Number',
            maxlength: 'Invalid Mobile Number',
            required: 'Mobile Number is required',
          }


        }
      })
    }
    if ($("#customerFormedit").length > 0) {
      $('#customerFormedit').validate({
        rules: {
          company_id: {
            required: true,
          },
          name: {
            maxlength: 190,
            required: true,

          },
          first_name: {
            maxlength: 190,
            required: true,
          },
          last_name: {
            maxlength: 190,
            required: true,
          },
          email: {
            maxlength: 190,
            required: true,
            email: true
          },

          address: {
            maxlength: 500,
            required: true,
          },
          mobile_no: {
            minlength: 9,
            maxlength: 13,
            required: true,
            number: true
          }
        },
        messages: {
          company_id: {
            required: "Please select company",
          },
          name: {
            minlenght: 'Invalid employee Name ',
            maxlength: 'employee Name should not be more than 50 character',
            required: 'employee Name is required',
          },
          first_name: {
            maxlength: 'first Name should not be more than 50 character',
            required: 'first Name is required',
          },
          last_name: {
            maxlength: 'last name should not be more than 50 character',
            required: 'last name is required',
          },
          email: {
            required: 'Email is required',
            maxlength: 'Email should not be more than 50 character'
          },

          address: {
            maxlength: 'Invalid Address',
            required: 'Address is required',
          },

          mobile_no: {
            minlength: 'Invalid Mobile Number',
            maxlength: 'Invalid Mobile Number',
            required: 'Mobile Number is required',
          }


        }
      })
    }
    if ($("#farmerForm").length > 0) {
      $('#farmerForm').validate({
        rules: {
          fpo_id: {
            required: true,
            maxlength: 190,
          },
          farmer_name: {
            required: true,
            maxlength: 190,
          },
          dob: {
            required: false,
            maxlength: 190,
          },
          email: {
            required: false,
            maxlength: 190,
            email: true
          },
          address: {
            required: true,
            maxlength: 500,
          },
          contact_no: {
            required: false,
            minlength: 9,
            maxlength: 13,
            number: true,
          },
          block: {
            required: true,
            maxlength: 190,
          },
          village: {
            required: true,
            maxlength: 190,
          },
          city: {
            required: true,
            maxlength: 190,
          },
          district: {
            required: true,
            maxlength: 190,
          },
          state: {
            required: true,
            maxlength: 190,
          },
          irrigated_area: {
            required: false,
            maxlength: 190,
          },
          unirrigated_area: {
            required: false,
            maxlength: 190,
          },
          infrastructure_detail: {
            required: false,
            maxlength: 190,
          },
          vegetables_grown: {
            required: false,
            maxlength: 190,
          },
          aadhar_number: {
            required: false,
            minlength: 12,
            maxlength: 12,
            number: true
          },
          kharsa_no: {
            required: false,
            maxlength: 10,
          },
          survey_no: {
            required: false,
            maxlength: 10,
          },
          irrigation: {
            required: false,
            maxlength: 190,
          },
          water_source: {
            required: false,
          },
          transport: {
            required: false,
            maxlength: 190,
          },
          seed_type: {
            required: false,
            maxlength: 190,
          },
          seed_buy_from: {
            required: false,
            maxlength: 190,
          },
          dependents: {
            required: false,
            maxlength: 190,
          },
          land_organic_farming: {
            required: false,
            maxlength: 190,
          },
          manure: {
            required: false,
            maxlength: 190,
          },
          fertilizer_type: {
            required: false,
            maxlength: 190,
          },
          fertilizer_obtain_from: {
            required: false,
            maxlength: 190,
          },
          pesticides_used: {
            required: false,
            maxlength: 190,
          },
          cost_of_production: {
            required: false,
            maxlength: 190,
          },
          father_name: {
            required: true,
            maxlength: 190,
          },
          share_price: {
            required: true,
            maxlength: 190,
          },
          caste: {
            required: true,
          },
          farmer_type: {
            required: true,
          },
          gender: {
            required: true,
          },
          trainee_required: {
            required: false,
          },
          bank_account: {
            required: false,
          },
          kisan_credit_card: {
            required: false,
          },
          loan: {
            required: false,
          },
          shg: {
            required: false,
          },
          total_family_no: {
            required: false,
          },
          marital_status: {
            required: true,
          },
          literate: {
            required: false,
          },
          organic_farming: {
            required: false,
          }
        },
        messages: {
          fpo_id: {
            required: 'FPO ID is required',
          },
          farmer_name: {
            required: 'Farmer Name is required',
            maxlength: 'Farmer Name should not be more than 50 character',

          },
          dob: {
            required: 'Date of Birth is required',
          },
          email: {
            maxlength: 'Email should not be more than 50 character',
          },
          address: {
            required: 'Address is required',
            maxlength: 'Invalid Address',
          },
          contact_no: {
            required: 'Mobile Number is required',
            maxlength: 'Invalid Mobile Number',
            minlength: 'Invalid Mobile Number'
          },
          block: {
            required: 'Block is required',
            maxlength: 'Invalid Block Name',
          },
          taluka: {
            maxlength: "Taluka Name should not be more than 50 character",
            required: "Taluka Name is required",
          },
          city: {
            required: "City Name is required",
            maxlength: "City Name should not be more than 50 character",
          },
          district: {
            maxlength: "District Name should not be more than 50 character",
            required: "District Name is required",
          },
          state: {
            maxlength: "State Name should not be more than 50 character",
            required: "State Name is required",
          },
          irrigated_area: {
            maxlength: "Irrigated Area should not be more than 50 character",
            required: "Irrigated Area is required",
          },
          unirrigated_area: {
            maxlength: "Unirrigated Area should not be more than 50 character",
            required: "Unirrigated Area is required",
          },
          infrastructure_detail: {
            maxlength: "Infrastructure Detail should not be more than 50 character",
            required: "Infrastructure Detail is required",
          },
          vegetables_grown: {
            maxlength: "Vegetables Grown should not be more than 50 character",
            required: "Vegetables Grown  is required",
          },
          aadhar_number: {
            maxlength: 'Invalid Aadhar Number',
            minlength: 'Invalid Aadhar Number',
            required: "Aadhar Number is required",
          },
          kharsa_no: {
            maxlength: "Kharsa No should not be more than 50 character",

          },
          survey_no: {
            maxlength: "Survey No should not be more than 50 character",

          },
          share_price: {
            maxlength: "Share Price should not be more than 50 character",
            required: "Share Price is required",
          },
          irrigation: {
            maxlength: "Irrigation should not be more than 50 character",
            required: "Irrigation is required",
          },
          water_source: {
            required: "Water Source is required",
          },
          transport: {
            maxlength: "Transport should not be more than 50 character",
            required: "Transport is required",
          },
          seed_type: {
            maxlength: "Seed Type should not be more than 50 character",
            required: "Seed Type is required",
          },
          seed_buy_from: {
            maxlength: "Seed buy From should not be more than 50 character",
            required: "Seed buy From is required",
          },
          dependents: {
            maxlength: "Dependents should not be more than 50 character",
            required: "Dependents is required",
          },
          land_organic_farming: {
            maxlength: "Land Organic Farming should not be more than 50 character",
            required: "Land Organic Farming is required",
          },
          manure: {
            maxlength: "Manure should not be more than 50 character",
            required: "Manure is required",
          },
          fertilizer_type: {
            maxlength: "Fertilizer Type should not be more than 50 character",
            required: "Fertilizer Type is required",
          },
          fertilizer_obtain_from: {
            maxlength: "Fertilizer Obtain From should not be more than 50 character",
            required: "Fertilizer Obtain From is required",
          },
          pesticides_used: {
            maxlength: "Pesticides Used should not be more than 50 character",
            required: "Pesticides Used is required",
          },
          cost_of_production: {
            maxlength: "Cost of Production should not be more than 50 character",
            required: "Cost of Production is required",
          },
          father_name: {
            maxlength: "Father Name should not be more than 50 character",
            required: "Father Name is required",
          },
          period: {
            maxlength: "Period should not be more than 50 character",
            required: "Period is required",
          },
          caste: {
            required: 'Caste is required',

          },
          farmer_type: {
            required: 'Farmer Type is required',
          },
          gender: {
            required: 'Gender is required',

          },
          trainee_required: {
            required: 'Trainee is required',
          },
          bank_account: {
            required: 'Do you have a back account?',
          },
          kisan_credit_card: {
            required: 'Do you have a kishan credit card ?',
          },
          loan: {
            required: 'Do you have a Loan?',
          },
          shg: {
            required: 'Do you have a SGH?',
          },
          total_family_no: {
            required: 'please enter your family member',
          },
          marital_status: {
            required: 'Please choos your status',
          },
          literate: {
            required: 'Literate is required',
          },
          organic_farming: {
            required: 'Do you have a Organic Farming',
          }
        },
        errorPlacement: function(error, element) {
          if (element.attr("name") == "caste") {
            error.appendTo('.errorcaste');
            return;
          }
          if (element.attr("name") == "farmer_type") {
            error.appendTo('.errorfarmer_type');
            return;
          }
          if (element.attr("name") == "gender") {
            error.appendTo('.errorgender');
            return;
          }
          if (element.attr("name") == "trainee_required") {
            error.appendTo('.errortrainee_required');
            return;
          }
          if (element.attr("name") == "bank_account") {
            error.appendTo('.errorbank_account');
            return;
          }
          if (element.attr("name") == "kisan_credit_card") {
            error.appendTo('.errorkisan_credit_card');
            return;
          }
          if (element.attr("name") == "loan") {
            error.appendTo('.errorloan');
            return;
          }
          if (element.attr("name") == "shg") {
            error.appendTo('.errorshg');
            return;
          }
          if (element.attr("name") == "informal_loan") {
            error.appendTo('.errorinformal_loan');
            return;
          }
          if (element.attr("name") == "marital_status") {
            error.appendTo('.errormarital_status');
            return;
          }
          if (element.attr("name") == "literate") {
            error.appendTo('.errorliterate');
            return;
          }
          if (element.attr("name") == "organic_farming") {
            error.appendTo('.errororgaic_farming');
            return;
          }
          if (element.attr("name") == "fpo_id") {
            error.appendTo('.errorfpo_id');
            return;
          }
          if (element.attr("name") == "farmer_name") {
            error.appendTo('.errorfarmer_name');
            return;
          }
          if (element.attr("name") == "dob") {
            error.appendTo('.errordob');
            return;
          }
          if (element.attr("name") == "email") {
            error.appendTo('.erroremail');
            return;
          }
          if (element.attr("name") == "address") {
            error.appendTo('.erroraddress');
            return;
          }
          if (element.attr("name") == "contact_no") {
            error.appendTo('.errorcontact_no');
            return;
          }
          if (element.attr("name") == "block") {
            error.appendTo('.errorblock');
            return;
          }
          if (element.attr("name") == "village") {
            error.appendTo('.errorvillage');
            return;
          }
          if (element.attr("name") == "city") {
            error.appendTo('.errorcity');
            return;
          }
          if (element.attr("name") == "district") {
            error.appendTo('.errordistrict');
            return;
          }
          if (element.attr("name") == "state") {
            error.appendTo('.errorstate');
            return;
          }
          if (element.attr("name") == "irrigated_area") {
            error.appendTo('.errorirrigated_area');
            return;
          }
          if (element.attr("name") == "unirrigated_area") {
            error.appendTo('.errorunirrigated_area');
            return;
          }
          if (element.attr("name") == "infrastructure_detail") {
            error.appendTo('.errorinfrastructure_detail');
            return;
          }
          if (element.attr("name") == "vegetables_grown") {
            error.appendTo('.errorvegetables_grown');
            return;
          }
          if (element.attr("name") == "aadhar_number") {
            error.appendTo('.erroraadhar_number');
            return;
          }
          if (element.attr("name") == "kharsa_no") {
            error.appendTo('.errorkharsa_no');
            return;
          }
          if (element.attr("name") == "survey_no") {
            error.appendTo('.errorsurvey_no');
            return;
          }
          if (element.attr("name") == "irrigation") {
            error.appendTo('.errorirrigation');
            return;
          }
          if (element.attr("name") == "water_source") {
            error.appendTo('.errorwater_source');
            return;
          }
          if (element.attr("name") == "transport") {
            error.appendTo('.errortransport');
            return;
          }
          if (element.attr("name") == "seed_type") {
            error.appendTo('.errorseed_type');
            return;
          }
          if (element.attr("name") == "seed_buy_from") {
            error.appendTo('.errorseed_buy_from');
            return;
          }
          if (element.attr("name") == "total_family_no") {
            error.appendTo('.errortotal_family_no');
            return;
          }
          if (element.attr("name") == "land_organic_farming") {
            error.appendTo('.errorland_organic_farming');
            return;
          }
          if (element.attr("name") == "manure") {
            error.appendTo('.errormanure');
            return;
          }
          if (element.attr("name") == "fertilizer_type") {
            error.appendTo('.errorfertilizer_type');
            return;
          }
          if (element.attr("name") == "fertilizer_obtain_from") {
            error.appendTo('.errorfertilizer_obtain_from');
            return;
          }
          if (element.attr("name") == "pesticides_used") {
            error.appendTo('.errorpesticides_used');
            return;
          }
          if (element.attr("name") == "cost_of_production") {
            error.appendTo('.errorcost_of_production');
            return;
          }
          if (element.attr("name") == "father_name") {
            error.appendTo('.errorfather_name');
            return;
          }
          if (element.attr("name") == "period") {
            error.appendTo('.errorperiod');
            return;
          }
          if (element.attr("name") == "share_price") {
            error.appendTo('.errorshare_price');
            return;
          }
        }


      })
    }
    if ($("#corpForm").length > 0) {
      $('#corpForm').validate({
        rules: {
          season_id: {
            required: true,
          },
          crop_name: {
            maxlength: 50,
            required: true,
          },
          area: {
            maxlength: 20,
            required: false,
          },
          direct: {
            maxlength: 50,
            required: false,
          },
          total_production: {
            required: false,
            maxlength: 8
          },
          average_yield: {
            maxlength: 10,
            required: false,
          },
          through_intermediaries: {
            maxlength: 10,
            required: false,
          },
          total: {
            maxlength: 10,
            required: false,
          }
        },
        messages: {
          season_id: {
            required: 'Please select season',
          },
          crop_name: {
            maxlength: "Crop Name should not be more than 50 character",
            required: "Crop Name is required",
          },
          area: {
            maxlength: "Area should not be more than 20 character",
            required: "Area is required",
          },
          direct: {
            maxlength: "Direct  should not be more than 50 character",
            required: "Direct is required",
          },
          total_production: {
            required: "Total Production is required",
            maxlength: "Total Production should not be more than 8 character"
          },
          average_yield: {
            maxlength: "Average Yield should not be more than 10 character",
            required: "Average Yield is required",
          },
          through_intermediaries: {
            maxlength: "Through Intermediaries should not be more than 10 character",
            required: "Through Intermediaries is required",
          },
          total: {
            maxlength: "Total should not be more than 10 character",
            required: "Total is required",
          }
        }
      })
    }



    if ($("#figForm").length > 0) {
      $('#figForm').validate({
        rules: {
          fpo_id: {
            required: true,
          },
          name: {
            required: true,
            maxlength: 50
          }

        },
        messages: {
          fpo_id: {
            required: "FPO ID is required",
          },
          name: {
            required: "Group Name is required ",
            maxlength: 'Group Name should not be more than 50 character',

          }
        }
      })
    }
  </script>



  @yield('scripts')
</body>

@toastr_js
@toastr_render
<style>
  .dataTables_wrapper .dataTables_paginate .paginate_button {
    padding: 0 !important;
  }
</style>

</html>