<?php

Route::redirect('/', 'admin/home');

Auth::routes(['register' => false]);

// Change Password Routes...
Route::get('change_password', 'Auth\ChangePasswordController@showChangePasswordForm')->name('auth.change_password');
Route::patch('change_password', 'Auth\ChangePasswordController@changePassword')->name('auth.change_password');

Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('permissions', 'Admin\PermissionsController');
    Route::delete('permissions_mass_destroy', 'Admin\PermissionsController@massDestroy')->name('permissions.mass_destroy');
    Route::resource('roles', 'Admin\RolesController');
    Route::delete('roles_mass_destroy', 'Admin\RolesController@massDestroy')->name('roles.mass_destroy');
    Route::resource('users', 'Admin\UsersController');
    Route::get('user/profile/{id}', 'Admin\UsersController@userProfile')->name('user.profile');
    Route::post('user/profile', 'Admin\UsersController@saveProfile')->name('user.Saveprofile');
    Route::delete('users_mass_destroy', 'Admin\UsersController@massDestroy')->name('users.mass_destroy');
    Route::resource('fpo', 'Admin\fpoController');
    Route::resource('farmer', 'Admin\farmerController');
    Route::resource('company', 'Admin\CompanyController');
    Route::delete('company_mass_destroy', 'Admin\CompanyController@massDestroy')->name('company.mass_destroy');
    Route::resource('fpo', 'Admin\fpoController');
    Route::delete('fpo_mass_destroy', 'Admin\fpoController@massDestroy')->name('fpo.mass_destroy');
    Route::resource('customer', 'Admin\CustomerController');
    Route::delete('customer_mass_destroy', 'Admin\CustomerController@massDestroy')->name('customer.mass_destroy');
    Route::resource('crop', 'Admin\CropController');
    Route::resource('fig', 'Admin\FigController');
    Route::get('fig/addfarmer/{id}', 'Admin\FigController@addfarmer')->name('fig.addfarmer');
    Route::post('fig/savefarmer', 'Admin\FigController@savefarmer')->name('fig.savefarmer');
    Route::get('lrp/{id}', 'Admin\FigController@addlrp')->name('fig.addlrp');
    Route::post('savelrp', 'Admin\FigController@savelrp')->name('fig.savelrp');
    Route::get('otherdoc/{id}', 'Admin\fpoController@otherdoc')->name('fpo.fpoController');
    Route::get('fpo/comapny/{id}', 'Admin\fpoController@comapny')->name('fpo.upload');
    Route::get('fpo/fig/{id}', 'Admin\fpoController@fig')->name('fpo.fig');
});
